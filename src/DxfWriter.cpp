#include <dxf/Base.h>
#include <dxf/DxfWriter.h>
#include <string>
#include <algorithm>
#include <fstream>
#include <iostream>
#include <sstream>
#include <cstdio>

using std::string;
using std::ifstream;
using std::stringstream;

namespace dxf {

	//静态成员初始化
	bool DxfWriter::first=true;

	//cad2000的dxf文件的固定handle
	//static int a[]={
	//	//Header节的handseed变量的handle
	//	0xFFFF,//1、
	//
	//	//0x1--0xA为tableSection各表的固定handle
	//	0x1,//2、Block_Record
	//	0x2,//3、Layer
	//	0x3,//4、Style
	//	0x5,//5、LType
	//	0x6,//6、View
	//	0x7,//7、Ucs
	//	0x8,//8、VPort
	//	0x9,//9、AppId
	//	0xA,//10、DimStyle
	//
	//	//Block_Record表中的默认块的handle
	//	0x1F,//11、Model_Space--5
	//	0x22,//12、Model_Space--340
	//	0xD2,//13、Paper_Space--5
	//	0xD3,//14、Paper_Space--340
	//	0xD6,//15、Paper_Space0--5
	//	0xD7,//16、Paper_Space0--340
	//
	//	//Style表的Standard样式的handle
	//	0x11,//17、Standard--Style
	//
	//	//AppId表的默认Acad的handle
	//	0x12,//18、Acad--AppId
	//
	//	//Layer表的默认图层0层的handle
	//	0x10,//19、0--layer
	//	0xF,//20、--layer的打印样式handle
	//
	//	//LType表的3个默认线型的handle
	//	0x14,//21、ByBlock--LType
	//	0x15,//22、ByLayer--LType
	//	0x16,//23、Continuous--LType
	//
	//	//DimStyle表的标准样式的handle
	//	0x27,//24、ISO-25--DimStyle--105
	//
	//	//Block节的3个默认块的handle
	//	0x20,//25、*Model_Space
	//	0x21,//26、*Model_Space
	//	0xD4,//27、*Paper_Space
	//	0xD5,//28、*Paper_Space
	//	0xD8,//29、*Paper_Space0
	//	0xD9,//30、*Paper_Space0
	//	
	//	0xC,//31、Objects--Dictionary固定的handle
	//	0xD,//32、ACAD_GROUP
	//	0x17,//33、ACAD_MLINESTYLE
	//	0x18,//34、ACAD_MLINESTYLE
	//	0x6B,//35、ACAD_COLOR
	//	0x1A,//36、ACAD_LAYOUT
	//	0x19,//37、ACAD_PLOTSETTINGS
	//	0xE,//38、ACAD_PLOTSTYLENAME
	//	0x7E,//39、ACAD_TABLESTYLE
	//	0x5E,//40、AcDbVariableDictionary
	//	0xF3,//41、DWGPROPS
	//	0x22,//42、Model---ACAD_LAYOUT
	//	0xD3,//43、布局1---ACAD_LAYOUT
	//	0xD7,//44、布局2---ACAD_LAYOUT
	//	0x18,//45、Standard---ACAD_MLINESTYLE
	//	0xE,//46、ACDBDICTIONARYWDFLT
	//	0xF,//47、Normal---ACDBDICTIONARYWDFLT
	//	0x7F,//48、Standard---ACAD_TABLESTYLE
	//	0x84,//49、CTABLESTYLE---AcDbVariableDictionary
	//	0x5F,//50、DIMASSOC---AcDbVariableDictionary
	//	0xF5,//51、Model---ACAD_LAYOUT
	//	0xF4,//52、
	//	0x84,//53、
	//	0x63,//54、
	//	0xF1,//55、
	//	0xF7,//56、
	//	0xF8,//57、
	//	0xF9,//58、
	//	0xFA,//59、
	//	0xF6//60
	//
	//	//more
	//};


	DxfWriter::DxfWriter()
	{
		this->m_handle=0x30;//?

		if(first) {
			//关于已使用句柄(hasRegisterHandle)初始化的解决方法

			//方法1、定义一个数组，将cad2000的dxf文件中的固定handle写入到数组中
			//    然后插入到hasRegisterHandle（记录已使用的handle）
			//问题：数组的长度不能确定(因为可能后续还要继续添加)

			//this->hasRegisterHandle.insert(this->hasRegisterHandle.begin(),a,a+60);	



			//方法2、将固定handle写入文件中，然后读入并插入到hasRegisterHandle中
			//		 相对于方法1不用实现确定handle的个数（长度）,并且能够人工手动添加固定handle，
			//       且不用修改源代码
			//问题：程序执行的时候必须附带着这个文件文件

			//ifstream inFile;
			//inFile.open("cadHandles.txt");
			//if(!inFile) {
			//	std::cerr<<"不能打开文件!程序退出"<<std::endl;
			//	exit(1);
			//}

			//string shandle;
			//int handle;
			//while(!inFile.eof()) {
			//		inFile>>shandle;
			//		HexToDec(shandle.substr(2),handle);//去掉0x前缀，并转化成10进制数
			//		this->hasRegisterHandle.push_back(handle);
			//		inFile>>shandle;
			//}
			//inFile.close();


			//方法3、观察固定handle的范围，将一定范围内的整数全部加入到hasRegisterHandle中（大面积封杀）
			//         1) 1~50
			//         2) 0x50~0x8F
			//         3) 0xD0~0xDF
			//         4) 0xF0~0xFF
			//         5) 0xFFFF

			//特点：相对于方法2不用手动的添加handle，基本不会出现handle重复的问题（也就不会出现cad打开dxf文件时的内存错误了）
			//       并且程序不用附带文件了

			for(int i=0x1;i<=50;i++)
				this->hasRegisterHandle.push_back(i);

			for(int i=0x50;i<=0x8F;i++)
				this->hasRegisterHandle.push_back(i);

			for(int i=0xD0;i<=0xDF;i++)
				this->hasRegisterHandle.push_back(i);

			for(int i=0xF0;i<=0xFF;i++)
				this->hasRegisterHandle.push_back(i);

			this->hasRegisterHandle.push_back(0xFFFF);

			first=false;
		}

	}

	DxfWriter::DxfWriter(const char* fileName):outFile(fileName)
	{
		this->m_handle=0x30;//?

		if(first) {
			//说明请参见上面的默认构造函数
			for(int i=0x1;i<=50;i++)
				this->hasRegisterHandle.push_back(i);

			for(int i=0x50;i<=0x8F;i++)
				this->hasRegisterHandle.push_back(i);

			for(int i=0xD0;i<=0xDF;i++)
				this->hasRegisterHandle.push_back(i);

			for(int i=0xF0;i<=0xFF;i++)
				this->hasRegisterHandle.push_back(i);

			this->hasRegisterHandle.push_back(0xFFFF);

			first=false;
		}
	}

	DxfWriter::~DxfWriter()
	{
		if(outFile.is_open())
			this->close();
		this->hasRegisterHandle.clear();
	}

	int DxfWriter::handle()
	{
		while(true) {
			if(this->findHandle(m_handle))
				m_handle++;
			else {
				this->hasRegisterHandle.push_back(m_handle);
				break;
			}
		}

		return m_handle;
	}

	void DxfWriter::resetHandle()
	{
		m_handle=1;
	}

	bool DxfWriter::findHandle(int _handle)
	{
		vector<int>::iterator itr=std::find(this->hasRegisterHandle.begin(),this->hasRegisterHandle.end(),_handle);
		if(itr==this->hasRegisterHandle.end()) 
			return false;
		else
			return true;
	}

	void DxfWriter::registerHandle(int _handle)
	{
		if(!this->findHandle(_handle))
			this->hasRegisterHandle.push_back(_handle);
	}

	void DxfWriter::open(const char* fileName)
	{	
		//作为测试，不管文件存在与否，都新建一个新的文件
		//	if(!this->isFileExist(fileName))
		outFile.open(fileName,std::ios::out);
		//	else
		//		outFile.open(fileName);
	}

	bool DxfWriter::isFileExist(const char* fileName) const
	{
		bool isFileExist=false;
		ifstream inFile;
		inFile.open(fileName);
		if(inFile) isFileExist=true;
		inFile.close();
		return isFileExist;
	}

	bool DxfWriter::isOpenFailed() const
	{
		return outFile.fail();
	}

	void DxfWriter::close() 
	{
		outFile.close();
	}


	void DxfWriter::writeReal(int group_code, double value) 
	{
		//stringstream ss;
		//string s;

		//ss.setf(std::ios::left);//设置左对齐
		//ss.precision(16);//设置精度为16位
		//ss<<value;

		//ss>>s;//写入到s中
		//ss.clear();

		char str[256];
		sprintf(str, "%.16lf", value);
		string s(str);

		StringExt::replace_all(s,",",".");
		StringExt::trim_right(s,"0");//去掉后面多余的0

		//if(s.length()==0)
		//	s+="0.0";
		if(s.at(s.length()-1)=='.')
			s+="0";

		this->writeString(group_code, s);
		outFile.flush();
	}



	void DxfWriter::writeInt(int group_code, int value) 
	{
		outFile << (group_code<10 ? "  " : (group_code<100 ? " " : "")) << group_code << "\n"
			<< value << "\n";
	}



	void DxfWriter::writeHex(int group_code, int value)  
	{
		string s;
		stringstream ss;
		ss.flags(std::ios::hex);
		ss<<value;
		ss>>s;
		ss.clear();

		writeString(group_code, StringExt::to_upper_copy(s));
	}

	void DxfWriter::writeString(int group_code, const string& value)  
	{
		outFile << (group_code<10 ? "  " : (group_code<100 ? " " : "")) << group_code << "\n"
			<< value << "\n";
	}

}