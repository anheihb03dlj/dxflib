#include <dxf/DxfReader.h>
#include <iostream>
#include <algorithm>
#include <dxf/Base.h>

namespace dxf {

	static bool stringToHex(const string& shex,int & idec)//将16进制字符串转化成10进制整数(注意：16进制字符串不能带0x前缀)
	{
		int i,mid;   
		int len=shex.length();       
		if(len>8) return false;      
		mid=0;   
		idec=0;   
		for(i=0;i<len;i++) {   
			if(shex[i]>='0'&&shex[i]<='9')   mid=shex[i]-'0';   
			else if(shex[i]>='a' && shex[i]<='f')
				mid=shex[i]-'a'+10;   
			else if(shex[i]>='A' && shex[i]<='F')
				mid=shex[i]-'A'+10;   
			else  
				return false;       
			mid<<=((len-i-1)<<2);   
			idec |=mid;       
		}       
		return true;   

	}


	DxfReader::DxfReader()
	{
		currentLine=0;
		startPos.push_back(0);
	}

	DxfReader::DxfReader(const char* fileName):inFile(fileName)
	{

	}

	DxfReader::~DxfReader()
	{
		if(inFile.is_open())
			this->close();
	}

	void DxfReader::open(const char* fileName)
	{
		inFile.open(fileName);
		if(!inFile) {
			std::cerr<<"文件不存在！"<<std::endl;
			std::cerr<<"程序退出......"<<std::endl;
			exit(1);
		}
	}

	bool DxfReader::isFileExist(const char* fileName) const
	{
		bool isFileExist=false;
		ifstream infile;
		infile.open(fileName);
		if(infile) isFileExist=true;
		infile.close();
		return isFileExist;
	}

	bool DxfReader::isEndOfFile() const 
	{
		return inFile.eof();
	}

	bool DxfReader::isOpenFailed() const
	{
		return inFile.fail();
	}

	void DxfReader::close()
	{
		inFile.close();
	}

	void DxfReader::readGroupCode(int &group_code)
	{
		storePos();//存储当前行的起始位置和行号

		int _groupCode;
		inFile>>_groupCode;
		inFile.seekg(2,std::ios::cur);
		group_code=_groupCode;
	}


	void DxfReader::readString(string &value)
	{
		storePos();

		string _value;
		std::getline(inFile,_value);
		value=StringExt::trim(_value);
	}

	void DxfReader::readInt(int &value)
	{
		string s;
		readString(s);
		value=StringExt::toInt(s);
	}


	void DxfReader::readReal(double &value)
	{
		string s;
		readString(s);
		value=StringExt::toReal(s);
	}

	void DxfReader::readHex(int &value)
	{
		string s;
		readString(s);
		stringToHex(s,value);
	}

	long DxfReader::getLinePos(int lineNum)
	{
		if(lineNum<=0) return -1;

		vector<long>::iterator itr=std::find(startPos.begin(),startPos.end(),lineNum);
		if(itr!=startPos.end())
			return *itr;
		else 
			return -1;
	}

	void DxfReader::goBack(int lineNum)
	{
		if(lineNum<=0) return ;

		int i=lineNum;
		while(i>0) {
			if(i==1) 
				inFile.seekg(startPos.at(this->currentLine));
			this->currentLine--;
			this->startPos.pop_back();
			i--;
		}
	}

}