#include <dxf/Dxf.h>

//section类(目前只支持Table Section、Block Section、Entity Section)
#include <dxf/section/Section.h>
#include <dxf/section/EntitiesSection.h>
#include <dxf/section/TablesSection.h>
#include <dxf/section/BlocksSection.h>

// 实体子类
#include <dxf/entity/DxfEntity.h>
#include <dxf/entity/DxfLine.h>
#include <dxf/entity/DxfArc.h>
#include <dxf/entity/DxfCircle.h>
#include <dxf/entity/DxfEllipse.h>
#include <dxf/entity/DxfText.h>
#include <dxf/entity/DxfMText.h>
#include <dxf/entity/DxfLWPolyLine.h>
#include <dxf/entity/DxfInsert.h>
#include <dxf/entity/DxfVertex.h>


//扩展数据管理类
//#include <dxf/entity/xdata.h>


//表类(图层、线型)
#include <dxf/table/DxfTable.h>
#include <dxf/table/DxfLayer.h>
#include <dxf/table/DxfLineType.h>


//块类
#include <dxf/block/DxfBlock.h>


//读写类
#include <dxf/DxfReader.h>
#include <dxf/DxfWriter.h>

#include <sstream>
#include <iostream>
#include <map>
using std::map;
//
//void Dxf::destroyDxfReader(DxfReader* infile)
//{
//	delete infile;
//}
//
//void Dxf::destroyDxfWriter(DxfWriter* outfile)
//{
//	delete outfile;
//}

Dxf::Dxf(void)
{
	initial();
}

Dxf::~Dxf(void)
{
	if(!hasCleared) {
#ifdef _DEBUG
		std::cout<<"调用Dxf::~Dxf(void)"<<std::endl;
#endif
		clear();
	}
}


void Dxf::initial()
{
	hasCleared=false;
	DxfLayer::initial();//初始化默认图层（创建0层）
	DxfLineType::initial();//创建几条默认的线型（bylaer,byblock,continuous、dot、dash等等）
	DxfBlock::initial();//创建默认的块定义
}

void Dxf::clear()
{
	if(!hasCleared) {
#ifdef _DEBUG
		std::cout<<"调用Dxf::clear()"<<std::endl;
#endif

		//做一些静态变量的清除和初始工作
		DxfEntity::first=true;
		DxfEntity::num=0;
		DxfEntity::entityTypeName.clear();
		DxfWriter::first=true;
		DxfLayer::defaultLayer=NULL;
		DxfLineType::first=true;
		DxfBlock::first=true;
		DxfTable::tableTypeName.clear();
		DxfTable::first=true;
		///
		delete Section::tablesSection;
		Section::tablesSection=NULL;

		delete Section::blocksSection;
		Section::blocksSection=NULL;

		delete Section::entitiesSection;
		Section::entitiesSection=NULL;

		hasCleared=true;
	}
}

DxfReader* Dxf::in(const string &file)
{
	DxfReader* inFile=new DxfReader;
	if(inFile) {
		if(!inFile->isFileExist(file.c_str())) {
			std::cerr<<"文件不存在！系统退出！"<<std::endl;
			exit(1);
		}
		else {
			inFile->open(file.c_str());
			if(inFile->isOpenFailed()) {
				std::cerr<<"无法打开文件！"<<std::endl;
				exit(1);
			}
			else return inFile;
		}
	}
	else {
		std::cerr<<"打开文件失败，不能分配足够的内存！"<<std::endl;
		exit(1);
	}
}

DxfWriter* Dxf::out(const string& file)
{
	DxfWriter* outFile=new DxfWriter;
	if(outFile) {
		outFile->open(file.c_str());
		if(outFile->isOpenFailed()) {
			std::cerr<<"创建文件失败!"<<std::endl;
			exit(1);
		}
		else
			return outFile;
	}
	else {
		std::cerr<<"打开文件失败，不能分配足够的内存！"<<std::endl;
		exit(1);
	}
}

ILineType* Dxf::loadLineType(const string& ltname)
{
	return DxfLineType::load(ltname);
}

void Dxf::setCurrentLayer(const string& layerName)
{
	DxfLayer::setDefaultLayer(layerName);
}

/**
* 如果图层名为空，则返回当前图层指针.
* 如果没有查找到该图层，则返回NULL.
*/
ILayer* Dxf::getLayerByName(const string& layerName)
{
	if(layerName.empty())
		return this->getCurrentLayer();

	map<string,DxfLayer*>::iterator itr=TablesSection::layers.find(layerName);
	if(itr!=TablesSection::layers.end())
		return itr->second;
	else
		return NULL;
}

ILayer* Dxf::getCurrentLayer()
{
	return DxfLayer::getDefaultLayer();
}

//void Dxf::getEntitiesOnLayer(const string& layerName, map<int,DxfEntity*>& entitiesOnLayer)
//{
//	if(layerName.empty()) return ;
//
//	map<int,DxfEntity*>::iterator itr;
//	for(itr=EntitiesSection::entities.begin();itr!=EntitiesSection::entities.end();itr++)
//		if(itr->second->getLayer()==layerName)
//			entitiesOnLayer.insert(*itr);
//}
//
//
//void Dxf::getAllLayers(map<string,DxfLayer*>& layers)
//{
//	layers=TablesSection::layers;
//}


EntityOnLayerIterator* Dxf::createEntityOnLayerIterator(const string& layerName)
{
	return new IteratorOfEntitiesOnLayer(EntitiesSection::entities,layerName);
}

AllLayersIterator* Dxf::createAllLayersIterator()
{
	return new IteratorOfAllLayers(TablesSection::layers);
}



//----------------------------------------------------------------------------------------------------------------------------
void Dxf::read(DxfReader *inFile)
{
	string value;

	while(!inFile->isEndOfFile()) {
		value=this->readBeginSection(inFile);

		if(value=="HEADER" || value=="CLASSES" || value=="OBJECTS")
			processNothing(inFile);//什么也不处理，只是简单的忽略，并读到section结束

		else if(value=="TABLES") {
			if(Section::tablesSection==NULL)
				Section::createSection("TABLES");

			Section::tablesSection->read(inFile);
		}
		else if(value=="BLOCKS") {
			if(Section::blocksSection==NULL)
				Section::createSection("BLOCKS");

			Section::blocksSection->read(inFile);
		}
		else if(value=="ENTITIES") {
			if(Section::entitiesSection==NULL)
				Section::createSection("ENTITIES");

			Section::entitiesSection->read(inFile);
		}
		else if(value=="EOF") break;
		else {
			std::cerr<<"第"<<inFile->getCurrentLine()<<"行与预期的数据不符"<<std::endl;
			std::cerr<<"section类型未知!"<<std::endl;
			std::cerr<<"程序退出......"<<std::endl;
			exit(1);
		}
	}

	//收尾工作
	delete inFile;
	inFile=NULL;
}


void Dxf::write(DxfWriter *outFile)
{

	this->writeHeader(outFile);//写Header Section

	//按照顺序写dxf文件

	if(Section::tablesSection==NULL)
		Section::createSection("TABLES");
	Section::tablesSection->write(outFile);

	if(Section::blocksSection==NULL)
		Section::createSection("BLOCKS");
	Section::blocksSection->write(outFile);

	if(Section::entitiesSection==NULL) 
		Section::createSection("ENTITIES");
	Section::entitiesSection->write(outFile);

	this->writeObjects(outFile);

	outFile->writeString(0,"EOF");

	//收尾工作
	delete outFile;
	outFile=NULL;
}




//以下的为实体、图层、线型、块的创建和添加方法

void Dxf::addEntity(IEntity *ent)
{
	if(ent) {
		if(Section::entitiesSection==NULL)
			Section::createSection("ENTITIES");
		Section::entitiesSection->addEntity(dynamic_cast<DxfEntity*>(ent));
	}
}

void Dxf::removeEntity(IEntity* ent)
{
	if(ent==NULL) return;

	Section::entitiesSection->removeEntity(ent->getEntityNum());
}

void Dxf::addTable( ITable *tbl)
{
	if(tbl) {
		if(Section::tablesSection==NULL)
			Section::createSection("TABLES");
		Section::tablesSection->addTable(dynamic_cast<DxfTable*>(tbl));
	}
}

void Dxf::removeTable(ITable* tbl)
{
	if(tbl==NULL) return;

	if(tbl->typeId()==LAYER) {
		if(static_cast<DxfLayer*>(tbl)->isDefaultLayer()) 
			std::cerr<<"不能删除当前图层"<<std::endl;
		else 
			Section::tablesSection->removeLayer(tbl->getName());
	}
}

void Dxf::addBlock( IBlock *blk)
{
	if(blk) {
		if(Section::blocksSection==NULL)
			Section::createSection("BLOCKS");
		Section::blocksSection->addBlock(dynamic_cast<DxfBlock*>(blk));
	}
}

void Dxf::removeBlock( IBlock* blk)
{
	if(blk==NULL) return ;
	map<int,DxfEntity*>::iterator itr;
	for(itr=EntitiesSection::entities.begin();itr!=EntitiesSection::entities.end();itr++)
		if(itr->second->typeId()==INSERT)//是insert类型
			if(static_cast<DxfInsert*>(itr->second)->getBlockName()==blk->getBlockName()) { //且是根据块定义创建的insert
				delete itr->second;			
				EntitiesSection::entities.erase(itr);//删除
			}

			Section::blocksSection->removeBlock(blk->getBlockName());
}

void Dxf::addLine( ILine* line)
{
	if(line) {
		IEntity* ent=NULL;
		line->transform(ent);
		this->addEntity(ent);
	}
}

ILine* Dxf::createLine()
{
	DxfLine* line=DxfLine::createLine();
	if(line) return line;
	else return NULL;
}

ILine* Dxf::createLine(const DxfPoint &startPt, const DxfPoint &endPt)
{
	DxfLine* line=DxfLine::createLine(startPt,endPt);
	if(line) return line;
	else return NULL;
}

ILine* Dxf::createLine(const double p1x,const double p1y,const double p1z,const double p2x,const double p2y,const double p2z)
{
	DxfLine* line=DxfLine::createLine(p1x,p1y,p1z,p2x,p2y,p2z);
	if(line) return line;
	else return NULL;
}

void Dxf::addArc( IArc* arc)
{
	if(arc) {
		IEntity* ent=NULL;
		arc->transform(ent);
		this->addEntity(ent);
	}
}

IArc* Dxf::createArc()
{
	DxfArc* arc=DxfArc::createArc();
	if(arc) return arc;
	else return NULL;
}

IArc* Dxf::createArc(const DxfPoint& center,double radius,double startAngle,double endAngle)
{
	DxfArc* arc=DxfArc::createArc(center,radius,startAngle,endAngle);
	if(arc) return arc;
	else return NULL;
}

IArc* Dxf::createArc(double xCenter,double yCenter,double zCenter,double radius,double startAngle,double endAngle)
{
	DxfArc* arc=DxfArc::createArc(xCenter,yCenter,zCenter,radius,startAngle,endAngle);
	if(arc)  return arc;
	else return NULL;
}

void Dxf::addCircle( ICircle* circle)
{
	if(circle) {
		IEntity* ent=NULL;
		circle->transform(ent);
		this->addEntity(ent);
	}
}

ICircle* Dxf::createCircle()
{
	DxfCircle* circle=DxfCircle::createCircle();
	if(circle) return circle;
	else return NULL;
}

ICircle* Dxf::createCircle(const DxfPoint& center,double radius)
{
	DxfCircle* circle=DxfCircle::createCircle(center,radius);
	if(circle) return circle;
	else return NULL;
}

ICircle* Dxf::createCircle(double x,double y,double z,double radius)
{
	DxfCircle* circle=DxfCircle::createCircle(x,y,z,radius);
	if(circle) return circle;
	else return NULL;
}

void Dxf::addEllipse( IEllipse* ellipse)
{
	if(ellipse) {
		IEntity* ent=NULL;
		ellipse->transform(ent);
		this->addEntity(ent);
	}
}

IEllipse* Dxf::createEllipse()
{
	DxfEllipse* ellipse=DxfEllipse::createEllipse();
	if(ellipse) return ellipse;
	else return NULL;
}

IEllipse* Dxf::createEllipse(const DxfPoint& center,const DxfPoint& endPoint,double ratio,double startAngle,double endAngle)
{
	DxfEllipse* ellipse=DxfEllipse::createEllipse(center,endPoint,ratio,startAngle,endAngle);
	if(ellipse) return ellipse;
	else return NULL;
}

void Dxf::addInsert( IInsert* insert)
{
	if(insert) {
		IEntity* ent=NULL;
		insert->transform(ent);
		this->addEntity(ent);
	}
}

IInsert* Dxf::createInsert()
{
	DxfInsert* insert=DxfInsert::createInsert();
	if(insert) return insert;
	else return NULL;
}

IInsert* Dxf::createInsert(const string& blockName,const DxfPoint& insertPoint,double xscale,double yscale,double zscale,double rotationAngle)
{
	DxfInsert* insert=DxfInsert::createInsert(blockName,insertPoint,xscale,yscale,zscale,rotationAngle);
	if(insert) return insert;
	else return NULL;
}


void Dxf::addText( IText* text)
{
	if(text) {
		IEntity* ent=NULL;
		text->transform(ent);
		this->addEntity(ent);
	}
}

IText* Dxf::createText()
{
	DxfText* text=DxfText::createText();
	if(text) return text;
	else return NULL;
}

IText* Dxf::createText(const DxfPoint& insertPoint,const DxfPoint& alignPoint,double height,double xScaleFactor,int hJustification,int vJustification,const string& styleName,const string& text,double rotationAngle)
{
	DxfText* txt=DxfText::createText(insertPoint,alignPoint,height,xScaleFactor,hJustification,vJustification,styleName,text,rotationAngle);
	if(txt) return txt;
	else return NULL;
}

IText* Dxf::createText(double ix,double iy,double iz,double ax,double ay,double az,double height,double xScaleFactor,int hJustification,int vJustification,const string& styleName,const string& text,double rotationAngle)
{
	DxfText* txt=DxfText::createText(ix,iy,iz,ax,ay,az,height,xScaleFactor,hJustification,vJustification,styleName,text,rotationAngle);
	if(txt) return txt;
	else return NULL;
}

void Dxf::addMText( IMText* mtext)
{
	if(mtext) {
		IEntity* ent=NULL;
		mtext->transform(ent);
		this->addEntity(ent);
	}
}

IMText* Dxf::createMText()
{
	DxfMText* mtext=DxfMText::createMText();
	if(mtext) return mtext;
	else return NULL;
}

IMText* Dxf::createMText(const DxfPoint& insertPoint,double height,double width,int attachPoint,int drawDirection,const string& text,const string& styleName,double rotationAngle)
{
	DxfMText* mtext=DxfMText::createMText(insertPoint,height,width,attachPoint,drawDirection,text,styleName,rotationAngle);
	if(mtext) return mtext;
	else return NULL;
}
IMText* Dxf::createMText(double ix,double iy,double iz,double height,double width,int attachPoint,int drawDirection,const string& text,const string& styleName,double rotationAngle)
{
	DxfMText* mtext=DxfMText::createMText(ix,iy,iz,height,width,attachPoint,drawDirection,text,styleName,rotationAngle);
	if(mtext) return mtext;
	else return NULL;
}


void Dxf::addVertex( IVertex* vertex)
{
	if(vertex) {
		IEntity* ent=NULL;
		vertex->transform(ent);
		this->addEntity(ent);
	}
}

IVertex* Dxf::createVertex()
{
	DxfVertex* vertex=DxfVertex::createVertex();
	if(vertex) return vertex;
	else return NULL;
}

IVertex* Dxf::createVertex(double x,double y,double z,double startWidth,double endWidth)
{
	DxfVertex* vertex=DxfVertex::createVertex(x,y,z,startWidth,endWidth);
	if(vertex) return vertex;
	else return NULL;
}

IVertex* Dxf::createVertex(const DxfPoint& point,double startWidth,double endWidth)
{
	DxfVertex* vertex=DxfVertex::createVertex(point,startWidth,endWidth);
	if(vertex) return vertex;
	else return NULL;
}

void Dxf::addPolyLine( ILWPolyLine* pline)
{
	if(pline) {
		IEntity* ent=NULL;
		pline->transform(ent);
		this->addEntity(ent);
	}
}

ILWPolyLine* Dxf::createPolyLine()
{
	DxfLWPolyLine* pline=DxfLWPolyLine::createLWPolyLine();
	if(pline) return pline;
	else return NULL;
}

void Dxf::addLayer( ILayer* layer)
{
	if(layer) {
		ITable* tbl=NULL;
		layer->transform(tbl);
		this->addTable(tbl);
	}
}

ILayer* Dxf::createLayer()
{
	//存在一个内存管理方面的漏洞，如果直接采用默认构造函数的默认值，
	//则返回的指针地址和原来使用默认参数的对象的指针地址是一样，
	//这样将会修改原有的值，而不是新建了一个指针
	//解决方法：不使用默认的参数，明确指定一个图层名称，命名采用“图层n”的方式

	std::stringstream ss;
	ss<<"图层";
	ss<<TablesSection::layers.size();

	string s;
	ss>>s;

	DxfLayer* layer=DxfLayer::createLayer(s);


	if(layer) return layer;
	else return NULL;
}

ILayer* Dxf::createLayer(const string& name,int color,const string& ltype,int flag)
{
	DxfLayer* layer=DxfLayer::createLayer(name,color,ltype,flag);
	if(layer) return layer;
	else return NULL;
}

//void Dxf::addLineType( DxfLineType* ltype)
//{
//	if(ltype) 
//		this->addTable(ltype);
//}

//DxfLineType* Dxf::createLineType()
//{
//	DxfLineType* ltype=DxfLineType::createLineType();
//	if(ltype) return ltype;
//	else return NULL;
//}

//DxfLineType* Dxf::createLineType(const string& ltypeName,const string& description,int elementsCount,double totalLength)
//{
//	DxfLineType* ltype=DxfLineType::createLineType(ltypeName,description,elementsCount,totalLength);
//	if(ltype) return ltype;
//	else return NULL;
//}

IBlock* Dxf::createBlock()
{
	DxfBlock* block=DxfBlock::createBlock();

	if(block) return block;
	else return NULL;
}

IBlock* Dxf::createBlock(const string& name,int flag,double xx,double yy,double zz)
{
	DxfBlock* block=DxfBlock::createBlock(name,flag,xx,yy,zz);
	if(block) return block;
	else return NULL;
}



//Header Section
void Dxf::writeHeader(DxfWriter* outFile)
{
	outFile->writeString(0,"SECTION");
	outFile->writeString(2,"HEADER");

	outFile->writeString(9,"$ACADVER");
	outFile->writeString(1,"AC1015");

	outFile->writeString(9, "$HANDSEED");
	outFile->writeHex(5, 0xFFFF);

	outFile->writeString(9, "$INSUNITS");
	outFile->writeInt(70, 4);

	outFile->writeString(9, "$DIMEXE");
	outFile->writeReal(40, 1.25);

	outFile->writeString(9, "$TEXTSTYLE");
	outFile->writeString(7, "Standard");

	outFile->writeString(9, "$LIMMIN");
	outFile->writeReal(10, 0.0);
	outFile->writeReal(20, 0.0);

	outFile->writeString(9,"$CLAYER");
	outFile->writeString(8,"0");

	outFile->writeString(9,"$CELTYPE");
	outFile->writeString(6,"ByLayer");

	outFile->writeString(0,"ENDSEC");
}


//Objects Section
void Dxf::writeObjects(DxfWriter* outFile)
{
	//object节开始
	outFile->writeString(0,"SECTION");
	outFile->writeString(2,"OBJECTS");
	outFile->writeString(0,"DICTIONARY");
	outFile->writeHex(5,0xC);//cad2000固定值
	outFile->writeString(100,"AcDbDictionary");
	outFile->writeInt(281,1);

	//outFile->writeString(3,"ACAD_COLOR");
	//outFile->writeHex(350,0x6B);//cad2000固定值

	outFile->writeString(3,"ACAD_GROUP");
	outFile->writeHex(350,0xD);//cad2000固定值

	outFile->writeString(3,"ACAD_LAYOUT");
	outFile->writeHex(350,0x1A);

	outFile->writeString(3,"ACAD_MLINESTYLE");
	outFile->writeHex(350,0x17);//cad2000固定值

	outFile->writeString(3,"ACAD_PLOTSETTINGS");
	outFile->writeHex(350,0x19);

	outFile->writeString(3,"ACAD_PLOTSTYLENAME");
	outFile->writeHex(350,0xE);

	/*outFile->writeString(3,"ACAD_TABLESTYLE");
	outFile->writeHex(350,0x7E);*/

	outFile->writeString(3,"AcDbVariableDictionary");
	outFile->writeHex(350,0x5E);

	//------------------------------------------

	//ACAD_COLOR
	/*outFile->writeString(0,"DICTIONARY");
	outFile->writeHex(5,0x6B);
	outFile->writeString(102,"{ACAD_REACTORS");
	outFile->writeHex(330,0xC);
	outFile->writeString(102,"}");
	outFile->writeHex(330,0xC);
	outFile->writeString(100,"AcDbDictionary");
	outFile->writeInt(280,1);*/

	//ACAD_GROUP
	outFile->writeString(0,"DICTIONARY");
	outFile->writeHex(5,0xD);//cad2000固定值
	outFile->writeString(102,"{ACAD_REACTORS");
	outFile->writeHex(330,0xC);
	outFile->writeString(102,"}");
	outFile->writeHex(330,0xC);
	outFile->writeString(100,"AcDbDictionary");
	outFile->writeInt(280,1);

	//ACDBDICTIONARYWDFLT
	outFile->writeString(0,"ACDBDICTIONARYWDFLT");
	outFile->writeHex(5,0xE);//cad2000固定值
	outFile->writeString(102,"{ACAD_REACTORS");
	outFile->writeHex(330,0xC);
	outFile->writeString(102,"}");
	outFile->writeHex(330,0xC);
	outFile->writeString(100,"AcDbDictionary");
	outFile->writeInt(281,1);
	outFile->writeString(3,"Normal");
	outFile->writeHex(350,0xF);//cad2000固定值
	outFile->writeString(100,"AcDbDictionaryWithDefault");
	outFile->writeHex(340,0xF);//cad2000固定值

	//ACDBPLACEHOLDER
	outFile->writeString(0,"ACDBPLACEHOLDER");
	outFile->writeHex(5,0xF);//cad2000固定值
	outFile->writeString(102,"{ACAD_REACTORS");
	outFile->writeHex(330,0xE);
	outFile->writeString(102,"}");
	outFile->writeHex(330,0xE);

	//ACAD_MLINESTYLE
	outFile->writeString(0,"DICTIONARY");
	outFile->writeHex(5,0x17);//cad2000固定值
	outFile->writeString(102,"{ACAD_REACTORS");
	outFile->writeHex(330,0xC);
	outFile->writeString(102,"}");
	outFile->writeHex(330,0xC);
	outFile->writeString(100,"AcDbDictionary");
	outFile->writeInt(281,1);
	outFile->writeString(3,"Standard");
	outFile->writeHex(350,0x18);//cad2000固定值

	//Standard---MLINESTYLE
	outFile->writeString(0,"MLINESTYLE");
	outFile->writeHex(5,0x18);
	outFile->writeString(102,"{ACAD_REACTORS");
	outFile->writeHex(330,0x17);
	outFile->writeString(102,"}");
	outFile->writeHex(330,0x17);
	outFile->writeString(100,"AcDbMlineStyle");
	outFile->writeString(2,"STANDARD");
	outFile->writeInt(70,0);
	outFile->writeString(3,"");
	outFile->writeInt(62,256);
	outFile->writeReal(51,90.0);
	outFile->writeReal(52,90.0);
	outFile->writeInt(71,2);
	outFile->writeReal(49,0.5);
	outFile->writeInt(62,256);
	outFile->writeString(6,"BYLAYER");
	outFile->writeReal(49,-0.5);
	outFile->writeInt(62,256);
	outFile->writeString(6,"BYLAYER");

	//ACAD_PLOTSETTINGS
	outFile->writeString(0,"DICTIONARY");
	outFile->writeHex(5,0x19);//cad2000固定值
	outFile->writeString(102,"{ACAD_REACTORS");
	outFile->writeHex(330,0xC);
	outFile->writeString(102,"}");
	outFile->writeHex(330,0xC);
	outFile->writeString(100,"AcDbDictionary");
	outFile->writeInt(281,1);

	//ACAD_LAYOUT
	outFile->writeString(0,"DICTIONARY");
	outFile->writeHex(5,0x1A);//cad2000固定值
	//outFile->writeString(102,"{ACAD_REACTORS");
	//outFile->writeHex(330,0xC);
	//outFile->writeString(102,"}");
	//outFile->writeHex(330,0xC);
	outFile->writeString(100,"AcDbDictionary");
	outFile->writeInt(281,1);
	outFile->writeString(3,"Model");
	outFile->writeHex(350,0x22);//cad2000固定值
	outFile->writeString(3,"布局1");
	outFile->writeHex(350,0xD3);//cad2000固定值
	outFile->writeString(3,"布局2");
	outFile->writeHex(350,0xD7);//cad2000固定值


	//Model---ACAD_LAYOUT
	outFile->writeString(0,"LAYOUT");
	outFile->writeHex(5,0x22);
	outFile->writeString(102,"{ACAD_XDICTIONARY");
	outFile->writeHex(360,0xF5);//cad2000固定值
	outFile->writeString(102,"}");
	outFile->writeString(102,"{ACAD_REACTORS");
	outFile->writeHex(360,0x1A);
	outFile->writeString(102,"}");
	outFile->writeHex(330,0x1A);
	outFile->writeString(100,"AcDbPlotSettings");
	outFile->writeString(1,"");
	outFile->writeString(2,"none_device");
	outFile->writeString(4,"ISO_A4_(210.00_x_297.00_MM)");
	outFile->writeString(6,"");
	outFile->writeReal( 40, 7.5);
	outFile->writeReal( 41, 20.0);
	outFile->writeReal( 42, 7.5);
	outFile->writeReal( 43, 20.0);
	outFile->writeReal( 44, 210.0);
	outFile->writeReal( 45, 297.0);
	outFile->writeReal( 46, 11.54999923706053);
	outFile->writeReal( 47, -13.65000009536743);
	outFile->writeReal( 48, 0.0);
	outFile->writeReal( 49, 0.0);
	outFile->writeReal(140, 0.0);
	outFile->writeReal(141, 0.0);
	outFile->writeReal(142, 1.0);
	outFile->writeReal(143, 8.704084754739808);
	outFile->writeInt( 70, 11952);
	outFile->writeInt( 72, 1);
	outFile->writeInt( 73, 0);
	outFile->writeInt( 74, 0);
	outFile->writeString(  7, "");
	outFile->writeInt( 75, 0);
	outFile->writeReal(147, 0.1148885871608098);
	outFile->writeReal(148, 0.0);
	outFile->writeReal(149, 0.0);
	outFile->writeString(100, "AcDbLayout");
	outFile->writeString(  1, "Model");
	outFile->writeInt( 70, 1);
	outFile->writeInt( 71, 0);
	outFile->writeReal( 10, 0.0);
	outFile->writeReal( 20, 0.0);
	outFile->writeReal( 11, 12.0);
	outFile->writeReal( 21, 9.0);
	outFile->writeReal( 12, 0.0);
	outFile->writeReal( 22, 0.0);
	outFile->writeReal( 32, 0.0);
	outFile->writeReal( 14, 0.0);
	outFile->writeReal( 24, 0.0);
	outFile->writeReal( 34, 0.0);
	outFile->writeReal( 15, 0.0);
	outFile->writeReal( 25, 0.0);
	outFile->writeReal( 35, 0.0);
	outFile->writeReal(146, 0.0);
	outFile->writeReal( 13, 0.0);
	outFile->writeReal( 23, 0.0);
	outFile->writeReal( 33, 0.0);
	outFile->writeReal( 16, 1.0);
	outFile->writeReal( 26, 0.0);
	outFile->writeReal( 36, 0.0);
	outFile->writeReal( 17, 0.0);
	outFile->writeReal( 27, 1.0);
	outFile->writeReal( 37, 0.0);
	outFile->writeInt( 76, 0);
	outFile->writeHex(330,0x1F);//参照的块句柄

	//布局1---ACAD_LAYOUT
	outFile->writeString(0,"LAYOUT");
	outFile->writeHex(5,0xD3);
	outFile->writeString(102,"{ACAD_XDICTIONARY");
	outFile->writeHex(360,0xF7);//cad2000固定值
	outFile->writeString(102,"}");
	outFile->writeString(102,"{ACAD_REACTORS");
	outFile->writeHex(360,0x1A);
	outFile->writeString(102,"}");
	outFile->writeHex(330,0x1A);
	outFile->writeString(100,"AcDbPlotSettings");
	outFile->writeString(1,"");
	outFile->writeString(2,"C:\\Documents and Settings\\basas\\Application Data\\Autodesk\\AutoCAD 2005\\R16.1\\enu\\plotters\\Default Windows System Printer.pc3 \
						   ");
	outFile->writeString(4,"");
	outFile->writeString(6,"");
	outFile->writeReal( 40, 0.0);
	outFile->writeReal( 41, 0.0);
	outFile->writeReal( 42, 0.0);
	outFile->writeReal( 43, 0.0);
	outFile->writeReal( 44, 0.0);
	outFile->writeReal( 45, 0.0);
	outFile->writeReal( 46, 0.0);
	outFile->writeReal( 47, 0.0);
	outFile->writeReal( 48, 0.0);
	outFile->writeReal( 49, 0.0);
	outFile->writeReal(140, 0.0);
	outFile->writeReal(141, 0.0);
	outFile->writeReal(142, 1.0);
	outFile->writeReal(143, 1.0);
	outFile->writeInt( 70, 688);
	outFile->writeInt( 72, 0);
	outFile->writeInt( 73, 0);
	outFile->writeInt( 74, 5);
	outFile->writeString(  7, "");
	outFile->writeInt( 75, 16);
	outFile->writeReal(147, 1.0);
	outFile->writeReal(148, 0.0);
	outFile->writeReal(149, 0.0);
	outFile->writeString(100, "AcDbLayout");
	outFile->writeString(  1, "布局1");
	outFile->writeInt( 70, 1);
	outFile->writeInt( 71, 1);
	outFile->writeReal( 10, 0.0);
	outFile->writeReal( 20, 0.0);
	outFile->writeReal( 11, 12.0);
	outFile->writeReal( 21, 9.0);
	outFile->writeReal( 12, 0.0);
	outFile->writeReal( 22, 0.0);
	outFile->writeReal( 32, 0.0);
	outFile->writeReal( 14, 0.0);
	outFile->writeReal( 24, 0.0);
	outFile->writeReal( 34, 0.0);
	outFile->writeReal( 15, 0.0);
	outFile->writeReal( 25, 0.0);
	outFile->writeReal( 35, 0.0);
	outFile->writeReal(146, 0.0);
	outFile->writeReal( 13, 0.0);
	outFile->writeReal( 23, 0.0);
	outFile->writeReal( 33, 0.0);
	outFile->writeReal( 16, 1.0);
	outFile->writeReal( 26, 0.0);
	outFile->writeReal( 36, 0.0);
	outFile->writeReal( 17, 0.0);
	outFile->writeReal( 27, 1.0);
	outFile->writeReal( 37, 0.0);
	outFile->writeInt( 76, 0);
	outFile->writeHex(330,0xD2);//参照的块句柄

	//布局2---ACAD_LAYOUT
	outFile->writeString(0,"LAYOUT");
	outFile->writeHex(5,0xD7);
	outFile->writeString(102,"{ACAD_XDICTIONARY");
	outFile->writeHex(360,0xF9);//cad2000固定值
	outFile->writeString(102,"}");
	outFile->writeString(102,"{ACAD_REACTORS");
	outFile->writeHex(360,0x1A);
	outFile->writeString(102,"}");
	outFile->writeHex(330,0x1A);
	outFile->writeString(100,"AcDbPlotSettings");
	outFile->writeString(1,"");
	outFile->writeString(2,"C:\\Documents and Settings\\basas\\Application Data\\Autodesk\\AutoCAD 2005\\R16.1\\enu\\plotters\\Default Windows System Printer.pc3 \
						   ");
	outFile->writeString(4,"");
	outFile->writeString(6,"");
	outFile->writeReal( 40, 0.0);
	outFile->writeReal( 41, 0.0);
	outFile->writeReal( 42, 0.0);
	outFile->writeReal( 43, 0.0);
	outFile->writeReal( 44, 0.0);
	outFile->writeReal( 45, 0.0);
	outFile->writeReal( 46, 0.0);
	outFile->writeReal( 47, 0.0);
	outFile->writeReal( 48, 0.0);
	outFile->writeReal( 49, 0.0);
	outFile->writeReal(140, 0.0);
	outFile->writeReal(141, 0.0);
	outFile->writeReal(142, 1.0);
	outFile->writeReal(143, 1.0);
	outFile->writeInt( 70, 688);
	outFile->writeInt( 72, 0);
	outFile->writeInt( 73, 0);
	outFile->writeInt( 74, 5);
	outFile->writeString(  7, "");
	outFile->writeInt( 75, 16);
	outFile->writeReal(147, 1.0);
	outFile->writeReal(148, 0.0);
	outFile->writeReal(149, 0.0);
	outFile->writeString(100, "AcDbLayout");
	outFile->writeString(  1, "布局2");
	outFile->writeInt( 70, 1);
	outFile->writeInt( 71, 1);
	outFile->writeReal( 10, 0.0);
	outFile->writeReal( 20, 0.0);
	outFile->writeReal( 11, 12.0);
	outFile->writeReal( 21, 9.0);
	outFile->writeReal( 12, 0.0);
	outFile->writeReal( 22, 0.0);
	outFile->writeReal( 32, 0.0);
	outFile->writeReal( 14, 0.0);
	outFile->writeReal( 24, 0.0);
	outFile->writeReal( 34, 0.0);
	outFile->writeReal( 15, 0.0);
	outFile->writeReal( 25, 0.0);
	outFile->writeReal( 35, 0.0);
	outFile->writeReal(146, 0.0);
	outFile->writeReal( 13, 0.0);
	outFile->writeReal( 23, 0.0);
	outFile->writeReal( 33, 0.0);
	outFile->writeReal( 16, 1.0);
	outFile->writeReal( 26, 0.0);
	outFile->writeReal( 36, 0.0);
	outFile->writeReal( 17, 0.0);
	outFile->writeReal( 27, 1.0);
	outFile->writeReal( 37, 0.0);
	outFile->writeInt( 76, 0);
	outFile->writeHex(330,0xD6);//参照的块句柄

	//AcDbVariableDictionary
	outFile->writeString(0,"DICTIONARY");
	outFile->writeHex(5,0x5E);//cad2000固定值
	outFile->writeString(102,"{ACAD_REACTORS");
	outFile->writeHex(330,0xC);
	outFile->writeString(102,"}");
	outFile->writeHex(330,0xC);
	outFile->writeString(100,"AcDbDictionary");
	outFile->writeInt(280,1);
	//outFile->writeString(3,"CTABLESTYLE");
	//outFile->writeHex(350,0x84);//cad2000固定值
	outFile->writeString(3,"DIMASSOC");
	outFile->writeHex(350,0x5F);//cad2000固定值
	outFile->writeString(3,"HIDETEXT");
	outFile->writeHex(350,0x63);//cad2000固定值

	//DIMASSOC---DICTIONARYVAR
	outFile->writeString(0,"DICTIONARYVAR");
	outFile->writeHex(5,0x5F);//cad2000固定值
	outFile->writeString(102,"{ACAD_REACTORS");
	outFile->writeHex(330,0x5E);
	outFile->writeString(102,"}");
	outFile->writeHex(330,0x5E);
	outFile->writeString(100,"DictionaryVariables");
	outFile->writeInt(280,0);
	outFile->writeString(1,"2");

	//HIDETEXT---DICTIONARYVAR
	outFile->writeString(0,"DICTIONARYVAR");
	outFile->writeHex(5,0x63);//cad2000固定值
	outFile->writeString(102,"{ACAD_REACTORS");
	outFile->writeHex(330,0x5E);
	outFile->writeString(102,"}");
	outFile->writeHex(330,0x5E);
	outFile->writeString(100,"DictionaryVariables");
	outFile->writeInt(280,0);
	outFile->writeString(1,"1");

	outFile->writeString(0,"ENDSEC");
}



string Dxf::readBeginSection(DxfReader* inFile)
{
	int groupCode=0;
	string value;

	inFile->readGroupCode(groupCode);
	inFile->readString(value);
	if(groupCode==0 && value=="SECTION")//section开始
	{
		inFile->readGroupCode(groupCode);
		inFile->readString(value);
		if(groupCode==2) {
			return value;
		}
		else {
			std::cerr<<"第"<<inFile->getCurrentLine()-1<<"行与预期的数据不符"<<std::endl;
			std::cerr<<"此处的组码应为:"<<2<<",实际组码为:"<<groupCode<<std::endl;
			std::cerr<<"程序退出......"<<std::endl;
			exit(1);
		}
	}
	else if(groupCode==0 && value=="EOF") 
		return "EOF";
	else {
		std::cerr<<"第"<<inFile->getCurrentLine()-1<<"行与预期的数据不符"<<std::endl;
		std::cerr<<"section节开始部分出现错误!"<<std::endl;
		std::cerr<<"程序退出......"<<std::endl;		
		exit(1);
	}
}

bool Dxf::readEndSection(DxfReader* inFile)
{
	int groupCode=0;
	string value;
	inFile->readGroupCode(groupCode);
	inFile->readString(value);
	if(groupCode==0 && value=="ENDSEC" )
		return true;
	else
		return false;
}

void Dxf::processNothing(DxfReader* inFile)
{
	int groupCode=0;
	string value;
	do {
		inFile->readGroupCode(groupCode);
		inFile->readString(value);
	}
	while(!(groupCode==0 && value=="ENDSEC"));
}





//迭代器类
//
//IteratorOfEntitiesOnLayer::IteratorOfEntitiesOnLayer(map<int,DxfEntity*>& entities,const string& layerName):_entities(entities),_layerName(layerName)
//{
//	first();
//}


void IteratorOfEntitiesOnLayer::first()
{
	currentItr=this->_entities.begin();
	map<int,DxfEntity*>::iterator itr=this->currentItr;

	do {
		if(itr->second->getLayer()==_layerName)
			break;
		itr++;

	}while(itr!=this->_entities.end());

	currentItr=itr;
}

void IteratorOfEntitiesOnLayer::next()
{
	map<int,DxfEntity*>::iterator itr=++(this->currentItr);
	if(itr==this->_entities.end()) {
		return;
	}

	do {
		if(itr->second->getLayer()==_layerName)
			break;
		itr++;

	}while(itr!=this->_entities.end());

	this->currentItr=itr;
}

//bool IteratorOfEntitiesOnLayer::isDone() const
//{
//	if(currentItr==this->_entities.end()) 
//		return true;
//	else 
//		return false;
//}
//
//IEntity* IteratorOfEntitiesOnLayer::currentItem() const
//{
//	return this->currentItr->second;
//}

//IteratorOfAllLayers::IteratorOfAllLayers(map<string,DxfLayer*>& layers):_layers(layers)
//{
//	first();
//}
//
//void IteratorOfAllLayers::first()
//{
//	this->currentItr=this->_layers.begin();
//}
//
//void IteratorOfAllLayers::next()
//{
//	if(currentItr!=this->_layers.end())
//		currentItr++;
//}
//
//bool IteratorOfAllLayers::isDone() const
//{
//	if(currentItr!=this->_layers.end())
//		return false;
//	else 
//		return true;
//}
//
//
//ILayer* IteratorOfAllLayers::currentItem() const
//{
//	return currentItr->second;
//}
//
//IteratorOfAllLayers::~IteratorOfAllLayers() 
//{
//	//this->currentItr=this->_layers.end();
//	std::cout<<"调用~IteratorOfAllLayers() "<<std::endl;
//}
//
//
//IteratorOfEntitiesOnLayer::~IteratorOfEntitiesOnLayer()
//{ 
//	//this->currentItr=this->_entities.end();
//	std::cout<<"调用~IteratorOfEntitiesOnLayer()"<<std::endl;
//}

namespace dxf {

	DXF_DLL_API IDxf* createDxf()
	{
		return new Dxf();
	}
}
