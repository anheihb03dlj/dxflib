#include <dxf/Base.h>
#include <dxf/block/DxfBlock.h>

#include <dxf/entity/IEntity.h>
#include <dxf/entity/DxfEntity.h>

#include <dxf/table/ILayer.h>
#include <dxf/table/DxfLayer.h>

#include <dxf/section/BlocksSection.h>
#include <dxf/section/EntitiesSection.h>
#include <dxf/DxfReader.h>
#include <dxf/DxfWriter.h>

#include <iostream>
#include <sstream>

//静态成员
bool DxfBlock::first=true;


/**
 * 当block初始化的时候，默认设置,
 * <ol>
 * <li>基点X坐标--0
 * <li>基点Y坐标--0
 * <li>基点Z坐标-0
 * <li>块定义标志--0
 * </ol>
 * @see BlockType
 */
//DxfBlock::DxfBlock(void)
//{
//	this->x=0;
//	this->y=0;
//	this->z=0;
//	this->flags=0;
//}


//DxfBlock::DxfBlock(const string& name,int flag,double xx,double yy,double zz)
//:blockName(name),flags(flag),x(xx),y(yy),z(zz)
//{
//
//}

DxfBlock::~DxfBlock(void)
{
#ifdef _DEBUG
	std::cout<<"调用DxfBlock::~DxfBlock(void)"<<std::endl;
#endif
	map<int,IEntity*>::iterator itr;
	for(itr=entities.begin();itr!=entities.end();) {
#ifdef _DEBUG
		std::cout<<"删除块中定义的实体"<<itr->second->typeId()<<std::endl;
#endif
		delete itr->second;
		entities.erase(itr++);
	}
}

void DxfBlock::setBlockName(const string &name)
{
	static int n=1;

	if(name.empty()) {                                                      //如果块名为空，则构造一个“块n”的名称
		std::stringstream ss;
		ss<<"块"<<n;
		n++;
		ss>>this->blockName;
	}
	else
		this->blockName=name;
}

//string DxfBlock::getBlockName()
//{
//	return this->blockName;
//}

//void DxfBlock::setFlag(int flag)
//{
//	this->flags=flag;
//}

//int DxfBlock::getFlag()
//{
//	return this->flags;
//}

//void DxfBlock::setBasePoint(double x, double y, double z)
//{
//	this->x=x;
//	this->y=y;
//	this->z=z;
//}

bool DxfBlock::isDefaultBlock()                                                       //判断是否为内部的默认块，一般不需要考虑，这些块只是作为内部使用，以及保证dxf文件的完整性
{
	string s=this->blockName;
	if(s=="*Model_Space" ||	s=="*Paper_Space" || s=="*Paper_Space0") {
		return true;
	}
	else {
		return false;
	}
}

void DxfBlock::add(IEntity* entity)
{
	if(entity) {
		this->entities.insert(map<int,IEntity*>::value_type(entity->getEntityNum(),entity));
		map<int,DxfEntity*>::iterator itr=EntitiesSection::entities.find(entity->getEntityNum());
		if(itr!=EntitiesSection::entities.end()) {
			EntitiesSection::entities.erase(itr);
		}
	}
}

void DxfBlock::remove(int entityNum)
{
	if(entityNum<0) return;

	map<int,IEntity*>::iterator itr=this->entities.find(entityNum);
	if(itr!=this->entities.end()) {
		//delete itr->second;
		this->entities.erase(itr);
	}
}

void DxfBlock::write(DxfWriter *outFile)
{
	if(this->blockName.empty()) {
		std::cerr<<"块名不能为空"<<std::endl;
		exit(1);
	}
	else {
		outFile->writeString(2,this->blockName);                                 //写入块名

		outFile->writeInt(70,this->flags);                                       //写入块标志(是否匿名块、外部引用等)--这个目前考虑有些欠妥，简单起见，最好不要设置这个值

		outFile->writeReal(10,this->x);                                          //写入块基点坐标
		outFile->writeReal(20,this->y);
		outFile->writeReal(30,this->z);
		
		outFile->writeString(3,this->blockName);                                 //写入块名

		if(this->flags==Anonymous)                                               //如果标记为匿名块，则写入组1
			outFile->writeString(1,"");

		map<int,IEntity*>::iterator itr;
		IReadWrite* pw=NULL;
		for(itr=this->entities.begin();itr!=this->entities.end();itr++) {         //循环写入实体数据(这个和dxf参考文档说的不一样，block中的实体也可以有句柄(也称为实体描述字))
			itr->second->transform(pw);
			pw->write(outFile);
			//itr->second->write(outFile);
		}

	}
}


void DxfBlock::read(DxfReader* inFile)
{
	DxfEntity* entity=NULL;
	bool isEndBlock=false;
	int groupCode;
	string value;
	
	inFile->readGroupCode(groupCode);                                            //定位于“0---BLOCK”位置
	inFile->readString(value);

	int i=0;
	double d=0.0;

	if(groupCode==0 && value=="BLOCK") {
		inFile->readGroupCode(groupCode);                                       //开始循环读取并设置block的各项数据
		while(/*groupCode!=0 && */!isEndBlock) {
			switch(groupCode) {
				case 2:
					inFile->readString(value);
					this->blockName=value;
					break;
				case 70:
					inFile->readInt(i);
					this->flags=i;
					break;
				case 10:
					inFile->readReal(d);
					this->x=d;
					break;
				case 20:
					inFile->readReal(d);
					this->y=d;
					break;
				case 30:
					inFile->readReal(d);
					this->z=d;
					break;
				case 3:
					inFile->readString(value);                                 //从3组开始，就是block中各实体的数据

					if(value==this->blockName) {                               //3组和2组的名称必须一样

						inFile->readGroupCode(groupCode);                      //一般情况下，3组以后，还有一“1--"" ”组，这个可以读取后忽略
						inFile->readString(value);

						if(groupCode==0)                                      
							inFile->goBack(2);

						inFile->readGroupCode(groupCode);                      //这个时候下一个组可能是“0--ENDBLK”或者“0--LINE”
						inFile->readString(value);
						inFile->goBack(2);
						while(!(groupCode==0 && value =="ENDBLK")) {

							entity=DxfEntity::createEntityByTypeName(value);   //根据实体类型名称创建实体

							if(entity==NULL) {
								std::cerr<<"第"<<inFile->getCurrentLine()<<"行与预期的数据不符"<<std::endl;
								std::cerr<<"发生异常，不能分配足够的内存"<<std::endl;
								std::cerr<<"程序退出......"<<std::endl;
								exit(1);
							}

							entity->read(inFile);                               //读实体部分内容
							this->add(entity);	

							inFile->readGroupCode(groupCode);
							inFile->readString(value);
							inFile->goBack(2);

						}

						if(groupCode==0 && value =="ENDBLK") {                 //如果组码为0，且组值为“ENDBLK”，则标识该块已经结束，停止读取数据
							isEndBlock=true;
							break; 
						}
					}
					else {
						std::cerr<<"第"<<inFile->getCurrentLine()<<"行与预期的数据不符"<<std::endl;
						std::cerr<<"组码2与组码3的块名不一致"<<std::endl;
						std::cerr<<"程序退出......"<<std::endl;
						exit(1);
					}
					
					break;

				default:
					inFile->readString(value);
					break;
			}
			inFile->readGroupCode(groupCode);
		}

		inFile->readString(value);                                              //向前读一行

	                                                                       	    //开始处理ENDBLK部分，简单的忽略，到"0--BLOCK"(也就是下一个块的开始位置)就停止

		while(!(groupCode==0 && value=="BLOCK") && !(groupCode==0 && value=="ENDSEC"))
		{
			inFile->readGroupCode(groupCode);
			inFile->readString(value);
		}

		inFile->goBack(2);
	}
	else {
		std::cerr<<"第"<<inFile->getCurrentLine()<<"行数据与预期数据不符"<<std::endl;
		std::cerr<<"block定义的开始不是BLOCK字样"<<std::endl;
		std::cerr<<"程序退出......"<<std::endl;
		exit(1);
	}
}


//静态方法

/**
 * 当block初始化的时候,创建3个默认的系统内部块,
 * <ol>
 * <li>Model_Space
 * <li>Paper_Space
 * <li>Paper_Space0
 * </ol>
 * @see BlockType
 */
void DxfBlock::initial()
{
	createBlock();
}

/**
 * 当第一次创建block的时候，创建3个默认的系统内部块,
 * <ol>
 * <li>Model_Space
 * <li>Paper_Space
 * <li>Paper_Space0
 * </ol>
 */
DxfBlock* DxfBlock::createBlock(const string& name,int flag,double xx,double yy,double zz)
{
	DxfBlock* block;

	if(first) {//插入3个默认的块
		block=new DxfBlock("*Model_Space",0,0.0,0.0,0.0);
		if(block)
			BlocksSection::blocks.insert(map<string,DxfBlock*>::value_type("*Model_Space",block));

		block=new DxfBlock("*Paper_Space",0,0.0,0.0,0.0);
		if(block)
			BlocksSection::blocks.insert(map<string,DxfBlock*>::value_type("*Paper_Space",block));

		block=new DxfBlock("*Paper_Space0",0,0.0,0.0,0.0);
		if(block)
			BlocksSection::blocks.insert(map<string,DxfBlock*>::value_type("*Paper_Space0",block));
		
		first=false;
	}
	
	block=new DxfBlock(name,flag,xx,yy,zz);
	if(block)
		return block;
	else return NULL;
}


/**
 * 当第一次创建block的时候，创建3个默认的系统内部块,
 * <ol>
 * <li>Model_Space
 * <li>Paper_Space
 * <li>Paper_Space0
 * </ol>
 */
DxfBlock* DxfBlock::createBlock()
{
	DxfBlock* block;

	if(first) {//插入3个默认的块
		block=new DxfBlock("*Model_Space",0,0.0,0.0,0.0);
		if(block)
			BlocksSection::blocks.insert(map<string,DxfBlock*>::value_type("*Model_Space",block));

		block=new DxfBlock("*Paper_Space",0,0.0,0.0,0.0);
		if(block)
			BlocksSection::blocks.insert(map<string,DxfBlock*>::value_type("*Paper_Space",block));

		block=new DxfBlock("*Paper_Space0",0,0.0,0.0,0.0);
		if(block)
			BlocksSection::blocks.insert(map<string,DxfBlock*>::value_type("*Paper_Space0",block));
		
		first=false;
	}

	block=new DxfBlock;
	if(block) 
		return block;
	else return NULL;
}

int DxfBlock::countEntities()
{
	return entities.size(); 
}