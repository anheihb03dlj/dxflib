#include <dxf/entity/DxfEllipse.h>
#include <dxf/DxfReader.h>
#include <dxf/DxfWriter.h>

/**
 * 当ellipse初始化的时候，默认设置
 * <ol>
 * <li>短轴与长轴之比-- 0.6090456974522166
 * <li>起始角度--0.0
 * <li>终点角度--2*PI
 * </ol>
 */
//DxfEllipse::DxfEllipse() 
//{
//	this->ratio= 0.6090456974522166;
//	this->startAngle=0.0;
//	this->endAngle=2*PI;
//}
//
//DxfEllipse::DxfEllipse(const DxfPoint& cen,const DxfPoint& ept,double r,double sa,double ea)
//:center(cen),endPoint(ept),ratio(r),startAngle(sa),endAngle(ea)
//{
//
//}
//
//DxfEllipse::~DxfEllipse()
//{
//
//}
//
//
//void DxfEllipse::setCenter(const DxfPoint &pt)
//{
//	this->center=pt;
//}
//
//void DxfEllipse::setCenter(double x, double y, double z)
//{
//	this->center=DxfPoint(x,y,z);
//}
//
//DxfPoint DxfEllipse::getCenter()
//{
//	return this->center;
//}
//
//void DxfEllipse::getCenter(double &x, double &y, double &z)
//{
//	x=this->center.x;
//	y=this->center.y;
//	z=this->center.z;
//}
//
//void DxfEllipse::setEndPoint(const DxfPoint &pt)
//{
//	this->endPoint=pt;
//}
//
//void DxfEllipse::setEndPoint(double x, double y, double z)
//{
//	this->endPoint=DxfPoint(x,y,z);
//}
//
//DxfPoint DxfEllipse::getEndPoint()
//{
//	return this->endPoint;
//}
//
//void DxfEllipse::getEndPoint(double &x, double &y, double &z)
//{
//	x=this->endPoint.x;
//	y=this->endPoint.y;
//	z=this->endPoint.z;
//}
//
//void DxfEllipse::setRatio(double ratio)
//{
//	this->ratio=ratio;
//}
//
//void DxfEllipse::setStartAngle(double sa)
//{
//	this->startAngle=sa;
//}
//
//double DxfEllipse::getStartAngle()
//{
//	return this->startAngle;
//}
//
//void DxfEllipse::setEndAngle(double ea)
//{
//	this->endAngle=ea;
//}
//
//double DxfEllipse::getEndAngle()
//{
//	return this->endAngle;
//}

void DxfEllipse::write(DxfWriter *outFile)
{
	if(isDeleted()) return ;                                           //标记为删除，则不写入到dxf文件中

	DxfEntity::write(outFile);                                         //写入ellipse的颜色、图层、线型等数据

	double x,y,z;
	double ex,ey,ez;
	this->getCenter(x,y,z);                                            //获取ellipse的中心点及长轴端点坐标
	this->getEndPoint(ex,ey,ez);

	outFile->writeReal(10,x);                                          //写入中心点坐标
	outFile->writeReal(20,y);
	outFile->writeReal(30,z);

	outFile->writeReal(11,ex);                                         //写入长轴端点坐标
	outFile->writeReal(21,ey);
	outFile->writeReal(31,ez);

	outFile->writeReal(40,this->ratio);                                //写入短轴与长轴之比
	outFile->writeReal(41,this->startAngle);                           //写入椭圆起始角度
	outFile->writeReal(42,this->endAngle);                             //写入椭圆终点角度

	this->writeXData(outFile);                                    //写入扩展数据
}

void DxfEllipse::read(DxfReader* inFile)
{
	DxfEntity::read(inFile);                                           //读取并设置ellipse的颜色、图层、线型

	int groupCode=0;
	string value;

	double d=0.0;

	inFile->readGroupCode(groupCode);                                  //开始循环读取组码和组值，并设置ellipse的各项数据

	while(groupCode!=0) {
		switch(groupCode) {
			case 10:
				inFile->readReal(d);
				this->center.x=d;
				break;
			case 20:
				inFile->readReal(d);
				this->center.y=d;
				break;
			case 30:
				inFile->readReal(d);
				this->center.z=d;
				break;
			case 11:
				inFile->readReal(d);
				this->endPoint.x=d;
				break;
			case 21:
				inFile->readReal(d);
				this->endPoint.y=d;
				break;
			case 31:
				inFile->readReal(d);
				this->endPoint.z=d;
				break;
			case 40:
				inFile->readReal(d);
				this->ratio=d;
				break;
			case 41:
				inFile->readReal(d);
				this->startAngle=d;
				break;
			case 42:
				inFile->readReal(d);
				this->endAngle=d;
				break;
			case 1001:
				inFile->goBack(1);
				this->readXData(inFile);
				break;
			default:
				inFile->readString(value);
				break;
		}
		inFile->readGroupCode(groupCode);
	}
	inFile->goBack(1);
}

//静态方法
DxfEllipse* DxfEllipse::createEllipse(const DxfPoint& cen,const DxfPoint& ept,double r,double sa,double ea)
{
	DxfEllipse* ellipse=new DxfEllipse(cen,ept,r,sa,ea);
	if(ellipse)
		return ellipse;
	else return NULL;
}

/**
 * 当ellipse初始化的时候，默认设置
 * <ol>
 * <li>短轴与长轴之比-- 0.6090456974522166
 * <li>起始角度--0.0
 * <li>终点角度--2*PI
 * </ol>
 */
DxfEllipse* DxfEllipse::createEllipse()
{
	DxfEllipse* ellipse=new DxfEllipse;
	if(ellipse)
		return ellipse;
	else return NULL;
}

//int DxfEllipse::typeId() const 
//{
//	return ELLIPSE; 
//}