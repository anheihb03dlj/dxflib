#include <dxf/entity/DxfCircle.h>

//DxfCircle::DxfCircle(const DxfPoint& pt,double r):center(pt),radius(r)
//{
//}
//
//DxfCircle::DxfCircle(double x,double y,double z,double r):radius(r)
//{
//	this->setCenter(x,y,z);
//}
//
//DxfCircle::~DxfCircle(void)
//{
//}
//
//void DxfCircle::setCenter(const DxfPoint& cen)
//{
//	this->center=cen;
//}
//
//void DxfCircle::setCenter(double x, double y, double z)
//{
//	this->center=DxfPoint(x,y,z);
//}
//
//DxfPoint DxfCircle::getCenter()
//{
//	return this->center;
//}
//
//void DxfCircle::getCenter(double& x,double& y,double& z)
//{
//	x=this->center.x;
//	y=this->center.y;
//	z=this->center.z;
//}
//
//void DxfCircle::setRadius(double r)
//{
//	this->radius=r;
//}
//
//double DxfCircle::getRadius()
//{
//	return this->radius;
//}


void DxfCircle::write(DxfWriter* outFile)
{
	if(isDeleted()) return;                                //如果标记为删除，则不写入到dxf文件中

	DxfEntity::write(outFile);                             //写入circle的颜色、图层、线型等数据

	double x,y,z;
	this->getCenter(x,y,z);                                //获取圆心坐标
	outFile->writeReal(10,x);                              //写入圆心坐标
	outFile->writeReal(20,y);
	outFile->writeReal(30,z);
	outFile->writeReal(40,this->radius);                   //写入圆半径

	this->writeXData(outFile);
}


void DxfCircle::read(DxfReader* inFile)
{
	DxfEntity::read(inFile);                               //读取并设置circle的颜色、图层、线型

	int groupCode=0;
	string value;

	double d=0.0;

	inFile->readGroupCode(groupCode);                      //开始循环读取组码和组值，并设置circle的圆心坐标及半径

	while(groupCode!=0) {
		switch(groupCode) {
			case 10:
				inFile->readReal(d);
				this->center.x=d;
				break;
			case 20:
				inFile->readReal(d);
				this->center.y=d;
				break;
			case 30:
				inFile->readReal(d);
				this->center.z=d;
				break;
			case 40:
				inFile->readReal(d);
				this->radius=d;
				break;
			case 1001:
				inFile->goBack(1);
				this->readXData(inFile);
				break;
			default:
				inFile->readString(value);
				break;
		}
		inFile->readGroupCode(groupCode);
	}
	inFile->goBack(1);                                    //文件读指针回退一行,并定位下一个实体的开始位置
}

//静态函数

DxfCircle* DxfCircle::createCircle(double x,double y,double z,double r)
{
	DxfCircle* circle=new DxfCircle(x,y,z,r);
	if(circle) {
		return circle;
	}
	else {
		return NULL;
	}
}

DxfCircle* DxfCircle::createCircle(const DxfPoint& pt,double r)
{
	DxfCircle* circle=new DxfCircle(pt,r);
	if(circle) {
		return circle;
	}
	else {
		return NULL;
	}
}

//int DxfCircle::typeId() const 
//{ 
//	return CIRCLE; 
//}