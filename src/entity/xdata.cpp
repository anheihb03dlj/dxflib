#include <dxf/entity/xdata.h>
#include <dxf/Base.h>
#include <dxf/section/TablesSection.h>

#include <dxf/DxfReader.h>
#include <dxf/DxfWriter.h>

#include <iostream>
#include <sstream>
#include <cstdio>
#include <algorithm>
using std::stringstream;


//EntityXData::EntityXData()
//{
//
//}

EntityXData::~EntityXData() 
{ 
#ifdef _DEBUG
	std::cout<<"调用EntityXData::~EntityXData()"<<std::endl;
#endif
	map<string,XDataList>::iterator itr;
	for(itr=this->xdatas.begin();itr!=this->xdatas.end();) {
#ifdef _DEBUG
		std::cout<<"删除扩展数据"<<std::endl;
#endif
		itr->second.clear();
		this->xdatas.erase(itr++);
	}
}

void EntityXData::addInt(int value)
{
	string s;
	stringstream ss;
	ss<<value;
	ss>>s;

	xdatas[this->currentAppName].push_back(s);

	ss.clear();
}


void EntityXData::addLong(long value)
{
	string s;
	stringstream ss;
	ss<<value;
	ss>>s;

	xdatas[this->currentAppName].push_back(s);

	ss.clear();
}

void EntityXData::addPoint(double x, double y, double z)
{
	addReal(x);
	addReal(y);
	addReal(z);
}

void EntityXData::addReal(double value)
{
	char str[256];
	sprintf(str, "%.16lf", value);
	string s(str);

	StringExt::replace_all(s,",",".");
	StringExt::trim_right(s,"0");//去掉后面多余的0

	//if(s.length()==0)
	//	s+="0.0";
	if(s.at(s.length()-1)=='.')
		s+="0";

	xdatas[this->currentAppName].push_back(s);

}

void EntityXData::addString(const string &value)
{
	xdatas[this->currentAppName].push_back(value);
}

bool EntityXData::isAppExist(const string& appName)
{
	map<string,XDataList>::iterator itr=this->xdatas.find(appName);
	if(itr==this->xdatas.end())
		return false;
	else
		return true;
}

void EntityXData::registerApp(const string &appName)
{
	this->xdatas.insert(map<string,XDataList>::value_type(appName,list<string>())); 
		
	list<string>::iterator itr=std::find(TablesSection::appNames.begin(),TablesSection::appNames.end(),appName);
	if(itr==TablesSection::appNames.end())
		TablesSection::appNames.push_back(appName);

	this->setCurrentApp(appName);
}

void EntityXData::setCurrentApp(const string &appName)
{
	if(this->isAppExist(appName))
		this->currentAppName=appName;

	else if(this->xdatas.size()==0) {
		std::cerr<<"*没有扩展数据，程序退出......"<<std::endl;
		exit(1);
	}
	else
		this->currentAppName=this->xdatas.begin()->first;
}


void EntityXData::read(DxfReader *inFile)
{
	int groupCode=0;
	string value;
	inFile->readGroupCode(groupCode);

	while(groupCode!=0) {
		if(groupCode==1001) {
			inFile->readString(value);
			if(!this->isAppExist(value))
				this->registerApp(value);

			this->setCurrentApp(value);

			inFile->readGroupCode(groupCode);
			while(groupCode!=1001 && groupCode!=0) {
				inFile->readString(value);
				this->xdatas[this->currentAppName].push_back(value);

				inFile->readGroupCode(groupCode);
			}
			inFile->goBack(1);
		}
		else
			inFile->readString(value);

		inFile->readGroupCode(groupCode);
	}

	inFile->goBack(1);
}


void EntityXData::write(DxfWriter *outFile)
{
	map<string,list<string>>::iterator itr;
	XDataList::iterator list_itr;

	for(itr=this->xdatas.begin();itr!=this->xdatas.end();itr++) {
		outFile->writeString(1001,itr->first);
		for(list_itr=itr->second.begin();list_itr!=itr->second.end();list_itr++)
			outFile->writeString(1000,*list_itr);
	}
}


void EntityXData::addData(const string &name, const string &value)
{
	this->addString(name);
	this->addString(value);
}

void EntityXData::addData(const string &name, int value)
{
	this->addString(name);
	this->addInt(value);
}

void EntityXData::addData(const string &name, double value)
{
	this->addString(name);
	this->addReal(value);
}

void EntityXData::addData(const string &name, double x, double y, double z )
{
	this->addString(name);
	this->addPoint(x,y,z);
}


void EntityXData::getData(const string& name,string& value)
{
	XDataList::iterator itr=std::find(this->xdatas[this->currentAppName].begin(),
									  this->xdatas[this->currentAppName].end(),
									  name);

	if(itr!=this->xdatas[this->currentAppName].end()) {
		itr++;
		if(itr==this->xdatas[this->currentAppName].end()) {
			std::cerr<<"*"<<name<<"字段没有设置值，返回一个空值"<<std::endl;
			value="";
		}
		else 
			value=*itr;
	}
	else {
		std::cerr<<"*"<<name<<"字段不存在，返回一个空值"<<std::endl;
		value="";
	}

}

void EntityXData::getData(const string &name, int &value)
{
	XDataList::iterator itr=std::find(this->xdatas[this->currentAppName].begin(),
									  this->xdatas[this->currentAppName].end(),
									  name);

	if(itr!=this->xdatas[this->currentAppName].end()) {
		itr++;
		if(itr==this->xdatas[this->currentAppName].end()) {
			std::cerr<<"*"<<name<<"字段没有设置值，返回int最大范围值"<<std::endl;
			value=numeric_limits<int>::max();
		}
		else
			value=StringExt::toInt(*itr);
	}
	else {
		std::cerr<<"*"<<name<<"字段不存在，返回int最大范围值"<<std::endl;
		value=numeric_limits<int>::max();
	}
}

void EntityXData::getData(const string &name, double &value)
{
	XDataList::iterator itr=std::find(this->xdatas[this->currentAppName].begin(),
									  this->xdatas[this->currentAppName].end(),
									  name);

	if(itr!=this->xdatas[this->currentAppName].end()) {
		itr++;
		if(itr==this->xdatas[this->currentAppName].end()) {
			std::cerr<<"*"<<name<<"字段没有设置值，返回double最大范围值"<<std::endl;
			value=numeric_limits<double>::max();
		}
		else
			value=StringExt::toReal(*itr);
	}
	else {
		std::cerr<<"*"<<name<<"字段不存在，返回double最大范围值"<<std::endl;
		value=numeric_limits<double>::max();
	}
}

void EntityXData::getData(const string &name, double &x, double &y, double &z)
{
	XDataList::iterator itr=std::find(this->xdatas[this->currentAppName].begin(),
									  this->xdatas[this->currentAppName].end(),
									  name);

	if(itr!=this->xdatas[this->currentAppName].end()) {
		itr++;
		if(itr==this->xdatas[this->currentAppName].end()) {
			std::cerr<<"*"<<name<<"字段没有设置X坐标值，设置X、Y、Z坐标为double最大范围值"<<std::endl;
			x=y=z=numeric_limits<double>::max();
		}
		else {
			x=StringExt::toReal(*itr);

			itr++;
			if(itr==this->xdatas[this->currentAppName].end()) {
				std::cerr<<"*"<<name<<"字段没有设置Y坐标值，设置Y、Z坐标为double最大范围值"<<std::endl;
				y=z=numeric_limits<double>::max();
			}
			else {
				y=StringExt::toReal(*itr);
				
				itr++;
				if(itr==this->xdatas[this->currentAppName].end()) {
					std::cerr<<"*"<<name<<"字段没有设置Z坐标值，设置Z坐标为double最大范围值"<<std::endl;	
					z=numeric_limits<double>::max();
				}
				else 
					z=StringExt::toReal(*itr);
			}
		}
	}
	else {
		std::cerr<<"*"<<name<<"字段不存在，设置X、Y、Z坐标为double最大范围值"<<std::endl;
		x=y=z=numeric_limits<double>::max();
	}
}




//IteratorOfEntityXData::IteratorOfEntityXData(EntityXData* xdata):xdatas(xdata->getXData()) 
//{
//	first(); 
//}
//
//IteratorOfEntityXData::~IteratorOfEntityXData() 
//{ 
//	//this->currentItr=this->xdatas.end(); 
//	std::cout<<"调用~IteratorOfEntityXData()"<<std::endl;
//}
//
//
//void IteratorOfEntityXData::first() 
//{
//	this->currentItr=this->xdatas.begin(); 
//}

void IteratorOfEntityXData::next() 
{
	if(this->currentItr==this->xdatas.end()) {
		this->currentItr=this->xdatas.end();
	}
	else {
		if(currentItr!=this->xdatas.end())	currentItr++; 
	}
}

bool IteratorOfEntityXData::isDone() const 
{
	if(currentItr!=this->xdatas.end())
		return false;
	else 
		return true;
}

//string IteratorOfEntityXData::currentItem() const 
//{
//	return currentItr->first; 
//}

//int IteratorOfEntityXData::getXDataAppCount()
//{
//	return this->xdatas.size(); 
//}



//IteratorOfXDataElement::IteratorOfXDataElement(EntityXData* xdata,const string& appName):xdatas((xdata->getXData())[appName])
//{
//	first(); 
//}
//
//
//IteratorOfXDataElement::~IteratorOfXDataElement()
//{ 
//	//currentItr=xdatas.end();
//	std::cout<<"调用~IteratorOfXDataElement()"<<std::endl;
//}
//
//
//void IteratorOfXDataElement::first() 
//{ 
//	currentItr=xdatas.begin(); 
//}
//
//void IteratorOfXDataElement::next() 
//{
//	if(currentItr!=xdatas.end()) {
//		currentItr++; 
//	}
//}
//
//bool IteratorOfXDataElement::isDone() const 
//{ 
//	if(currentItr!=xdatas.end()) {
//		return false; 
//	}
//	else {
//		return true;
//	}
//}
//
//string IteratorOfXDataElement::currentItem() const 
//{
//	return *currentItr; 
//}
//
XDataIterator* EntityXData::createXDataIterator()
{
	//return IteratorOfEntityXData::createEntityXDataIterator(this);
	return new IteratorOfEntityXData(this);
}

XDataIterator* EntityXData::createXDataItemIterator(const string& appName)
{
	//return IteratorOfXDataElement::createXDataElementIterator(this,appName);
	return new IteratorOfXDataElement(this,appName);
}