#include <dxf/entity/DxfArc.h>

//DxfArc::DxfArc(const DxfPoint& cen,double r,double sa,double ea):center(cen),radius(r),startAngle(sa),endAngle(ea)
//{
//}
//
//DxfArc::~DxfArc(void)
//{
//}
//
//void DxfArc::setCenter(const DxfPoint &pt)
//{
//	this->center=pt;
//}
//
//void DxfArc::setCenter(double x, double y, double z)
//{
//	this->center=DxfPoint(x,y,z);
//}
//
//DxfPoint DxfArc::getCenter()
//{
//	return this->center;
//}
//
//void DxfArc::getCenter(double &x, double &y, double &z)
//{
//	x=this->center.x;
//	y=this->center.y;
//	z=this->center.z;
//}
//
//void DxfArc::setRadius(double r)
//{
//	this->radius=r;
//}
//
//double DxfArc::getRadius()
//{
//	return this->radius;
//}
//
//void DxfArc::setStartAngle(double sa)
//{
//	this->startAngle=sa;
//}
//
//double DxfArc::getStartAngle()
//{
//	return this->startAngle;
//}
//
//void DxfArc::setEndAngle(double ea)
//{
//	this->endAngle=ea;
//}
//
//double DxfArc::getEndAngle()
//{
//	return this->endAngle;
//}
//
//void DxfArc::setAngle(double sa,double ea)
//{
//	this->startAngle=sa;
//	this->endAngle=ea;
//}
//
//void DxfArc::getAngle(double &sa, double &ea)
//{
//	sa=this->startAngle;
//	ea=this->endAngle;
//}

void DxfArc::write(DxfWriter *outFile)
{
	if(isDeleted()) return;                                //实体标记为删除，不写入任何数据

	DxfEntity::write(outFile);                             //写入类型名，图层、颜色、线型等数据

	double x,y,z;
	this->getCenter(x,y,z);                                //获取圆弧圆心坐标
	outFile->writeReal(10,x);                              //写入圆弧圆心坐标
	outFile->writeReal(20,y);
	outFile->writeReal(30,z);
	outFile->writeReal(40,this->radius);                   //写入圆弧半径
	outFile->writeString(100,"AcDbArc");                   //固定用法，如果不写入该项，则dxf文件不能被cad所识别
	outFile->writeReal(50,this->startAngle);               //写入圆弧起始角度
	outFile->writeReal(51,this->endAngle);                 //写入圆弧终点角度

	this->writeXData(outFile);                        //写入扩展数据
}

void DxfArc::read(DxfReader* inFile)
{
	DxfEntity::read(inFile);                               //读取arc的颜色、线型、图层
 
	int groupCode=0;
	string value;

	double d;

	inFile->readGroupCode(groupCode);                      //开始循环读取组码和组值，并设置arc的圆心、半径、起始角度、终点角度

	while(groupCode!=0) {
		switch(groupCode) {
			case 10:
				inFile->readReal(d);
				this->center.x=d;
				break;
			case 20:
				inFile->readReal(d);
				this->center.y=d;
				break;
			case 30:
				inFile->readReal(d);
				this->center.z=d;
				break;
			case 40:
				inFile->readReal(d);
				this->radius=d;
				break;
			case 50:
				inFile->readReal(d);
				this->startAngle=d;
				break;
			case 51:
				inFile->readReal(d);
				this->endAngle=d;
				break;
			case 1001:
				inFile->goBack(1);
				this->readXData(inFile);
				break;
			default:
				inFile->readString(value);
				break;
		}
		inFile->readGroupCode(groupCode);
	}
	inFile->goBack(1);                                      //文件读指针回退一行,定位到下一个实体的开始位置
}


//静态方法

DxfArc* DxfArc::createArc(const DxfPoint& cen,double r,double sa,double ea)
{
	DxfArc* arc=new DxfArc(cen,r,sa,ea);
	if(arc)	return arc;
	else return NULL;
}

DxfArc* DxfArc::createArc(double x,double y,double z,double r,double sa,double ea)
{
	DxfArc* arc=new DxfArc(DxfPoint(x,y,z),r,sa,ea);
	if(arc) return arc;
	else return NULL;
}

//
//int DxfArc::typeId() const 
//{ 
//	return ARC;
//}