#include <dxf/Base.h>
#include <dxf/entity/DxfEntity.h>
#include <dxf/table/DxfLayer.h>
#include <dxf/table/DxfLineType.h>
#include <dxf/section/TablesSection.h>


#include <dxf/entity/DxfLine.h>
#include <dxf/entity/DxfArc.h>
#include <dxf/entity/DxfCircle.h>
#include <dxf/entity/DxfEllipse.h>
#include <dxf/entity/DxfInsert.h>
#include <dxf/entity/DxfText.h>
#include <dxf/entity/DxfMText.h>
#include <dxf/entity/DxfLWPolyLine.h>
#include <dxf/entity/DxfVertex.h>
//可能后续还要添加其他的实体

//#include <iostream>
#include <algorithm>

#include <dxf/entity/xdata.h>//扩展数据管理类

bool DxfEntity::first=true;//仅在第一次执行的时候进行初始化
int DxfEntity::num=0;

EntityTypeName DxfEntity::entityTypeName;


/**
 * <p>
 * 在第一次执行构造函数的时候，初始化entityTypeName的内容，建立实体类型id与实体类型符号及对应的实体arx类名的映射关系<br>
 * 另外，每个实体在生成的时候都做一些相同的初始化工作
 * <ol>
 * 	  <li>图层--当前图层
 * 	  <li>颜色--ByLayer
 * 	  <li>线型--ByLayer
 * 	  <li>实体编号--累加得到该编号，保证唯一性
 * </ol>
 * </p>
 */
DxfEntity::DxfEntity(void)
{

	if(first) {
#ifdef _DEBUG
		std::cout<<(first?"true":"false")<<std::endl;
#endif
		entityTypeName.insert(EntityTypeName::value_type(ARC,make_pair("ARC","AcDbCircle")));
		entityTypeName.insert(EntityTypeName::value_type(LINE,make_pair("LINE","AcDbLine")));
		entityTypeName.insert(EntityTypeName::value_type(CIRCLE,make_pair("CIRCLE","AcDbCircle")));
		entityTypeName.insert(EntityTypeName::value_type(ELLIPSE,make_pair("ELLIPSE","AcDbEllipse")));
		entityTypeName.insert(EntityTypeName::value_type(INSERT,make_pair("INSERT","AcDbBlockReference")));
		entityTypeName.insert(EntityTypeName::value_type(TEXT,make_pair("TEXT","AcDbText")));
		entityTypeName.insert(EntityTypeName::value_type(MTEXT,make_pair("MTEXT","AcDbMText")));
		entityTypeName.insert(EntityTypeName::value_type(LWPOLYLINE,make_pair("LWPOLYLINE","AcDbPolyline")));
		entityTypeName.insert(EntityTypeName::value_type(VERTEX,make_pair("VERTEX","AcDbVertex")));

		//添加更多的实体类型

		first=false;
	}

	this->deletedFlag=false;
	this->layer=DxfLayer::getDefaultLayer()->getLayerName();
	this->color=BYLAYER;
	this->linetype="ByLayer";
	this->entityNum=num;//可以看作是实体的句柄
	num++;

	xdata=NULL;
}


DxfEntity::~DxfEntity(void)
{
#ifdef _DEBUG
	std::cout<<"调用DxfEntity::~DxfEntity(void)"<<std::endl;
#endif
	delete xdata;
	//entityTypeName.clear();
}

string DxfEntity::getLayer()
{
	return this->layer;
}

void DxfEntity::setLayer(const string &s)
{

	if(s.empty()) {                  	                                //当图层名为空的时候，设置实体的图层为当前图层(也即默认图层)
		this->layer=DxfLayer::getDefaultLayer()->getLayerName();
		return;
	}

	map<string,DxfLayer*>::iterator itr=TablesSection::layers.find(s);	//查找图层是否存在
	if(itr!=TablesSection::layers.end())
		this->layer=s;
	else {
		DxfLayer* player=DxfLayer::createLayer(s);                      //如果图层不存在，则新建一个图层，并添加到图层集合中layers
		if(player) {
			TablesSection::layers.insert(map<string,DxfLayer*>::value_type(s,player));
			this->layer=s;
		}
		else
			this->layer=DxfLayer::getDefaultLayer()->getLayerName();    //如果创建图层失败，则设置实体的图层为当前图层
	}
}

string DxfEntity::getLineType()
{
	return this->linetype;                                              //返回线型
}

void DxfEntity::setLineType(const string &s)
{
	if(s.empty()) {                                                     //如果线型名为空，则设置实体的线型为ByLayer(随层)
		this->linetype="ByLayer";
		return ;
	}

	map<string,DxfLineType*>::iterator itr=TablesSection::ltypes.find(s);//查找线型是否存在
	if(itr!=TablesSection::ltypes.end())
		this->linetype=s;
	else
		this->linetype="ByLayer";                                        //不存在则设置实体的线型为ByLayer(随层)
}

int DxfEntity::getColor()
{
	return this->color;                                                  //返回颜色
}

void DxfEntity::setColor(int col)
{
	if(col<0)                                                            //如果颜色值小于0，则设置实体的颜色为ByLayer(256)
		this->color=BYLAYER;
	else 
		this->color=col;
}

bool DxfEntity::isDeleted()
{
	return this->deletedFlag;                                            //返回实体删除标记
}

void DxfEntity::setDeleted(bool onOff)
{
	
	if(onOff)                                                            //如果实体删除标记开，则输出到dxf文件的时候不写入
		this->deletedFlag=true;                                        
	else 
		this->deletedFlag=false;                                         //反之，则写入到dxf文件中（这也是删除实体的一种方法)
}

void DxfEntity::write(DxfWriter* outFile)
{
	outFile->writeString(0,entityTypeName[this->typeId()].first);        //根据实体的类型id返回实体对应的类型名称,例如；0--LINE

	outFile->writeHex(5,outFile->handle());                              //句柄，例如：5--16进制句柄
	outFile->writeString(100,"AcDbEntity");                              //类分割符，例如：100--AcDbEntity

	outFile->writeString(8,this->getLayer());                            //图层，例如：8--图层名

	int col=this->getColor();                                            //如果颜色为ByLayer，则不写入到dxf文件中;例如：62--颜色值
	if(col!=BYLAYER)
		outFile->writeInt(62,col);                                       

	outFile->writeInt(370,-1);                                           //固定用法(软指针句柄)

	string ltype=this->getLineType();//StringExt::to_upper_copy()        //如果线型为ByLayer，则不写入到dxf文件中;例如：6--线型名
	if(ltype!="ByLayer")
		outFile->writeString(6,ltype);



	outFile->writeString(100,entityTypeName[this->typeId()].second);     //实体类分隔符;例如：100--AcDbLine
}

void DxfEntity::read(DxfReader *inFile)
{
	int groupCode=0;
	string value;
	int i=0;

	inFile->readGroupCode(groupCode);                                    //读取组码
	inFile->readString(value);                                           //读取组值


	inFile->readGroupCode(groupCode);                                    // 开始循环读取组码和组值，至关注相关的组码
	while(groupCode!=0) {
		switch(groupCode) {
			case 8:
				inFile->readString(value);                               //图层
				this->setLayer(value);
				break;
			case 6:
				inFile->readString(value);                               //线型
				this->setLineType(value);
				break;
			case 62:
				inFile->readInt(i);                                      //颜色
				this->setColor(i);
				break;
			case 100:
				inFile->readString(value);                               //类分割符
				if(value==entityTypeName[this->typeId()].second)         //etc，如果类分割符==AcDbLine，则强制退出，表示实体的公共部分读取完毕
					return;//强制退出函数
				break;
			default:
				inFile->readString(value);                               //其它组码一律忽略
				break;
		}
		inFile->readGroupCode(groupCode);	
	}
}



//静态方法

DxfEntity* DxfEntity::createEntityByTypeName(const string& entityTypeName)
{
	                                                                     //根据实体类型名创建实体
	DxfEntity* entity=NULL;
	string s=StringExt::to_upper_copy(entityTypeName);                   //将实体类型名转化成大写形式(LINE、CRICLE、ARC等)

	if(s=="LINE") 
		entity=DxfLine::createLine();

	else if(s=="ARC")
		entity=DxfArc::createArc();

	else if(s=="CIRCLE")
		entity=DxfCircle::createCircle();
	
	else if(s=="ELLIPSE") 
		entity=DxfEllipse::createEllipse();

	else if(s=="INSERT")
		entity=DxfInsert::createInsert();

	else if(s=="TEXT") 
		entity=DxfText::createText();
	
	else if(s=="MTEXT")
		entity=DxfMText::createMText();

	else if(s=="LWPOLYLINE")
		entity=DxfLWPolyLine::createLWPolyLine();

	else if(s=="VERTEX") 
		entity=DxfVertex::createVertex();

	if(entity)
		return entity;
	else 
		return	NULL;
	//if--else  结构可能还要继续增长
}



//扩展数据相关函数
IXData* DxfEntity::xData(bool isCreateNewOne)
{
	if(xdata==NULL && isCreateNewOne)
		xdata=new EntityXData();

	return xdata;
}

//void DxfEntity::deleteXData()
//{
//	delete xdata;
//}

void DxfEntity::readXData(DxfReader* inFile)
{
	this->xData();
	this->xdata->read(inFile);
}

void DxfEntity::writeXData(DxfWriter* outFile)
{
	if(this->xdata==NULL) return ;
	
	/*this->xData();*/
	this->xdata->write(outFile);
}

//int DxfEntity::getEntityNum() const
//{
//	return this->entityNum;
//}