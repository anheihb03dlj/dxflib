//#include <dxf/Base.h>
#include <dxf/entity/DxfMText.h>
#include <cmath>

/**
 * 当mtext初始化的时候，默认设置
 * <ol>
 * <li>文本样式--STANDARD
 * <li>对齐方式--左上
 * <li>绘制方向--从左至右
 * <li>旋转角度--0.0
 * <li>高度--50.0
 * </ol>
 */
//DxfMText::DxfMText()
//{
//	this->style="STANDARD";
//	this->attachPoint=TopLeft;
//	this->drawDirection=LeftRight;
//	this->rotationAngle=0.0;
//	this->height=50.0;
//	this->width=0.0;
//}
//
//DxfMText::DxfMText(const DxfPoint& pt,double h,double w,int attachPt,int drawDir,const string& t,const string& s,double angle)
//:insertPoint(pt),height(h),width(w),attachPoint(attachPt),drawDirection(drawDir),text(t),style(s),rotationAngle(angle)
//{
//}
//
//DxfMText::~DxfMText(void)
//{
//}
//
//DxfMText::DxfMText(double ix,double iy,double iz,double h,double w,int attachPt,int drawDir,const string& t,const string& s,double angle)
//:insertPoint(DxfPoint(ix,iy,iz)),height(h),width(w),attachPoint(attachPt),drawDirection(drawDir),text(t),style(s),rotationAngle(angle)
//{
//}
//
//void DxfMText::setInsertPoint(const DxfPoint &pt)
//{
//	this->insertPoint=pt;
//}
//
//void DxfMText::setInsertPoint(double x, double y, double z)
//{
//	this->insertPoint=DxfPoint(x,y,z);
//}
//
//DxfPoint DxfMText::getInsertPoint()
//{
//	return this->insertPoint;
//}
//
//void DxfMText::getInsertPoint(double &x, double &y, double &z)
//{
//	x=this->insertPoint.x;
//	y=this->insertPoint.y;
//	z=this->insertPoint.z;
//}
//
//
//void DxfMText::setTextHeight(double h)
//{
//	this->height=h;
//}
//
//double DxfMText::getTextHeight()
//{
//	return this->height;
//}
//
//void DxfMText::setTextWidth(double w)
//{
//	this->width=w;
//}
//
//double DxfMText::getTextWidth()
//{
//	return this->width;
//}
//
//void DxfMText::setAttachPoint(int attach)
//{
//	this->attachPoint=attach;
//}
//
//void DxfMText::setDrawDirection(int dir)
//{
//	this->drawDirection=dir;
//}
//
//void DxfMText::setText(const string &s)
//{
//	this->text=s;
//}
//
//string DxfMText::getText()
//{
//	return this->text;
//}
//
//void DxfMText::setTextStyle(const string &s)
//{
//	this->style=s;
//}
//
//void DxfMText::setRotationAngle(double angle)
//{
//	this->rotationAngle=angle;
//}
//
//double DxfMText::getRotationAngle()
//{
//	return this->rotationAngle;
//}


void DxfMText::write(DxfWriter *outFile)
{
	if(isDeleted()) return ;                                    //如果标记为删除，则不写入到dxf文件中

	DxfEntity::write(outFile);                                  //写入mtext的颜色、图层、线型等数据

	double x,y,z;
	this->getInsertPoint(x,y,z);
	outFile->writeReal(10,x);                                   //写入文本插入点坐标
	outFile->writeReal(20,y);
	outFile->writeReal(30,z);


	outFile->writeReal(40,this->height);                       //写入文本高度

	outFile->writeReal(41,this->width);                        //写入文本框宽度(这个组码我试验后，没什么大的作用，不论是否存在，还是设置为0，或是其他的值，都没有什么影响，所以一般可以不用考虑该项组码，给定默认值即可

	outFile->writeInt(71,this->attachPoint);                  //写入对齐方式
 
	outFile->writeInt(72,this->drawDirection);                //写入绘制方向

	string s=this->text;
	string stemp;
	int length=s.length();
	int temp=0;
	if(length<=250) {                                        //写入文本内容,如果字符串长度小于 250 个字符，所有字符均出现在组 1 中。如果字符串长度大于 250 个字符，该字符串将分成长度为 250 个字符的数据块，并显示在一个或多个组 3 代码中。如果使用组 3 代码，最后一个组将是组 1 并且字符数少于 250 个 
		StringExt::replace_all(s,"\n","\\P");
		outFile->writeString(1,s);
	}
	else {
		while(length>250) {
			temp=length;
			stemp=s.substr(0,250);
			StringExt::replace_all(stemp,"\n","\\P");
			outFile->writeString(3,stemp);
			s=s.substr(250);
			length=s.length();
		}
		StringExt::replace_all(s,"\n","\\P");
		outFile->writeString(1,s);
	}
	
	if(this->style!="STANDARD")                              //如果文本样式==STANDARD,则不写入到dxf文件中
		outFile->writeString(7,this->style);

	if(!(abs(this->rotationAngle-0.0)<1e-3))                //如果旋转角度==0.0，则不写入到dxf文件中
		outFile->writeReal(50,this->rotationAngle);         // dxf的参考文档有错误，这里的单位应该是度(°)，而不是弧度（rad)

	this->writeXData(outFile);                              //写入扩展数据

}

void DxfMText::read(DxfReader *inFile)
{
	DxfEntity::read(inFile);                                //读取并设置mtext的颜色、图层、线型

	int groupCode=0;
	string value;
	
	int i=0;
	double d=0.0;

	inFile->readGroupCode(groupCode);                      //开始循环读取并设置mtext的各项数据

	while(groupCode!=0) {
		switch(groupCode) {
			case 10:
				inFile->readReal(d);
				this->insertPoint.x=d;
				break;
			case 20:
				inFile->readReal(d);
				this->insertPoint.y=d;
				break;
			case 30:
				inFile->readReal(d);
				this->insertPoint.z=d;
				break;
			case 40:
				inFile->readReal(d);
				this->height=d;
				break;
			case 41:
				inFile->readReal(d);
				this->width=d;
				break;
			case 71:
				inFile->readInt(i);
				this->attachPoint=i;
				break;
			case 72:
				inFile->readInt(i);
				this->drawDirection=i;
				break;
			case 1:
				inFile->readString(value);
				this->text=value;
				break;
			case 7:
				inFile->readString(value);
				this->style=value;
				break;
			case 50:
				inFile->readReal(d);
				this->rotationAngle=d;
				break;
			case 1001:
				inFile->goBack(1);
				this->readXData(inFile);
				break;
			default:
				inFile->readString(value);
				break;
		}
		inFile->readGroupCode(groupCode);
	}
	inFile->goBack(1);                                     //文件读指针回滚到上一行
}

//静态方法
/**
 * 当mtext初始化的时候，默认设置
 * <ol>
 * <li>文本样式--STANDARD
 * <li>对齐方式--左上
 * <li>绘制方向--从左至右
 * <li>旋转角度--0.0
 * <li>高度--50.0
 * </ol>
 */
DxfMText* DxfMText::createMText()
{
	DxfMText* mtext=new DxfMText;
	if(mtext) {
		return mtext;
	}
	else {
		return NULL;
	}
}

DxfMText* DxfMText::createMText(const DxfPoint &insertPoint, double height, double width, int attachPoint, int drawDirection, const string &text, const string &styleName, double rotationAngle)
{
	DxfMText* mtext=new DxfMText(insertPoint,height,width,attachPoint,drawDirection,text,styleName,rotationAngle);
	if(mtext) {
		return mtext;
	}
	else {
		return NULL;
	}
}

DxfMText* DxfMText::createMText(double ix, double iy, double iz, double height, double width, int attachPoint, int drawDirection, const string &text, const string &styleName, double rotationAngle)
{
	DxfMText* mtext=new DxfMText(ix,iy,iz,height,width,attachPoint,drawDirection,text,styleName,rotationAngle);
	if(mtext) {
		return mtext;
	}
	else {
		return NULL;
	}
}

//int DxfMText::typeId() const 
//{
//	return MTEXT; 
//}