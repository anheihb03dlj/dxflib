#include <dxf/entity/DxfLine.h>

//DxfLine::DxfLine(const DxfPoint& startPt,const DxfPoint& endPt):startPoint(startPt),endPoint(endPt)
//{
//
//}
//
//DxfLine::DxfLine(double p1x,double p1y,double p1z,double p2x,double p2y,double p2z):startPoint(p1x,p1y,p1z),endPoint(p2x,p2y,p2z)
//{
//
//}
//
//DxfLine::~DxfLine(void)
//{
//
//}
//
//
//void DxfLine::setStartPoint(const DxfPoint& pt)
//{
//	this->startPoint=pt;
//}
//
//void DxfLine::setStartPoint(const double x,const double y,const double z)
//{
//	this->startPoint=DxfPoint(x,y,z);
//}
//
//void DxfLine::setEndPoint(const DxfPoint& pt)
//{
//	this->startPoint=pt;
//}
//
//void DxfLine::setEndPoint(const double x,const double y,const double z)
//{
//	this->endPoint=DxfPoint(x,y,z);
//}
//
//DxfPoint DxfLine::getStartPoint()
//{
//	return startPoint;
//}
//
//DxfPoint DxfLine::getEndPoint()
//{
//	return endPoint;
//}
//
//void DxfLine::getStartPoint(double& x,double& y,double& z)
//{
//	x=this->startPoint.x;
//	y=this->startPoint.y;
//	z=this->startPoint.z;
//}
//
//void DxfLine::getEndPoint(double &x, double &y, double &z)
//{
//	x=this->endPoint.x;
//	y=this->endPoint.y;
//	z=this->endPoint.z;
//}

void DxfLine::write(DxfWriter *outFile)
{
	if(isDeleted()) return ;                             //如果标记为删除，则不写入到dxf文件中

	DxfEntity::write(outFile);                           //写开始部分(颜色、图层、线型等)

	double x1,y1,z1;
	double x2,y2,z2;
	getStartPoint(x1,y1,z1);                             //获取起点和终点的坐标
	getEndPoint(x2,y2,z2);

	outFile->writeReal(10,x1);                           //写入起点坐标
	outFile->writeReal(20,y1);
	outFile->writeReal(30,z1);

	outFile->writeReal(11,x2);                           //写入终点坐标
	outFile->writeReal(21,y2);
	outFile->writeReal(31,z2);

	this->writeXData(outFile);                      //写入扩展数据
}

void DxfLine::read(DxfReader* inFile)
{
	DxfEntity::read(inFile);                            //读入开始部分,并设置line的颜色、图层、线型

	int groupCode=0;
	string value;

	double d=0.0;
	
	inFile->readGroupCode(groupCode);

	while(groupCode!=0) {                               //循环读取组码和组值，设置line的起点和终点坐标
		switch(groupCode) {
			case 10:
				inFile->readReal(d);
				this->startPoint.x=d;
				break;
			case 20:
				inFile->readReal(d);
				this->startPoint.y=d;
				break;
			case 30:
				inFile->readReal(d);
				this->startPoint.z=d;
				break;
			case 11:
				inFile->readReal(d);
				this->endPoint.x=d;
				break;
			case 21:
				inFile->readReal(d);
				this->endPoint.y=d;
				break;
			case 31:
				inFile->readReal(d);
				this->endPoint.z=d;
				break;
			case 1001:
				inFile->goBack(1);
				this->readXData(inFile);
				break;
			default:
				inFile->readString(value);
				break;
		}
		inFile->readGroupCode(groupCode);
	}

	inFile->goBack(1);                                    //文件读指针退后一行,定位到下一个实体开始位置
}


//静态方法
DxfLine* DxfLine::createLine(const DxfPoint& startPt,const DxfPoint& endPt)
{
	DxfLine* line=new DxfLine(startPt,endPt);
	if(line) {
		return line;
	}
	else {
		return NULL;
	}
}

DxfLine* DxfLine::createLine(double p1x,double p1y,double p1z, double p2x,double p2y,double p2z)
{
	DxfLine* line=new DxfLine(p1x,p1y,p1z,p2x,p2y,p2z);
	if(line) {
		return line;
	}
	else {
		return NULL;
	}
}

//int DxfLine::typeId() const 
//{ 
//	return LINE;
//}