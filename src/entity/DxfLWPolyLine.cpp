#include <dxf/entity/DxfLWPolyLine.h>
#include <dxf/entity/DxfVertex.h>

#include <iostream>
#include <cmath>

///** 默认设置多段线是不闭合的. */
//DxfLWPolyLine::DxfLWPolyLine()
//{
//	this->flag=0;
//}

DxfLWPolyLine::~DxfLWPolyLine()
{
#ifdef _DEBUG
	std::cout<<"调用~DxfLWPolyLine()"<<std::endl;
#endif
	vector<IVertex*>::iterator itr;
	for(itr=this->vertices.begin();itr!=this->vertices.end(); itr++) {
#ifdef _DEBUG
		std::cout<<"删除节点"<<std::endl;
#endif
		delete *itr;
	}
}

void DxfLWPolyLine::read(DxfReader* inFile)
{
	DxfEntity::read(inFile);                               //读取并设置多段线的颜色、图层、线型  

	DxfVertex* vertex=NULL;
	bool isWidthEqual=false;
	double widthEqual=0.0;
	DxfPoint pt;

	int groupCode=0;
	string value;

	int i=0;
	double d=0.0;

	inFile->readGroupCode(groupCode);                     //开始循环读取组码和组值，并设置多段线的各项数据

	while(groupCode!=0) {
		switch(groupCode) {
			case 70:
				inFile->readInt(i);
				this->flag=i;
				break;
			case 43:                                     //如果出现组码43，则标识多段线是等宽的
				isWidthEqual=true;
				inFile->readReal(d);
				widthEqual=d;                            //临时保存等宽值
				break;
			case 10:
				inFile->readReal(d);
				vertex=DxfVertex::createVertex();
				
				if(vertex==NULL)  {
					std::cerr<<"内存分配错误"<<std::endl;
					std::cerr<<"程序退出"<<std::endl;
					exit(1);
				}

				this->addVertex(vertex);
				if(isWidthEqual)                         //如果多段线是等宽的，设置节点的起始宽度一样
					vertex->setSEWidth(widthEqual,widthEqual);

				pt.x=d;                                  //保存节点的坐标-X，只有20组码存在时，才能保证完整的坐标信息，从而设置节点的坐标
				break;
			case 20:
				inFile->readReal(d);
				pt.y=d;
				vertex->setPoint(pt);
				break;
			case 40:
				if(isWidthEqual) {                       //如果已经确定多段线是等宽的，则忽略40、41组
					inFile->readReal(d);
					break;
				}

				inFile->readReal(d);
				vertex->setStartWidth(d);
				break;
			case 41:
				if(isWidthEqual) {
					inFile->readReal(d);
					break;
				}

				inFile->readReal(d);
				vertex->setEndWidth(d);
				break;
			case 1001:
				inFile->goBack(1);
				this->readXData(inFile);
				break;
			default:
				inFile->readString(value);
				break;
		}
		inFile->readGroupCode(groupCode);
	}
	inFile->goBack(1);                                //文件读指针回滚到上一行
}

void DxfLWPolyLine::write(DxfWriter* outFile)
{
	if(this->vertexCount()==0) {
		std::cerr<<"定义了一条没有任何节点的polyline是没有意义的"<<std::endl;
		return ;                                     //没有任何节点的polyline没有必要写入到文件中
	}

	if(isDeleted()) return;                          //实体标记为删除，不写入任何数据

	DxfEntity::write(outFile);                       //写入图层、颜色、线型等数据

	outFile->writeInt(90,this->vertexCount());       //写入节点个数

	outFile->writeInt(70,this->flag);                //写入节点标志(是否封闭)
	
	double x,y,z;
	vector<IVertex*>::iterator itr;

	                                                 //下面的这一部分判断多段线是否等宽(宽度由vertex的端点和终点宽度决定，不作为polyline的属性)
	double sw,ew;
	double lastWidth=(*vertices.begin())->getEndWidth();
	bool isWidthEqual=true;

	for(itr=vertices.begin();itr!=vertices.end();itr++) {
		(*itr)->getSEWidth(sw,ew);
		
		if(!(abs(sw-ew)<1e-3)) { isWidthEqual=false; break; }
		if(!(abs(ew-lastWidth)<1e-3)) { isWidthEqual=false; break; }
		lastWidth=ew;
	}

                                                     //如果多段线等宽，则不写入40、41组码
	if(isWidthEqual) {                               

		outFile->writeReal(43,lastWidth);            //写入多段线等宽值

		for(itr=vertices.begin();itr!=vertices.end();itr++) {
			(*itr)->getPoint(x,y,z);
			outFile->writeReal(10,x);                //写入节点坐标
			outFile->writeReal(20,y);
		}
	}
	else {
		for(itr=vertices.begin();itr!=vertices.end();itr++) {
			(*itr)->getPoint(x,y,z);
			(*itr)->getSEWidth(sw,ew);
			outFile->writeReal(10,x);                //写入节点坐标
			outFile->writeReal(20,y);
			outFile->writeReal(40,sw);               //写入节点起始宽度
			outFile->writeReal(41,ew);               //写入节点终点宽度
		}
	}

	this->writeXData(outFile);                  //写入扩展数据
}

//void DxfLWPolyLine::addVertex(DxfVertex* vertex)
//{
//	if(vertex) 
//		vertices.push_back(vertex);
//}



//静态方法
/** 默认多段线是不闭合的. */
DxfLWPolyLine* DxfLWPolyLine::createLWPolyLine()
{
	DxfLWPolyLine* lwpline=new DxfLWPolyLine;
	if(lwpline) 
		return lwpline;
	else
		return NULL;
}


//int DxfLWPolyLine::typeId() const
//{
//	return LWPOLYLINE; 
//}
//
//int DxfLWPolyLine::vertexCount() const
//{ 
//	return vertices.size();
//}
//
//void DxfLWPolyLine::setFlag(bool isClose)
//{
//	if(isClose) {
//		flag=1; 
//	}
//	else {
//		flag=0;	
//	}
//}