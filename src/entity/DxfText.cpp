#include <dxf/entity/DxfText.h>
#include <cmath>

/**
 * 当text初始化的时候，默认设置
 * <ol>
 * <li>旋转角度--0.0
 * <li>x比例因子--1.0
 * <li>文本样式--STANDARD
 * <li>水平对齐方式--左对齐
 * <li>垂直对齐方式--基线对齐
 * </ol>
 */
//DxfText::DxfText()
//{
//	this->rotationAngle=0.0;
//	this->xScaleFactor=1.0;
//	this->style="STANDARD";
//	this->hJustification=Left;
//	this->vJustification=BaseLine;
//	this->height=50.0;
//	this->alignPoint=DxfPoint(0,0,0);
//}
//
//
//DxfText::DxfText(const DxfPoint& insertPt,const DxfPoint& alignPt,
//		double h,double xsFactor,int hj,int vj,
//		const string& styleName,const string& t,double angle)
//		:insertPoint(insertPt),alignPoint(alignPt),height(h),xScaleFactor(xsFactor),hJustification(hj),vJustification(vj),rotationAngle(angle),style(styleName),text(t)
//{
//
//}
//
//DxfText::DxfText(double ix,double iy,double iz,
//		double ax,double ay,double az,
//		double h,double xsFactor,
//		int hj,int vj,
//		const string& styleName,const string& t,double angle)
//		:insertPoint(DxfPoint(ix,iy,iz)),alignPoint(DxfPoint(ax,ay,az)),height(h),xScaleFactor(xsFactor),hJustification(hj),vJustification(vj),rotationAngle(angle),style(styleName),text(t)
//{
//
//}
//
//DxfText::~DxfText(void)
//{
//}
//
//
//void DxfText::setInsertPoint(const DxfPoint &pt)
//{
//	this->insertPoint=pt;
//}
//
//void DxfText::setInsertPoint(double x, double y, double z)
//{
//	this->insertPoint=DxfPoint(x,y,z);
//}
//
//DxfPoint DxfText::getInsertPoint()
//{
//	return this->insertPoint;
//}
//
//void DxfText::getInsertPoint(double &x, double &y, double &z)
//{
//	x=this->insertPoint.x;
//	y=this->insertPoint.y;
//	z=this->insertPoint.z;
//}
//
//void DxfText::setAlignPoint(const DxfPoint &pt)
//{
//	this->alignPoint=pt;
//}
//
//void DxfText::setAlignPoint(double x, double y, double z)
//{
//	this->alignPoint=DxfPoint(x,y,z);
//}
//
//DxfPoint DxfText::getAlignPoint()
//{
//	return this->alignPoint;
//}
//
//void DxfText::getAlignPoint(double &x, double &y, double &z)
//{
//	x=this->alignPoint.x;
//	y=this->alignPoint.y;
//	z=this->alignPoint.z;
//}
//
//void DxfText::setTextHeight(double h)
//{
//	this->height=h;
//}
//
//double DxfText::getTextHeight()
//{
//	return this->height;
//}
//
//void DxfText::setXScaleFactor(double xsfactor)
//{
//	this->xScaleFactor=xsfactor;
//}
//
//double DxfText::getXScaleFactor()
//{
//	return this->xScaleFactor;
//}
//
//void DxfText::setTextJustification(int hj,int vj)
//{
//	this->hJustification=hj;
//	this->vJustification=vj;
//}
//
//void DxfText::setStyleName(const string& styleName)
//{
//	this->style=styleName;
//}
//
//string DxfText::getStyleName()
//{
//	return this->style;
//}
//
//void DxfText::setText(const string &text)
//{
//	this->text=text;
//}
//
//string DxfText::getText()
//{
//	return this->text;
//}
//
//void DxfText::setRotationAngle(double angle)
//{
//	this->rotationAngle=angle;
//}
//
//double DxfText::getRotationAngle()
//{
//	return this->rotationAngle;
//}


void DxfText::write(DxfWriter *outFile)
{
	if(isDeleted()) return ;                                      //如果标记为删除，则不写入到dxf文件中
	
	DxfEntity::write(outFile);                                    //写text的颜色、图层、线型等数据

	double x,y,z;
	this->getInsertPoint(x,y,z);
	outFile->writeReal(10,x);                                     //写入text的插入点坐标
	outFile->writeReal(20,y);
	outFile->writeReal(30,z);

	outFile->writeReal(40,this->height);                          //写入text的高度

	outFile->writeString(1,this->text);                           //写入text文本内容

	if(!(abs(this->rotationAngle-0.0)<1e-3))                      //如果旋转角度==0.0，则不写入到dxf文件中
		outFile->writeReal(50,this->rotationAngle);

	if(!(abs(this->xScaleFactor-1.0)<1e-3))                       //如果x比例因子==1.0，则不写入到dxf文件中
		outFile->writeReal(41,this->xScaleFactor);

	if(this->style!="STANDARD")                                   //如果文本样式==STANDARD，则不写入到dxf文件中
		outFile->writeString(7,this->style);

	if(!(this->hJustification == TextType::Left && this->vJustification == TextType::BaseLine)) {
		if(this->hJustification!=TextType::Left)                            
			outFile->writeInt(72,this->hJustification); 

		this->getAlignPoint(x,y,z);
		outFile->writeReal(11,x);                                //如果水平对齐方式!=左对齐或者垂直对齐方式!=基线对齐，则写入对齐点坐标
		outFile->writeReal(21,y);
		outFile->writeReal(31,z);
	}

	outFile->writeString(100,"AcDbText");

	if(this->vJustification!=TextType::BaseLine)
		if(this->vJustification!=TextType::BaseLine)
			outFile->writeInt(73,this->vJustification);

	this->writeXData(outFile);                                  //写入扩展数据

}

void DxfText::read(DxfReader* inFile)
{
	DxfEntity::read(inFile);                                     //读取text的颜色、图层、线型等数据

	int groupCode=0;
	string value;

	int i=0;
	double d=0.0;

	inFile->readGroupCode(groupCode);                           //开始循环读取组码和组值，并设置text的各项数据

	while(groupCode!=0) {
		switch(groupCode) {
			case 10:
				inFile->readReal(d);
				this->insertPoint.x=d;
				break;
			case 20:
				inFile->readReal(d);
				this->insertPoint.y=d;
				break;
			case 30:
				inFile->readReal(d);
				this->insertPoint.z=d;
				break;
			case 11:
				inFile->readReal(d);
				this->alignPoint.x=d;
				break;
			case 21:
				inFile->readReal(d);
				this->alignPoint.y=d;
				break;
			case 31:
				inFile->readReal(d);
				this->alignPoint.z=d;
				break;
			case 40:
				inFile->readReal(d);
				this->height=d;
				break;
			case 50:
				inFile->readReal(d);
				this->rotationAngle=d;
				break;
			case 41:
				inFile->readReal(d);
				this->xScaleFactor=d;
				break;
			case 72:
				inFile->readInt(i);
				this->hJustification=i;
				break;
			case 73:
				inFile->readInt(i);
				this->vJustification=i;
				break;
			case 7:
				inFile->readString(value);
				this->style=value;
				break;
			case 1:
				inFile->readString(value);
				this->text=value;
				break;
			case 1001:
				inFile->goBack(1);
				this->readXData(inFile);
				break;
			default:
				inFile->readString(value);
				break;
		}
		inFile->readGroupCode(groupCode);
	}
	inFile->goBack(1);
}

//静态方法
/**
 * 当text初始化的时候，默认设置
 * <ol>
 * <li>旋转角度--0.0
 * <li>x比例因子--1.0
 * <li>文本样式--STANDARD
 * <li>水平对齐方式--左对齐
 * <li>垂直对齐方式--基线对齐
 * </ol>
 */
DxfText* DxfText::createText()
{
	DxfText* txt=new DxfText;
	if(txt) {
		return txt;
	}
	else {
		return NULL;
	}
}

DxfText* DxfText::createText(const DxfPoint &insertPoint, const DxfPoint &alignPoint, 
					   double height, double xScaleFactor, 
					   int hJustification, int vJustification, 
					   const string &styleName, const string &text, double rotationAngle)
{
	DxfText* txt=new DxfText(insertPoint,alignPoint,height,xScaleFactor,hJustification,vJustification,styleName,text,rotationAngle);
	if(txt) {
		return txt;
	}
	else {
		return NULL;
	}
}

DxfText* DxfText::createText(double ix, double iy, double iz, double ax, double ay, double az, 
					   double height, double xScaleFactor, int hJustification, int vJustification, 
					   const string &styleName, const string &text, double rotationAngle)
{
	DxfText* txt=new DxfText(ix,iy,iz,ax,ay,az,height,xScaleFactor,hJustification,vJustification,styleName,text,rotationAngle);
	if(txt) {
		return txt;
	}
	else {
		return NULL;
	}
}
//
//int DxfText::typeId() const 
//{
//	return TEXT; 
//}