#include <dxf/entity/DxfInsert.h>
#include <cmath>

//DxfInsert::DxfInsert() 
//{
//	this->rotationAngle=0.0;
//	this->xscale=1.0;
//	this->yscale=1.0;
//	this->zscale=1.0;
//}
//
//DxfInsert::DxfInsert(const string& name,const DxfPoint& pt,double xs,double ys,double zs,double ra)
//:blockName(name),insertPoint(pt),xscale(xs),yscale(ys),zscale(zs),rotationAngle(ra)
//{
//}
//
//DxfInsert::~DxfInsert(void)
//{
//}
//
//
//void DxfInsert::setBlockName(const string& name)
//{
//	this->blockName=name;
//}
//
//string DxfInsert::getBlockName()
//{
//	return this->blockName;
//}
//
//void DxfInsert::setInsertPoint(const DxfPoint& pt)
//{
//	this->insertPoint=pt;
//}
//
//void DxfInsert::setInsertPoint(double x,double y,double z)
//{
//	this->insertPoint=DxfPoint(x,y,z);
//}
//
//DxfPoint DxfInsert::getInsertPoint()
//{
//	return this->insertPoint;
//}
//
//void DxfInsert::getInsertPoint(double& x,double& y,double& z)
//{
//	x=this->insertPoint.x;
//	y=this->insertPoint.y;
//	z=this->insertPoint.z;
//}
//
//void DxfInsert::setScale(double xs,double ys,double zs)
//{
//	this->xscale=xs;
//	this->yscale=ys;
//	this->zscale=zs;
//}
//
//void DxfInsert::setRotationAngle(double ra)
//{
//	this->rotationAngle=ra;
//}
//
//double DxfInsert::getRotationAngle()
//{
//	return this->rotationAngle;
//}

void DxfInsert::write(DxfWriter *outFile)
{
	if(isDeleted()) return ;                                                             //如果标记为删除，则不写入到dxf文件

	DxfEntity::write(outFile);                                                           //写入insert的颜色、图层、线型等数据
	
	outFile->writeString(2,this->blockName);                                             //写入块名
	double x,y,z;
	this->getInsertPoint(x,y,z);
	outFile->writeReal(10,x);                                                            //写入插入点坐标
	outFile->writeReal(20,y);
	outFile->writeReal(30,z);
                                                                                         //如果x、y轴比例==1.0，则不写入dxf文件
	if(!((abs(this->xscale-1.0)<1e-3) && (abs(this->yscale-1.0)<1e-3))) {//判断浮点数相等
		outFile->writeReal(41,this->xscale);
		outFile->writeReal(42,this->yscale);
		outFile->writeReal(43,this->zscale);
	}

	if(!(abs(this->rotationAngle-0.0)<1e-3))                                             //写入旋转角度
		outFile->writeReal(50,this->rotationAngle);

	this->writeXData(outFile);                                                      //写入扩展数据
}

void DxfInsert::read(DxfReader* inFile)
{
	DxfEntity::read(inFile);

	int groupCode=0;
	string value;
	
	double d=0.0;

	inFile->readGroupCode(groupCode);

	while(groupCode!=0) {
		switch(groupCode) {
			case 2:
				inFile->readString(value);
				this->blockName=value;
				break;
			case 10:
				inFile->readReal(d);
				this->insertPoint.x=d;
				break;
			case 20:
				inFile->readReal(d);
				this->insertPoint.y=d;
				break;
			case 30:
				inFile->readReal(d);
				this->insertPoint.z=d;
				break;
			case 41:
				inFile->readReal(d);
				this->xscale=d;
				break;
			case 42:
				inFile->readReal(d);
				this->yscale=d;
				break;
			case 43:
				inFile->readReal(d);
				this->zscale=d;
				break;
			case 50:
				inFile->readReal(d);
				this->rotationAngle=d;
				break;
			case 1001:
				inFile->goBack(1);
				this->readXData(inFile);
				break;
			default:
				inFile->readString(value);
				break;
		}
		inFile->readGroupCode(groupCode);
	}
	inFile->goBack(1);
}


//静态方法
DxfInsert* DxfInsert::createInsert(const string& name,const DxfPoint& pt,double xs,double ys,double zs,double ra)
{
	DxfInsert* insert=new DxfInsert(name,pt,xs,ys,zs,ra);
	if(insert) {
		return insert;
	}
	else {
		return NULL;
	}
}

DxfInsert* DxfInsert::createInsert()
{
	DxfInsert* insert=new DxfInsert;
	if(insert) {
		return insert;
	}
	else {
		return NULL;
	}
}


//int DxfInsert::typeId() const 
//{ 
//	return INSERT;
//}