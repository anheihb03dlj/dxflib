#include <dxf/entity/DxfVertex.h>
#include <cmath>

/**
 * 当vertex初始化的时候，默认设置
 * <ol>
 * <li>起点宽度--0.0
 * <li>终点宽度--0.0
 * </ol>
 */
//DxfVertex::DxfVertex(void)
//{
//	this->startWidth=0.0;
//	this->endWidth=0.0;
//}
//
//DxfVertex::DxfVertex(const DxfPoint& pt,double sw,double ew)
//:point(pt),startWidth(sw),endWidth(ew)
//{
//
//}
//
//DxfVertex::~DxfVertex(void)
//{
//}
//
//void DxfVertex::setPoint(const DxfPoint& pt)
//{
//	this->point=pt;
//}
//
//void DxfVertex::setPoint(double x,double y,double z)
//{
//	this->point=DxfPoint(x,y,z);
//}
//
//DxfPoint DxfVertex::getPoint()
//{
//	return this->point;
//}
//
//void DxfVertex::getPoint(double& x,double& y,double& z)
//{
//	x=this->point.x;
//	y=this->point.y;
//	z=this->point.z;
//}
//
//void DxfVertex::setStartWidth(double sw)
//{
//	this->startWidth=sw;
//}
//
//void DxfVertex::setEndWidth(double ew)
//{
//	this->endWidth=ew;
//}
//
//double DxfVertex::getStartWidth()
//{
//	return this->startWidth;
//}
//
//double DxfVertex::getEndWidth()
//{
//	return this->endWidth;
//}
//
//void DxfVertex::setSEWidth(double sw,double ew)
//{
//	this->startWidth=sw;
//	this->endWidth=ew;
//}
//
//void DxfVertex::getSEWidth(double& sw,double& ew)
//{
//	sw=this->startWidth;
//	ew=this->endWidth;
//}


void DxfVertex::read(DxfReader* inFile)                                                   //vertex暂时不考虑读写的情况，它的主要作用是用来生成多段线
{

}

void DxfVertex::write(DxfWriter* outFile)                                                //vertex暂时不考虑读写的情况，它的主要作用是用来生成多段线
{

}


//静态方法
/**
 * 当vertex初始化的时候，默认设置
 * <ol>
 * <li>起点宽度--0.0
 * <li>终点宽度--0.0
 * </ol>
 */
DxfVertex* DxfVertex::createVertex()
{
	DxfVertex* vertex=new DxfVertex;
	if(vertex) 
		return vertex;
	else
		return NULL;
}

DxfVertex* DxfVertex::createVertex(const DxfPoint& pt,double sw,double ew)
{
	DxfVertex* vertex=new DxfVertex(pt,sw,ew);
	if(vertex) 
		return vertex;
	else
		return NULL;
}

DxfVertex* DxfVertex::createVertex(double x,double y,double z,double sw,double ew)
{
	DxfVertex* vertex=new DxfVertex(DxfPoint(x,y,z),sw,ew);
	if(vertex) 
		return vertex;
	else
		return NULL;
}


//int DxfVertex::typeId() const 
//{ 
//	return VERTEX; 
//}