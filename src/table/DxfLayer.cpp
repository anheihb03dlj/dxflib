#include <dxf/table/DxfLayer.h>
#include <dxf/table/DxfLineType.h>
#include <dxf/section/TablesSection.h>
#include <map>
using std::map;

//静态成员初始化
DxfLayer* DxfLayer::defaultLayer=NULL;

DxfLayer::DxfLayer(const string& name,int col,const string& ltype,int f)
					:layerName(name),color(col),linetype(ltype),flags(f)
{
	//initial();
}

//DxfLayer::~DxfLayer(void)
//{
//	delete defaultLayer;
//}


//void DxfLayer::initial()
//{
//	getDefaultLayer();                                                 //强制创建一个0层（最开始的默认图层）
//}

//void DxfLayer::setLayerName(const string &name)
//{
//	this->layerName=name;
//}

//string DxfLayer::getLayerName()
//{
//	return this->layerName;
//}

//void DxfLayer::setColor(int color)
//{
//	this->color=color;
//}
//
//int DxfLayer::getColor()
//{
//	return this->color;
//}

/**
 * 如果线型名为空，设置线型为Continuous，
 * 如果没有查找到线型，设置线型为Continuous.
 */
void DxfLayer::setLineType(const string &linetype)
{
	if(linetype.empty()) {
		this->linetype="Continuous";
		return;
	}

	map<string,DxfLineType*>::iterator itr=TablesSection::ltypes.find(linetype);
	if(itr!=TablesSection::ltypes.end())
		this->linetype=linetype;
	else
		this->linetype="Continuous";
}

//string DxfLayer::getLineType()
//{
//	return this->linetype;
//}
//
//void DxfLayer::setFlags(int flags)
//{
//	this->flags=flags;
//}
//
//int DxfLayer::getFlags()
//{
//	return this->flags;
//}


/**
 * 默认图层(也就是当前图层)采用了单件模式，保证只有一个对象实例在运行.
 */
DxfLayer* DxfLayer::getDefaultLayer()
{
	if (defaultLayer == NULL) {
		defaultLayer = new DxfLayer;                                   //最初以0层作为默认层(当前层)
		map<string,DxfLayer*>::iterator itr=TablesSection::layers.find(defaultLayer->layerName);
		if(itr==TablesSection::layers.end())
			TablesSection::layers.insert(map<string,DxfLayer*>::value_type(defaultLayer->layerName,defaultLayer));
		else
			defaultLayer=itr->second;

		return defaultLayer;
	}
	return defaultLayer;
}

void DxfLayer::setDefaultLayer(const string& layerName)
{
	map<string,DxfLayer*>::iterator itr=TablesSection::layers.find(layerName);
	if(itr!=TablesSection::layers.end())
		defaultLayer=itr->second;
}

//bool DxfLayer::isDefaultLayer()
//{
//  return this == DxfLayer::getDefaultLayer();
//}
//

void DxfLayer::write(DxfWriter *outFile)
{
	DxfTable::write(outFile);                                          //写入表的类型名称、句柄、类分隔符等数据
 
	outFile->writeString(2,this->layerName);                           //写入layer的名称

	outFile->writeInt(70,this->flags);                                 //写入layer的标记

	outFile->writeInt(62,this->color);                                 //写入layer的颜色
	
	outFile->writeString(6,this->linetype);                            //写入layer的线型
                                                                       
	                                                                   //以下是参照生成的dxf文件的默认值写的

	outFile->writeInt(370,-3);                                         //固定用法,线宽                                   

	outFile->writeHex(390,0xF);                                        //固定用法,打印样式
}

void DxfLayer::read(DxfReader *inFile)
{
	int groupCode;
	string value;
	
	inFile->readGroupCode(groupCode);
	inFile->readString(value);

	int i=0;

	if(groupCode==0 && value=="LAYER") {                               //开始循环读取组码和组值,并设置layer的各项数据
		inFile->readGroupCode(groupCode);
		while(groupCode!=0) {
			switch(groupCode) {
				case 2:
					inFile->readString(value);
					this->layerName=value;
					break;
				case 70:
					inFile->readInt(i);
					this->flags=i;
					break;
				case 62:
					inFile->readInt(i);
					this->color=i;
					break;
				case 6:
					inFile->readString(value);
					this->linetype=value;
					break;
				default:
					inFile->readString(value);
					break;
			}
			inFile->readGroupCode(groupCode);
		}
		inFile->goBack(1);
	}	
}

//静态方法
DxfLayer* DxfLayer::createLayer(const string& name,int col,const string& ltype,int f)
{
	DxfLayer* p=new DxfLayer(name,col,ltype,f);
	if(p) {
		map<string,DxfLayer* >::iterator itr=TablesSection::layers.find(name);
		if(itr==TablesSection::layers.end())
			return p;
		else 
			return itr->second;
	}
	else
		return NULL;
}

//string DxfLayer::getName() 
//{ 
//	return this->getLayerName(); 
//}
//
//int DxfLayer::typeId() const 
//{
//	return LAYER; 
//}