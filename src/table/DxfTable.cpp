#include <dxf/table/DxfTable.h>
#include <dxf/Base.h>

//表类型名称映射
TableTypeName DxfTable::tableTypeName;

bool DxfTable::first=true;

/**
 * 当第一次创建table的时候，设置好各table名称的映射关系.
 * <p>
 * <ol>
 * <li>LTYPE--"LTYPE"--"AcDbLinetypeTableRecord"
 *
 * <li>LAYER--"LAYER"--"AcDbLayerTableRecord"
 *
 * <li>STYLE--"STYLE"--"AcDbTextStyleTableRecord"
 *
 * <li>DIMSTYLE--"DIMSTYLE"--"AcDbDimStyleTableRecord"
 *
 * <li>APPID--"APPID"--"AcDbRegAppTableRecord"
 *
 * <li>UCS--"UCS"--"AcDbUCSTableRecord"
 *
 * <li>VIEW--"VIEW"--"AcDbViewTableRecord"
 *
 * <li>VPORT--"VPORT"--"AcDbViewportTableRecord"
 *
 * <li>BLOCK_RECORD--"BLOCK_RECORD"--"AcDbBlockTableRecord"
 *
 * </ol>
 * </p>
 */
DxfTable::DxfTable(void)
{
	
	if(first) {
		tableTypeName.insert(TableTypeName::value_type(LTYPE,make_pair("LTYPE","AcDbLinetypeTableRecord")));
		tableTypeName.insert(TableTypeName::value_type(LAYER,make_pair("LAYER","AcDbLayerTableRecord")));
		tableTypeName.insert(TableTypeName::value_type(STYLE,make_pair("STYLE","AcDbTextStyleTableRecord")));
		tableTypeName.insert(TableTypeName::value_type(DIMSTYLE,make_pair("DIMSTYLE","AcDbDimStyleTableRecord")));
		tableTypeName.insert(TableTypeName::value_type(APPID,make_pair("APPID","AcDbRegAppTableRecord")));
		tableTypeName.insert(TableTypeName::value_type(UCS,make_pair("UCS","AcDbUCSTableRecord")));
		tableTypeName.insert(TableTypeName::value_type(VIEW,make_pair("VIEW","AcDbViewTableRecord")));
		tableTypeName.insert(TableTypeName::value_type(VPORT,make_pair("VPORT","AcDbViewportTableRecord")));
		tableTypeName.insert(TableTypeName::value_type(BLOCK_RECORD,make_pair("BLOCK_RECORD","AcDbBlockTableRecord")));

		first=false;
	}
}

DxfTable::~DxfTable(void)
{

}

void DxfTable::read(DxfReader* inFile)
{

}

void DxfTable::write(DxfWriter* outFile)
{
	outFile->writeString(0,tableTypeName[this->typeId()].first);                    //写表类型名

	if(this->typeId()==LAYER) {
		if(this->getName()=="0") {                                                  //图层0的句柄为固定值--0x10
			outFile->writeHex(5,0x10);
		}
		else
			outFile->writeHex(5,outFile->handle());                                 //其它的图层的句柄为生成的一个独一无二的句柄
	}
	else if(this->typeId()==LTYPE) {
		string s=this->getName();
		if(s=="ByBlock") {
			outFile->writeHex(5,0x14);                                              //线型ByBlock的句柄为固定值--0x14
		}
		else if(s=="ByLayer") {                         
			outFile->writeHex(5,0x15);                                              //线型ByLayer的句柄为固定值--0x15
		}
		else if(s=="Continuous") {
			outFile->writeHex(5,0x16);                                              //线型Continuous的句柄为固定值--0x16
		}
		else 
			outFile->writeHex(5,outFile->handle());                                 //其它线型的句柄为生成的一个独一无二的句柄
	}

	outFile->writeString(100,"AcDbSymbolTableRecord");                              //类分隔符
	outFile->writeString(100,tableTypeName[this->typeId()].second);                 //类分隔符
}
