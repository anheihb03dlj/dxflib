#include <dxf/table/DxfLineType.h>
#include <dxf/Base.h>
#include <dxf/section/TablesSection.h>

//静态成员初始化
map<string,DxfLineType*> DxfLineType::predefineLtypes;

bool DxfLineType::first=true;


/**
 * 当线型初始化的时候，默认设置
 * <ol>
 * <li>线型元素个数--0
 * <li>线型元素总长度--0.0
 * </ol>
 */
//DxfLineType::DxfLineType()
//{
//	this->elementsCount=0;
//	this->totalLength=0.0;
//}

//DxfLineType::DxfLineType(const string& ltname,const string& desc,int eleCount,double totalLen)
//:ltypeName(ltname),description(desc),elementsCount(eleCount),totalLength(totalLen)
//{
//}

//DxfLineType::~DxfLineType(void)
//{
//
//}

//void DxfLineType::setLineTypeName(const string &ltname)
//{
//	this->ltypeName=ltname;
//}
//
//string DxfLineType::getLineTypeName()
//{
//	return this->ltypeName;
//}
//
//void DxfLineType::setDescription(const string &desc)
//{
//	this->description=desc;
//}
//
//string DxfLineType::getDescription()
//{
//	return this->description;
//}


void DxfLineType::write(DxfWriter *outFile)
{
	DxfTable::write(outFile);                                        //写入LineType的类型名、句柄、类分隔符等信息

	outFile->writeString(2,this->ltypeName);                         //写入线型的名称

	outFile->writeInt(70,0);                                         //写入线型个数

	outFile->writeString(3,this->description);                       //写入线型的描述

	outFile->writeInt(72,65);                                        //固定用法

	outFile->writeInt(73,this->elementsCount);                       //写入线型元素个数

	outFile->writeReal(40,this->totalLength);                        //写入线型元素总长度

	vector<double>::iterator itr;
	for(itr=elements.begin();itr!=elements.end();itr++) {            //写入线型元素值
		outFile->writeReal(49,*itr);
		outFile->writeInt(74,0);
		                                                             //注意到ltype中的74组和cad的版本有关，
																	 //如果dxf文件的标题变量中没有设置cad的版本号($ACADVER)，
																	 //则cad将dxf文件默认看作R12版本
																	 //R12版本是不需要74组的,而2000版本则需要74组,
																	 //并且牵扯到相关的句柄值问题(5组)
	}

}

void DxfLineType::read(DxfReader *inFile)
{
	int groupCode;
	string value;
	
	inFile->readGroupCode(groupCode);
	inFile->readString(value);

	int i=0;
	double d=0.0;

	if(groupCode==0 && value=="LTYPE") {
		inFile->readGroupCode(groupCode);                            //循环读取并设置线型的各项数据
		while(groupCode!=0) {
			switch(groupCode) {
				case 2:
					inFile->readString(value);
					this->ltypeName=value;
					break;
				case 3:
					inFile->readString(value);
					this->description=value;
					break;
				case 73:
					inFile->readInt(i);
					this->elementsCount=i;
					break;
				case 40:
					inFile->readReal(d);
					this->totalLength=d;
					break;
				case 49:
					inFile->readReal(d);
					this->elements.push_back(d);
					break;
				default:
					inFile->readString(value);
					break;
			}
			inFile->readGroupCode(groupCode);
		}
		inFile->goBack(1);
	}
}



//静态方法

//初始化过程，创建3条默认线型，同时创建其它的线型并添加到预定义线型中
void DxfLineType::initial()
{
	DxfLineType* p=NULL;

	if(first) {

		//将3条默认线型添加到tablesSection的ltypes中

		p=new DxfLineType("ByBlock","");
		if(p) TablesSection::ltypes.insert(map<string,DxfLineType* >::value_type("ByBlock",p));

		p=new DxfLineType("ByLayer","");
		if(p) TablesSection::ltypes.insert(map<string,DxfLineType* >::value_type("ByLayer",p));

		p=new DxfLineType("Continuous","Solid Line");
		if(p) TablesSection::ltypes.insert(map<string,DxfLineType* >::value_type("Continuous",p));


		//将其它的预定义线型加载到DxfLineType的predefineLtypes中，
		//当需要的时候可以从这里加载到tablesSection的ltypes中
		//该功能类似于cad中的线型加载功能

		p=new DxfLineType("ACAD_ISO02W100","ISO Dashed __ __ __ __ __ __ __ __ __ __ _",2,15.0);
		if(p) {
			p->elements.push_back(12.0);
			p->elements.push_back(-3.0);
			predefineLtypes.insert(map<string,DxfLineType* >::value_type("ACAD_ISO02W100",p));
		}

		p=new DxfLineType("ACAD_ISO03W100","ISO Dashed with Distance __    __    __    _",2,30.0);
		if(p) {
			p->elements.push_back(12.0);
			p->elements.push_back(-18.0);
			predefineLtypes.insert(map<string,DxfLineType* >::value_type("ACAD_ISO03W100",p));
		}

		p=new DxfLineType("ACAD_ISO04W100","ISO Long Dashed Dotted ____ . ____ . __",4,30.0);
		if(p) {
			p->elements.push_back(24.0);
			p->elements.push_back(-3.0);
			p->elements.push_back(0.0);
			p->elements.push_back(-3.0);
			predefineLtypes.insert(map<string,DxfLineType* >::value_type("ACAD_ISO04W100",p));
		}

		p=new DxfLineType("ACAD_ISO05W100","ISO Long Dashed Double Dotted ____ .. __",6,33.0);
		if(p) {
			p->elements.push_back(24.0);
			p->elements.push_back(-3.0);
			p->elements.push_back(0.0);
			p->elements.push_back(-3.0);
			p->elements.push_back(0.0);
			p->elements.push_back(-3.0);
			predefineLtypes.insert(map<string,DxfLineType* >::value_type("ACAD_ISO05W100",p));
		}

		p=new DxfLineType("BORDER","Border __ __ . __ __ . __ __ . __ __ . __ __ .",6,44.5);
		if(p) {
			p->elements.push_back(12.7);
			p->elements.push_back(-6.35);
			p->elements.push_back(12.7);
			p->elements.push_back(-6.35);
			p->elements.push_back(0.0);
			p->elements.push_back(-6.35);
			predefineLtypes.insert(map<string,DxfLineType* >::value_type("BORDER",p));
		}

		p=new DxfLineType("BORDER2","Border (.5x) __.__.__.__.__.__.__.__.__.__.__.",6,22.225);
		if(p) {
			p->elements.push_back(6.35);
			p->elements.push_back(-3.175);
			p->elements.push_back(6.35);
			p->elements.push_back(-3.175);
			p->elements.push_back(0.0);
			p->elements.push_back(-3.175);
			predefineLtypes.insert(map<string,DxfLineType* >::value_type("BORDER2",p));
		}

		p=new DxfLineType("BORDERX2","Border (2x) ____  ____  .  ____  ____  .  ___",6,88.9);
		if(p) {
			p->elements.push_back(25.4);
			p->elements.push_back(-12.7);
			p->elements.push_back(25.4);
			p->elements.push_back(-12.7);
			p->elements.push_back(0.0);
			p->elements.push_back(-12.7);
			predefineLtypes.insert(map<string,DxfLineType* >::value_type("BORDERX2",p));
		}

		p=new DxfLineType("CENTER","Center ____ _ ____ _ ____ _ ____ _ ____ _ ____",4,50.8);
		if(p) {
			p->elements.push_back(31.75);
			p->elements.push_back(-6.35);
			p->elements.push_back(6.35);
			p->elements.push_back(-6.35);
			predefineLtypes.insert(map<string,DxfLineType* >::value_type("CENTER",p));
		}

		p=new DxfLineType("CENTER2","Center (.5x) ___ _ ___ _ ___ _ ___ _ ___ _ ___",4,28.575);
		if(p) {
			p->elements.push_back(19.05);
			p->elements.push_back(-3.175);
			p->elements.push_back(3.175);
			p->elements.push_back(-3.175);
			predefineLtypes.insert(map<string,DxfLineType* >::value_type("CENTER2",p));
		}

		p=new DxfLineType("CENTERX2","Center (2x) ________  __  ________  __  _____",4,101.6);
		if(p) {
			p->elements.push_back(63.5);
			p->elements.push_back(-12.7);
			p->elements.push_back(12.7);
			p->elements.push_back(-12.7);
			predefineLtypes.insert(map<string,DxfLineType* >::value_type("CENTERX2",p));
		}

		p=new DxfLineType("DASHDOT","Dash dot __ . __ . __ . __ . __ . __ . __ . __",4,25.4);
		if(p) {
			p->elements.push_back(12.7);
			p->elements.push_back(-6.35);
			p->elements.push_back(0.0);
			p->elements.push_back(-6.35);
			predefineLtypes.insert(map<string,DxfLineType* >::value_type("DASHDOT",p));
		}

		p=new DxfLineType("DASHDOT2","Dash dot (.5x) _._._._._._._._._._._._._._._.",4,12.7);
		if(p) {
			p->elements.push_back(6.35);
			p->elements.push_back(-3.175);
			p->elements.push_back(0.0);
			p->elements.push_back(-3.175);
			predefineLtypes.insert(map<string,DxfLineType* >::value_type("DASHDOT2",p));
		}

		p=new DxfLineType("DASHDOTX2","Dash dot (2x) ____  .  ____  .  ____  .  ___",4,50.8);
		if(p) {
			p->elements.push_back(25.4);
			p->elements.push_back(-12.7);
			p->elements.push_back(0.0);
			p->elements.push_back(-12.7);
			predefineLtypes.insert(map<string,DxfLineType* >::value_type("DASHDOTX2",p));
		}

		p=new DxfLineType("DASHED","Dashed __ __ __ __ __ __ __ __ __ __ __ __ __ _",2,19.5);
		if(p) {
			p->elements.push_back(12.7);
			p->elements.push_back(-6.35);
			predefineLtypes.insert(map<string,DxfLineType* >::value_type("DASHED",p));
		}

		p=new DxfLineType("DASHED2","Dashed (.5x) _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _",2,9.525);
		if(p) {
			p->elements.push_back(6.35);
			p->elements.push_back(-3.175);
			predefineLtypes.insert(map<string,DxfLineType* >::value_type("DASHED2",p));
		}

		p=new DxfLineType("DASHEDX2","Dashed (2x) ____  ____  ____  ____  ____  ___",2,38.1);
		if(p) {
			p->elements.push_back(25.4);
			p->elements.push_back(-12.7);
			predefineLtypes.insert(map<string,DxfLineType* >::value_type("DASHEDX2",p));
		}

		p=new DxfLineType("DIVIDE","Divide ____ . . ____ . . ____ . . ____ . . ____",6,31.75);
		if(p) {
			p->elements.push_back(12.7);
			p->elements.push_back(-6.35);
			p->elements.push_back(0.0);
			p->elements.push_back(-6.35);
			p->elements.push_back(0.0);
			p->elements.push_back(-6.35);
			predefineLtypes.insert(map<string,DxfLineType* >::value_type("DIVIDE",p));
		}

		p=new DxfLineType("DIVIDE2","Divide (.5x) __..__..__..__..__..__..__..__.._",6,15.875);
		if(p) {
			p->elements.push_back(6.35);
			p->elements.push_back(-3.175);
			p->elements.push_back(0.0);
			p->elements.push_back(-3.175);
			p->elements.push_back(0.0);
			p->elements.push_back(-3.175);
			predefineLtypes.insert(map<string,DxfLineType* >::value_type("DIVIDE2",p));
		}

		p=new DxfLineType("DIVIDEX2","Divide (2x) ________  .  .  ________  .  .  _",6,63.5);
		if(p) {
			p->elements.push_back(25.4);
			p->elements.push_back(-12.7);
			p->elements.push_back(0.0);
			p->elements.push_back(-12.7);
			p->elements.push_back(0.0);
			p->elements.push_back(-12.7);
			predefineLtypes.insert(map<string,DxfLineType* >::value_type("DIVIDEX2",p));
		}

		p=new DxfLineType("DOT","Dot . . . . . . . . . . . . . . . . . . . . . .",2,6.35);
		if(p) {
			p->elements.push_back(0.0);
			p->elements.push_back(-6.35);
			predefineLtypes.insert(map<string,DxfLineType* >::value_type("DOT",p));
		}

		p=new DxfLineType("DOT2","Dot (.5x) .....................................",2,3.175);
		if(p) {
			p->elements.push_back(0.0);
			p->elements.push_back(-3.175);
			predefineLtypes.insert(map<string,DxfLineType* >::value_type("DOT2",p));
		}

		p=new DxfLineType("DOTX2","Dot (2x) .  .  .  .  .  .  .  .  .  .  .  .  .",2,12.7);
		if(p) {
			p->elements.push_back(0.0);
			p->elements.push_back(-12.7);
			predefineLtypes.insert(map<string,DxfLineType* >::value_type("DOTX2",p));
		}

		//more lineTypes

		first=false;
	}
}


DxfLineType* DxfLineType::load(const string& ltname)
{
	if(ltname.length()==0) return NULL;

	DxfLineType* pltype=NULL;
	map<string,DxfLineType*>::iterator itr=predefineLtypes.find(ltname);                                               //在预定义线型中查找线型
	if(itr!=predefineLtypes.end()) {
		TablesSection::ltypes.insert(map<string,DxfLineType*>::value_type(itr->second->getLineTypeName(),itr->second));//找到线型之后，将它添加到已有线型集合中，并返回指针
		pltype=itr->second;
		predefineLtypes.erase(itr);
		return pltype;
	}
	else
		return NULL;
}

DxfLineType* DxfLineType::createLineType()
{
	DxfLineType* p=NULL;

	if(first)
		initial();

	p=new DxfLineType;
	if(p)
		return p;
	else 
		return NULL;

}


DxfLineType* DxfLineType::createLineType(const string& ltname,const string& desc,int eleCount,double totalLen)
{
	DxfLineType* p=NULL;

	if(first)
		initial();

	map<string,DxfLineType*>::iterator itr=TablesSection::ltypes.find(/*s*/ltname);
	if(itr!=TablesSection::ltypes.end()) 
		return itr->second;
	else {
		p=load(ltname);                                                 //如果该线型不存在，则尝试加载其它预定义线型
		if(p==NULL) {
			p=new DxfLineType(ltname,desc,eleCount,totalLen);
			if(p)
				return p;
			else 
				return NULL;
		}
		else 
			return p;
	}
}

//string DxfLineType::getName()
//{ 
//	return this->getLineTypeName(); 
//}
//
//int DxfLineType::typeId() const
//{
//	return LTYPE; 
//}