#include <iostream>
#include <dxf/section/Section.h>

#include <dxf/section/TablesSection.h>
#include <dxf/section/BlocksSection.h>
#include <dxf/section/EntitiesSection.h>

//静态成员初始化
TablesSection* Section::tablesSection=NULL;
BlocksSection* Section::blocksSection=NULL;
EntitiesSection* Section::entitiesSection=NULL;


Section::Section()
{

}

Section::~Section(void)
{

}


//静态方法(简单工厂模式)
void Section::createSection(const string& sectionName)
{
	string s=StringExt::to_upper_copy(sectionName);

	if (s == "TABLES") {
		if(tablesSection==NULL)
			tablesSection=new TablesSection;
	}

	if (s == "BLOCKS") {
		if(blocksSection==NULL)
			blocksSection=new BlocksSection;
	}

	if (s == "ENTITIES") {
		if(entitiesSection==NULL)
			entitiesSection=new EntitiesSection;
	}

}

void Section::beginSection(DxfWriter* outFile)
{
	outFile->writeString(0,"SECTION");                                      //开始section，组码--0，组值--SECTION

	outFile->writeString(2,this->getSectionName());                         //section类型，组码--2，组值--TABLES或者ENTITIES等等
}

void Section::endSection(DxfWriter *outFile)
{
	outFile->writeString(0,"ENDSEC");                                       //写section结尾
}
