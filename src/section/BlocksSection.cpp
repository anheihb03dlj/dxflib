#include <dxf/section/BlocksSection.h>
#include <dxf/DxfReader.h>
#include <dxf/DxfWriter.h>

#include <iostream>
#include <map>
using std::map;

//静态成员初始化
map<string,DxfBlock*> BlocksSection::blocks;

BlocksSection::BlocksSection(void)
{
}

BlocksSection::~BlocksSection(void)
{
#ifdef _DEBUG
	std::cout<<"调用BlocksSection::~BlocksSection(void)"<<std::endl;
#endif
	clear();
}

void BlocksSection::clear()
{
#ifdef _DEBUG
	std::cout<<"调用BlocksSection::clear()"<<std::endl;
#endif

	DxfBlock::first=true;
	map<string,DxfBlock*>::iterator itr;
	for(itr=blocks.begin();itr!=blocks.end();) {
#ifdef _DEBUG
		std::cout<<"删除"<<itr->second->getBlockName()<<std::endl;
#endif
		delete itr->second;
		blocks.erase(itr++);
	}
}

void BlocksSection::beginBlock(DxfWriter* outFile,string blockName)
{
	outFile->writeString(0,"BLOCK");                                             //block开始

	if(blockName=="*Model_Space") {                                      
		outFile->writeHex(5,0x20);                                               //固定用法，model_space的句柄为0x20
	}
	else if(blockName=="*Paper_Space") {
		outFile->writeHex(5,0xD4);                                               //固定用法，paper_space的句柄为0xD4
	}
	else if(blockName=="*Paper_Space0") {
		outFile->writeHex(5,0xD8);                                               //固定用法，paper_space0的句柄为0xD8
	}
	else {
		outFile->writeHex(5,outFile->handle());                                  //其它的块的句柄为生成的一个独一无二的句柄值
	}
		
	outFile->writeString(100,"AcDbEntity");

	if(blockName=="*Paper_Space")
		outFile->writeInt(67,1);                                                 //固定用法
	
	outFile->writeString(8,"0");                                                 //block的图层总是为0层

	outFile->writeString(100,"AcDbBlockBegin");
}

void BlocksSection::endBlock(DxfWriter* outFile,string blockName)
{
	outFile->writeString(0,"ENDBLK");                                           //block结束

	if(blockName=="*Model_Space") {
		outFile->writeHex(5,0x21);                                              //同上面的句柄是对应的(0x20-0x21)
	}
	else if(blockName=="*Paper_Space") {
		outFile->writeHex(5,0xD5);                                              //同上面的句柄是对应的(0xD4-0xD5)
	}
	else if(blockName=="*Paper_Space0") {
		outFile->writeHex(5,0xD9);                                              //同上面的句柄是对应的(oxD8-0xD9)
	}
	else {
		outFile->writeHex(5,outFile->handle());                                 //在上面生成的句柄值的基础上+1
	}

	outFile->writeString(100,"AcDbEntity");

	if(blockName=="*Paper_Space")
		outFile->writeInt(67,1);

	outFile->writeString(8,"0");

	outFile->writeString(100,"AcDbBlockEnd");
}



void BlocksSection::addBlock( DxfBlock *block)
{
	if(block)
		blocks.insert(map<string,DxfBlock*>::value_type(block->getBlockName(),block));
}

void BlocksSection::removeBlock(const string &blockName)
{
	if(blockName.length()==0) return;

	map<string,DxfBlock*>::iterator itr=blocks.find(blockName);
	if(itr!=blocks.end()) {
		delete itr->second;
		blocks.erase(itr);
	}
}

DxfBlock* BlocksSection::getBlock(const string &blockName)
{
	if(blockName.length()==0) return NULL;

	map<string,DxfBlock*>::iterator itr=blocks.find(blockName);
	if(itr!=blocks.end())
		return itr->second;
	else 
		return NULL;
}


void BlocksSection::write(DxfWriter *outFile)
{
	Section::beginSection(outFile);                                                //块节开始
 
	map<string,DxfBlock*>::iterator itr;
	for(itr=blocks.begin();itr!=blocks.end();itr++) {
	                                                                               //如果是默认块，则不检测是否包含实体（它们本身也确实没有实体）
		if(!itr->second->isDefaultBlock()) 
			if(itr->second->countEntities()==0) {
				std::cerr<<"定义了一个不包含任何实体的块！"<<std::endl;
				exit(1);
			}

		this->beginBlock(outFile,itr->second->getBlockName());                     //块定义开始
		
		itr->second->write(outFile);
		
		this->endBlock(outFile,itr->second->getBlockName());                      //块定义结束
	}

	Section::endSection(outFile);                                                 //块节结束
}

void BlocksSection::read(DxfReader *inFile)
{
	int groupCode;
	string value;

	do {
		
		this->readBlock(inFile);

		inFile->readGroupCode(groupCode);
		inFile->readString(value);
		inFile->goBack(2);
		
	}while(!(groupCode==0 && value=="ENDSEC"));

	inFile->readGroupCode(groupCode);
	inFile->readString(value);
}



void BlocksSection::readBlock(DxfReader* inFile)
{
	DxfBlock* block=NULL;

	int groupCode;
	string value;

	inFile->readGroupCode(groupCode);
	inFile->readString(value);

	if(groupCode!=0) {
		std::cerr<<"第"<<inFile->getCurrentLine()<<"行与预期的数据不符"<<std::endl;
		std::cerr<<"组码应为0"<<std::endl;
		std::cerr<<"程序退出......"<<std::endl;
		exit(1);
	}
	
	if(value=="BLOCK") {
		inFile->goBack(2);

		block=DxfBlock::createBlock();
		block->read(inFile);
		this->addBlock(block);
	}
	else {
		std::cerr<<"第"<<inFile->getCurrentLine()<<"行与预期的数据不符"<<std::endl;
		std::cerr<<"未知块类型"<<std::endl;
		std::cerr<<"程序退出......"<<std::endl;
		exit(1);
	}	
}
