#include <dxf/Base.h>
#include <dxf/section/TablesSection.h>
#include <dxf/table/DxfTable.h>
#include <dxf/table/DxfLayer.h>
#include <dxf/table/DxfLineType.h>
//将来会添加更多的表,如ucs、style、dimstyle、view、vport等

#include <dxf/section/BlocksSection.h>

#include <iostream>

//静态成员初始化
map<string,DxfLayer*> TablesSection::layers;
map<string,DxfLineType*> TablesSection::ltypes;
list<string> TablesSection::appNames;


TablesSection::TablesSection(void)
{

}

TablesSection::~TablesSection(void)
{
#ifdef _DEBUG
	std::cout<<"调用TablesSection::~TablesSection(void)"<<std::endl;
#endif
	clear();
}

void TablesSection::clear()
{
#ifdef _DEBUG
	std::cout<<"调用TablesSection::clear()"<<std::endl;
#endif

	//DxfLayer::defaultLayer=NULL;//将默认图层的指针置0
	map<string,DxfLayer*>::iterator itr1;
	for(itr1=layers.begin();itr1!=layers.end();) {
#ifdef _DEBUG
		std::cout<<"删除"<<itr1->second->getLayerName()<<std::endl;
#endif
		delete itr1->second;
		layers.erase(itr1++);
	}

	//DxfLineType::first=true;
	map<string,DxfLineType*>::iterator itr2;
	for(itr2=ltypes.begin();itr2!=ltypes.end();) {
#ifdef _DEBUG
		std::cout<<"删除线型"<<itr2->second->getLineTypeName()<<std::endl;
#endif
		delete itr2->second;
		ltypes.erase(itr2++);
	}

#ifdef _DEBUG
	std::cout<<"测试，是否输出......"<<std::endl;
#endif

	map<string,DxfLineType*>::iterator itr;
	for(itr=DxfLineType::predefineLtypes.begin(); itr!=DxfLineType::predefineLtypes.end(); ) {
#ifdef _DEBUG
		std::cout<<"删除预定义线型"<<itr->second->getLineTypeName()<<std::endl;
#endif
		delete itr->second;
		DxfLineType::predefineLtypes.erase(itr++);
	}

	appNames.clear();
}

void TablesSection::beginTable(DxfWriter* outFile,const string& tableName)
{
	outFile->writeString(0,"TABLE");                                                  //表开始

	string s=StringExt::to_upper_copy(tableName);
	if(s=="LTYPE") {
		outFile->writeString(2,s);                                                    //2--LTYPE
		outFile->writeHex(5,0x5);                                                     //固定用法，5--0x5(句柄)
	}
	else if(s=="LAYER") {
		outFile->writeString(2,s);                                                    //2--LAYER
		outFile->writeHex(5,0x2);                                                     //固定用法，5--0x2(句柄)
	}
	else if(s=="APPID") {
		outFile->writeString(2,s);                                                    //2--APPID
		outFile->writeHex(5,0x9);                                                     //固定用法，5--0x9(句柄)
	}
	else if(s=="BLOCK_RECORD") {
		outFile->writeString(2,s);                                                    //2--BLOCK_RECORD
		outFile->writeHex(5,0x1);                                                     //固定用法，5--0x1(句柄)
	}
	else if(s=="DIMSTYLE") {
		outFile->writeString(2,s);                                                    //2--DIMSTYLE
		outFile->writeHex(5,0xA);                                                     //固定用法，5--0xA(句柄)
	}
	else if(s=="STYLE") {
		outFile->writeString(2,s);                                                    //2--STYLE
		outFile->writeHex(5,0x3);                                                     //固定用法，5--0x3(句柄)
	}
	else if(s=="UCS") {
		outFile->writeString(2,s);                                                    //2--UCS
		outFile->writeHex(5,0x7);                                                     //固定用法，5--0x7(句柄)
	}
	else if(s=="VIEW") {
		outFile->writeString(2,s);                                                    //2--VIEW
		outFile->writeHex(5,0x6);                                                     //固定用法，5--0x6(句柄)
	}
	else if(s=="VPORT") {
		outFile->writeString(2,s);                                                    //2--VPORT
		outFile->writeHex(5,0x8);                                                     //固定用法，5--0x8(句柄)
	}
	else {
		std::cerr<<"未知类型的表,程序退出！"<<std::endl;
		exit(1);
	}

	outFile->writeString(100,"AcDbSymbolTable");                                      //类分隔符
}

void TablesSection::endTable(DxfWriter* outFile)
{
	outFile->writeString(0,"ENDTAB");                                                 //表结束
}

void TablesSection::write(DxfWriter *outFile)
{
	//表的顺序没有严格要求，但如果存在linetype表，则必须出现在layer的前面

	Section::beginSection(outFile);                                                   //table节开始


	this->writeVPort(outFile);	                                                      //写vport表


	if(ltypes.size()!=0) {                                                           //写linetype表

		this->beginTable(outFile,"ltype");                                           //表开始

		outFile->writeInt(70,ltypes.size());                                         //最大表项数

		                                                                             //先写默认的线型
		ltypes["ByBlock"]->write(outFile);
		ltypes["ByLayer"]->write(outFile);
		ltypes["Continuous"]->write(outFile);

		map<string,DxfLineType*>::iterator itr_ltype;                                //如果存在其它线型，且不是默认内部线型时，写入到dxf文件中
		for(itr_ltype=ltypes.begin();itr_ltype!=ltypes.end();itr_ltype++) {

			if(itr_ltype->second->isDefaultLineType()) continue;

			itr_ltype->second->write(outFile);
		}
		this->endTable(outFile);                                                      //表结束
	}


	if(layers.size()!=0) {	                                                          //写layer表

		this->beginTable(outFile,"layer");                                            //表开始

		outFile->writeInt(70,layers.size());                                          //最大表项数

		layers["0"]->write(outFile);                                                  //先写默认的0层

		map<string,DxfLayer*>::iterator itr_layer;                                    //如果存在其它的图层，且不是默认的0层时，写入到dxf文件
		for(itr_layer=layers.begin();itr_layer!=layers.end();itr_layer++) {
			
			if(itr_layer->second->isDefaultZeroLayer()) continue;

			itr_layer->second->write(outFile);
		}
		this->endTable(outFile);                                                      //表结束
	}

	                                                                                  //继续写其他的表

	                                                                                 // 为了保证dxf文件的完整性和合理性，
																					 // 下面的几个表的内容是固定且不能也没有必要修改

	this->writeStyle(outFile);                                                        //写style表

	this->writeView(outFile);                                                         //写view表

	this->writeUcs(outFile);                                                          //写ucs表

	this->writeAppid(outFile);                                                        //写appid表

	this->writeDimStyle(outFile);                                                     //写dimstyle表

	this->writeBlockRecord(outFile);                                                  //写block_record表

	Section::endSection(outFile);                                                     //table节结束
}


void TablesSection::read(DxfReader* inFile)
{
	int groupCode=0;
	string value;
	
	do {
		value=this->readBeginTable(inFile);
		if(value=="VPORT" || value=="STYLE"  || value=="VIEW"     ||
		    value=="UCS"  || value=="APPID"  || value=="DIMSTYLE" ||
			value=="BLOCK_RECORD" ) {

			processNothing(inFile);                                                    //什么也不处理，只是简单的忽略
		}
		else if(value=="LTYPE" || value=="LAYER") {
			this->readBeginTableEntry(inFile,value);                                   //忽略不需要的组（如句柄，类分隔符等）

			                                                                           //以下代码为核心部分
			inFile->readGroupCode(groupCode);
			inFile->readString(value);
			inFile->goBack(2);

			while(!(groupCode==0 && value=="ENDTAB")) {

				this->readTableEntry(inFile);                                          //读取各个表项

				inFile->readGroupCode(groupCode);
				inFile->readString(value);
				inFile->goBack(2);

			}
			inFile->readGroupCode(groupCode);
			inFile->readString(value);
		}
		else {
			std::cerr<<"第"<<inFile->getCurrentLine()<<"行与预期的数据不符"<<std::endl;
			std::cerr<<"未知表类型!"<<std::endl;
			std::cerr<<"程序退出......"<<std::endl;
			exit(1);
		}

		inFile->readGroupCode(groupCode);
		inFile->readString(value);
		inFile->goBack(2);

	}while(!(groupCode==0 && value=="ENDSEC") /*&& !inFile->isEndOfFile()*/);

	inFile->readGroupCode(groupCode);                                                 //重新定位
	inFile->readString(value);

}

void TablesSection::addTable(DxfTable *table)
{
	if(table) {
		if(table->typeId()==LAYER) {
			DxfLayer* layer=dynamic_cast<DxfLayer*>(table);
			if(layer) {
				layers.insert(map<string,DxfLayer*>::value_type(layer->getName(),layer));
			}
		}
		else if(table->typeId()==LTYPE) {
			DxfLineType* ltype=dynamic_cast<DxfLineType*>(table);
			if(ltype)
				ltypes.insert(map<string,DxfLineType*>::value_type(ltype->getName(),ltype));
		}
	}
}

void TablesSection::removeLayer(const string &tableName)
{
	map<string,DxfLayer*>::iterator itr=layers.find(tableName);
	if(itr!=layers.end())
		layers.erase(itr);
}

DxfLayer* TablesSection::getLayer(const string &layerName)
{
	map<string,DxfLayer*>::iterator itr=layers.find(layerName);
	if(itr!=layers.end())
		return itr->second;
	else 
		return NULL;
}

DxfLineType* TablesSection::getLineType(const string &ltypeName)
{
	map<string,DxfLineType*>::iterator itr=ltypes.find(ltypeName);
	if(itr!=ltypes.end())
		return itr->second;
	else
		return NULL;
}





//写其他的表

//写块表记录(block_record)的开始部分
void TablesSection::beginBlockRecord(DxfWriter* outFile)
{
	outFile->writeString(0, "TABLE");
	outFile->writeString(2, "BLOCK_RECORD");
	outFile->writeHex(5, 0x1);//cad2000固定值
	outFile->writeString(100, "AcDbSymbolTable");
	outFile->writeInt(70, BlocksSection::blocks.size());
}

void TablesSection::writeDefaultRecord(DxfWriter* outFile)
{
	outFile->writeString(0, "BLOCK_RECORD");
	outFile->writeHex(5, 0x1F);//cad2000固定值
	outFile->writeString(100, "AcDbSymbolTableRecord");
	outFile->writeString(100, "AcDbBlockTableRecord");
	outFile->writeString( 2, "*Model_Space");
	outFile->writeHex(340, 0x22);//cad2000固定值

	outFile->writeString(0, "BLOCK_RECORD");
	outFile->writeHex(5, 0xD2);//cad2000固定值
	outFile->writeString(100, "AcDbSymbolTableRecord");
	outFile->writeString(100, "AcDbBlockTableRecord");
	outFile->writeString(2, "*Paper_Space");
	outFile->writeHex(340, 0xD3);//cad2000固定值

	outFile->writeString(0, "BLOCK_RECORD");
	outFile->writeHex(5, 0xD6);//cad2000固定值
	outFile->writeString(100, "AcDbSymbolTableRecord");
	outFile->writeString(100, "AcDbBlockTableRecord");
	outFile->writeString(2, "*Paper_Space0");
	outFile->writeHex(340, 0xD7);//cad2000固定值
}

void TablesSection::endBlockRecord(DxfWriter* outFile)
{
	outFile->writeString(0,"ENDTAB");
}

void TablesSection::writeBlockRecord(DxfWriter* outFile)
{

	this->beginBlockRecord(outFile);
	this->writeDefaultRecord(outFile);

	map<string,DxfBlock*>::iterator itr;
	for(itr=BlocksSection::blocks.begin();itr!=BlocksSection::blocks.end();itr++) {
		string s=itr->second->getBlockName();
		if(s=="*Model_Space" || s=="*Paper_Space" || s=="*Paper_Space0")  continue;

		outFile->writeString(0,"BLOCK_RECORD");
		outFile->writeHex(5,outFile->handle());
		outFile->writeString(100, "AcDbSymbolTableRecord");
		outFile->writeString(100, "AcDbBlockTableRecord");
		outFile->writeString(2,s);
		outFile->writeHex(340,0);
	}

	this->endBlockRecord(outFile);
}

void TablesSection::writeStyle(DxfWriter* outFile)
{
	outFile->writeString(0,"TABLE");
	outFile->writeString(2,"STYLE");
	outFile->writeHex(5,0x3);//cad2000固定值
	outFile->writeString(100,"AcDbSymbolTable");
	outFile->writeInt(70,1);

	outFile->writeString(0,"STYLE");
	outFile->writeHex(5,0x11);//cad2000固定值
	outFile->writeString(100, "AcDbSymbolTableRecord");
	outFile->writeString(100, "AcDbTextStyleTableRecord");
	outFile->writeString( 2, "Standard");
	outFile->writeInt( 70, 0);
	outFile->writeReal( 40, 0.0);
	outFile->writeReal( 41, 1.0);
	outFile->writeReal( 50, 0.0);
	outFile->writeInt( 71, 0);
	outFile->writeReal( 42, 50.0);//字高
	outFile->writeString(  3, "txt");
	outFile->writeString(  4, "");

	outFile->writeString(  0, "ENDTAB");

}

void TablesSection::writeView(DxfWriter* outFile)
{
	outFile->writeString(  0, "TABLE");
	outFile->writeString(  2, "VIEW");
	outFile->writeHex(5, 0x6);//cad2000固定值
	outFile->writeString(100, "AcDbSymbolTable");
	outFile->writeInt(70, 0);
	outFile->writeString(0, "ENDTAB");
}

void TablesSection::writeUcs(DxfWriter* outFile)
{
	outFile->writeString(  0, "TABLE");
	outFile->writeString(  2, "UCS");
	outFile->writeHex(5, 0x7);//cad2000固定值
	outFile->writeString(100, "AcDbSymbolTable");
	outFile->writeInt(70, 0);
	outFile->writeString(0, "ENDTAB");
}

void TablesSection::writeVPort(DxfWriter* outFile)
{
	outFile->writeString(0, "TABLE");
	outFile->writeString(2, "VPORT");
	outFile->writeHex(5, 0x8);//cad2000固定的句柄

	outFile->writeString(100, "AcDbSymbolTable");
	outFile->writeInt(70, 3);
	outFile->writeString(0, "VPORT");
	outFile->writeHex(5,outFile->handle());
	outFile->writeString(100, "AcDbSymbolTableRecord");
	outFile->writeString(100, "AcDbViewportTableRecord");


	//以下的数值均是参照生成的dxf2000格式文件的内容
	outFile->writeString(  2, "*Active");
	outFile->writeInt( 70, 0);
	outFile->writeReal( 10, 0.0);
	outFile->writeReal( 20, 0.0);
	outFile->writeReal( 11, 1.0);
	outFile->writeReal( 21, 1.0);
	outFile->writeReal( 12, 1688.983231094199);
	outFile->writeReal( 22, 132.8722269508056);
	outFile->writeReal( 13, 0.0);
	outFile->writeReal( 23, 0.0);
	outFile->writeReal( 14, 10.0);
	outFile->writeReal( 24, 10.0);
	outFile->writeReal( 15, 10.0);
	outFile->writeReal( 25, 10.0);
	outFile->writeReal( 16, 0.0);
	outFile->writeReal( 26, 0.0);
	outFile->writeReal( 36, 1.0);
	outFile->writeReal( 17, 0.0);
	outFile->writeReal( 27, 0.0);
	outFile->writeReal( 37, 0.0);
	outFile->writeReal( 40, 1868.257639796519);
	outFile->writeReal( 41, 1.983193277310924);
	outFile->writeReal( 42, 50.0);
	outFile->writeReal( 43, 0.0);
	outFile->writeReal( 44, 0.0);
	outFile->writeReal( 50, 0.0);
	outFile->writeReal( 51, 0.0);
	outFile->writeInt( 71, 0);
	outFile->writeInt( 72,  1000);
	outFile->writeInt( 73, 1);
	outFile->writeInt( 74, 3);
	outFile->writeInt( 75, 0);
	outFile->writeInt( 76, 0);
	outFile->writeInt( 77, 0);
	outFile->writeInt( 78, 0);

	outFile->writeInt(281, 0);
	outFile->writeInt( 65, 1);
	outFile->writeReal(110, 0.0);
	outFile->writeReal(120, 0.0);
	outFile->writeReal(130, 0.0);
	outFile->writeReal(111, 1.0);
	outFile->writeReal(121, 0.0);
	outFile->writeReal(131, 0.0);
	outFile->writeReal(112, 0.0);
	outFile->writeReal(122, 1.0);
	outFile->writeReal(132, 0.0);
	outFile->writeInt( 79, 0);
	outFile->writeReal(146, 0.0);

	outFile->writeString(0, "ENDTAB");
}

void TablesSection::writeAppid(DxfWriter* outFile)
{
	int count=appNames.size();
	outFile->writeString(0,"TABLE");
	outFile->writeString(2,"APPID");
	outFile->writeHex(5,0x9);//cad2000固定值
	outFile->writeString(100,"AcDbSymbolTable");
	outFile->writeInt(70,1+count);//outFile->writeInt(70,3);

	outFile->writeString(0,"APPID");
	outFile->writeHex(5,0x12);//cad2000固定值
	outFile->writeString(100, "AcDbSymbolTableRecord");
	outFile->writeString(100, "AcDbRegAppTableRecord");
	outFile->writeString(2,"ACAD");
	outFile->writeInt(70,0);

	list<string>::iterator itr;
	for(itr=appNames.begin();itr!=appNames.end();itr++) {
		outFile->writeString(0,"APPID");
		outFile->writeHex(5,outFile->handle());//cad2000固定值
		outFile->writeString(100, "AcDbSymbolTableRecord");
		outFile->writeString(100, "AcDbRegAppTableRecord");
		outFile->writeString(2,*itr);//appid名
		outFile->writeInt(70,0);
	}

	outFile->writeString(0,"ENDTAB");
}

void TablesSection::writeDimStyle(DxfWriter* outFile)
{
	outFile->writeString(0,"TABLE");
	outFile->writeString(2,"DIMSTYLE");
	outFile->writeHex(5, 0xA);//cad2000固定值
	outFile->writeString(100, "AcDbSymbolTable");
	outFile->writeInt(70,1);
	outFile->writeString(100,"AcDbDimStyleTable");
	outFile->writeInt(71,1);

	outFile->writeString(0,"DIMSTYLE");//dimstyle的句柄组码为105
	outFile->writeHex(105,0x27);//cad2000固定值
	outFile->writeString(100,"AcDbSymbolTableRecord");
	outFile->writeString(100,"AcDbDimStyleTableRecord");

	outFile->writeString(2,"ISO-25");
	outFile->writeInt(70,0);
	outFile->writeReal(41,2.5);
	outFile->writeReal(42,0.625);
	outFile->writeReal(43,3.75);
	outFile->writeReal(44,1.25);

	outFile->writeInt(73,0);
	outFile->writeInt(74,0);
	outFile->writeInt(77,1);
	outFile->writeInt(78,8);

	outFile->writeReal(140,2.5);
	outFile->writeReal(141,2.5);
	outFile->writeReal(143,0.03937007874016);
	outFile->writeReal(147,0.625);

	outFile->writeInt(171,3);
	outFile->writeInt(172,1);
	outFile->writeInt(271,2);
	outFile->writeInt(272,2);
	outFile->writeInt(274,3);
	outFile->writeInt(278, 44);
	outFile->writeInt(283, 0);
	outFile->writeInt(284,8);
	outFile->writeHex(340,0x11);//cad固定值(参照的style句柄)

	outFile->writeString(0,"ENDTAB");
}


string TablesSection::readBeginTable(DxfReader* inFile)
{
	int groupCode=0;
	string value;
	
	inFile->readGroupCode(groupCode);
	inFile->readString(value);
	if(groupCode==0 && value=="TABLE") {
		inFile->readGroupCode(groupCode);
		inFile->readString(value);

		if(groupCode==2) {

			return value;
		}
		else {
			std::cerr<<"第"<<inFile->getCurrentLine()-1<<"行与预期的数据不符"<<std::endl;
			std::cerr<<"此处的组码应为:"<<2<<",实际组码为:"<<groupCode<<std::endl;
			std::cerr<<"程序退出......"<<std::endl;
			exit(1);			
		}
	}
	else {
		std::cerr<<"第"<<inFile->getCurrentLine()-1<<"行与预期的数据不符"<<std::endl;
		std::cerr<<"Table节始部分出现错误!"<<std::endl;
		std::cerr<<"程序退出......"<<std::endl;		
		exit(1);
	}
}


void TablesSection::processNothing(DxfReader* inFile)
{
	int groupCode=0;
	string value;

	do {

		inFile->readGroupCode(groupCode);
		inFile->readString(value);

	}while(!(groupCode==0 && value=="ENDTAB"));
}


void TablesSection::readBeginTableEntry(DxfReader* inFile,const string& tableName)
{
	int groupCode=0;
	string value;
	do {

		inFile->readGroupCode(groupCode);
		inFile->readString(value);

	}while(!(groupCode==0 && value==tableName));

	inFile->goBack(2);//读指针回滚到当前行的前2行
}


void TablesSection::readTableEntry(DxfReader* inFile/*,const string& tableName*/)
{
	DxfLineType* ltype;
	DxfLayer* layer;

	int groupCode=0;
	string value;

	inFile->readGroupCode(groupCode);
	inFile->readString(value);

	if(groupCode!=0) {
		std::cerr<<"第"<<inFile->getCurrentLine()<<"行与预期的数据不符"<<std::endl;
		std::cerr<<"组码应为0"<<std::endl;
		std::cerr<<"程序退出......"<<std::endl;
		exit(1);
	}


	if(value=="LTYPE") {
		inFile->goBack(2);
		ltype=DxfLineType::createLineType();
		ltype->read(inFile);
		this->addTable(ltype);
	}
	else if(value=="LAYER") {
		inFile->goBack(2);

		//这里存在一个内存方面的漏洞，如果不指定layerName，则生成的layer指针都是指向0层的，这样会导致0层被修改
		//解决方法：任意给定一个layerName
		layer=DxfLayer::createLayer("new");

		layer->read(inFile);
		this->addTable(layer);
	}
	else {
		std::cerr<<"第"<<inFile->getCurrentLine()<<"行与预期的数据不符"<<std::endl;
		std::cerr<<"未知表项"<<std::endl;
		std::cerr<<"程序退出......"<<std::endl;
		exit(1);
	}	

}
