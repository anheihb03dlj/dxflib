#include <dxf/section/EntitiesSection.h>
#include <dxf/entity/DxfEntity.h>
#include <iostream>

//静态成员初始化
map<int,DxfEntity*> EntitiesSection::entities;

EntitiesSection::EntitiesSection()
{
}

EntitiesSection::~EntitiesSection()
{
#ifdef _DEBUG
	std::cout<<"调用EntitiesSection::~EntitiesSection()"<<std::endl;
#endif
	clear();
}

void EntitiesSection::clear()
{
#ifdef _DEBUG
	std::cout<<"调用EntitiesSection::clear()"<<std::endl;
#endif

	map<int,DxfEntity*>::iterator itr;
	for(itr=entities.begin();itr!=entities.end();) {
#ifdef _DEBUG
		std::cout<<"删除类型为"<<itr->second->typeId()<<std::endl;
#endif
		delete itr->second;
		entities.erase(itr++);
	}
}

void EntitiesSection::write(DxfWriter *outFile)
{
	Section::beginSection(outFile);//实体节开始
	
	map<int,DxfEntity*>::iterator itr;
	for(itr=entities.begin();itr!=entities.end();itr++)
		itr->second->write(outFile);//实体数据

	Section::endSection(outFile);//实体节结束
}			

void EntitiesSection::read(DxfReader* inFile)
{
	int groupCode;
	string value;

	do {

		this->readEntity(inFile);

		inFile->readGroupCode(groupCode);
		inFile->readString(value);
		inFile->goBack(2);

	}while(!(groupCode==0 && value=="ENDSEC"));
	
	inFile->readGroupCode(groupCode);
	inFile->readString(value);//为下一个section做准备

}

void EntitiesSection::addEntity( DxfEntity* ent)
{
	if(ent)
		entities.insert(map<int,DxfEntity*>::value_type(ent->getEntityNum(),ent));
}

void EntitiesSection::removeEntity(int entityNum)
{
	if(entityNum<0) return ;

	map<int,DxfEntity*>::iterator itr=entities.find(entityNum);
	if(itr!=entities.end())
		entities.erase(itr);
}

//由entityNum（可以理解为一个句柄）来获得entity的指针
DxfEntity* EntitiesSection::getEntityByNum(int entityNum)
{
	if(entityNum<0) return NULL;

	map<int,DxfEntity*>::iterator itr=entities.find(entityNum);
	if(itr!=entities.end())
		return itr->second;
	else
		return NULL;
}

void EntitiesSection::readEntity(DxfReader* inFile)
{
	DxfEntity* entity=NULL;

	int groupCode=0;
	string value;

	inFile->readGroupCode(groupCode);
	inFile->readString(value);

	if(groupCode!=0) {
		std::cerr<<"第"<<inFile->getCurrentLine()<<"行与预期的数据不符"<<std::endl;
		std::cerr<<"组码应为0"<<std::endl;
		std::cerr<<"程序退出......"<<std::endl;
		exit(1);
	}

	entity=DxfEntity::createEntityByTypeName(value);
	if(entity) {
		entity->read(inFile);
		this->addEntity(entity);
	}
	else {
		std::cerr<<"不能分配足够的内存或"<<"第"<<inFile->getCurrentLine()<<"行实体类型不存在或者不支持"<<std::endl;
		std::cerr<<"程序退出......"<<std::endl;
		exit(1);
	}
}