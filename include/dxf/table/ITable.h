#pragma once

#include <string>
using std::string;

namespace dxf {

	class ILayer;
	class ILineType;

	class IReadWrite;

	class ITable
	{
		public:
			virtual ~ITable() {}
			virtual string getName() =0;
			virtual int typeId() const =0;

			//
			virtual void transform(ILayer*& p) { p=NULL; }
			virtual void transform(ILineType*& p) { p=NULL; }
			virtual void transform(IReadWrite*& p) { p=NULL; }
	};
}
