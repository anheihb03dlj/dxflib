#pragma once
#include <dxf/table/DxfTable.h>
#include <dxf/table/ILineType.h>

#include <dxf/Base.h>

#include <vector>
using std::vector;

using namespace dxf;

/** LINETYPE表(负责linetype读写). */
class DxfLineType : public DxfTable, public ILineType
{
public:

	static bool first;//判断是否第1次创建线型

	/** 预定义线型集合(static). */
	static map<string,DxfLineType*> predefineLtypes;

	/**
	 * 加载线型(static).
	 * @param[in] ltname 需要加载的线型名称
	 * @return 已加载的线型的指针
	 * @note 加载不成功，则返回NULL
	 */
	static DxfLineType* load(const string& ltname);

	/**
	 * 创建线型(static).
	 * @return DxfLineType指针
	 */
	static DxfLineType* createLineType();

	/**
	 * 创建线型(static).
	 * @param[in] ltname 线型名称
	 * @param[in] desc 线型的描述
	 * @param[in] eleCount 线型元素个数(默认为0)
	 * @param[in] totalLen 线型元素总长度(默认为0.0)
	 * @return DxfLineType指针
	 * @note 如果该线型已经存在，则返回该线型指针
	 */
	static DxfLineType* createLineType(const string& ltname,const string& desc,int eleCount=0,double totalLen=0.0);

	/**
	 * 初始化(static)
	 * @note 创建默认的线型以及预定义线型
	 */
	static void initial();

public:

	/**
	 * 从dxf文件中读入ltype的内容.
	 * @param[in] inFile DxfReader指针
	 */
	virtual void read(DxfReader* inFile);

	/**
	 * 将ltype的内容写入到dxf文件中.
	 * @param[in] outFile DxfWriter
	 */
	virtual void write(DxfWriter* outFile);

	/**
	 * 获取ltype的名称.
	 * @return ltype的名称
	 */
	virtual string getName() { return this->getLineTypeName(); }
	/**
	 * 获取ltype的类型标识.
	 * @return DxfTable::LTYPE
	 * @note 获取类型信息，避免使用c++的RTTI
	 */
	virtual int typeId() const { return LTYPE; }

public:
	
	/** 析构函数. */
	virtual ~DxfLineType(void) { }

	/**
	 * 判断是否是默认线型的一种.
	 * @return 是，则返回true;不是则返回false
	 */
	bool isDefaultLineType() { return this->ltypeName=="ByBlock" || this->ltypeName=="ByLayer" || this->ltypeName=="Continuous"; } 

	/**
	 * 设置线型的名称.
	 * @param[in] ltname 线型名称
	 */
	virtual void setLineTypeName(const string& ltname) { this->ltypeName=ltname; }

	/**
	 * 获取线型的名称.
	 * @return 线型名称
	 */
	virtual string getLineTypeName() { return this->ltypeName; } 


	/**
	 * 设置线型的描述内容.
	 * @param[in] desc 线型的描述内容
	 */
	virtual void setDescription(const string& desc) { this->description=desc; }

	/** 
	 * 获取线型的描述内容.
	 * @return 线型的描述内容
	 */
	virtual string getDescription() { return this->description; }


	//
	virtual void transform(ITable*& p) { p=this; }
	virtual void transform(ILineType*& p) { p=this; }
	virtual void transform(IReadWrite*& p) { p=this; }

protected:

	/** 构造函数(保护). */
	DxfLineType() : elementsCount(0),totalLength(0.0) {}

	/** 
	 * 构造函数(保护).
	 * @param[in] ltname 线型名称
	 * @param[in] desc 线型描述
	 * @param[in] eleCount 线型元素的个数(默认为0)
	 * @param[in] totalLen 线型元素的总长度(默认为0.0)
	 */
	DxfLineType(const string& ltname,const string& desc,int eleCount=0,double totalLen=0.0) 
		: ltypeName(ltname),description(desc),elementsCount(eleCount),totalLength(totalLen)	{}
	
private:
	string ltypeName;                        //线型名称，组码-2
	string description;                      //线型描述，组码-3
	//int flags;                             //线型标志，组码-70 (基本上所有的线型的70组的值都为0)
	int elementsCount;                       //线型元素个数，组码-73
	double totalLength;                      //线型元素总长度，组码-40
	vector<double> elements;                 //线型元素组码-49

};
