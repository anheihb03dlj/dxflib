#pragma once

#include <dxf/table/DxfTable.h>
#include <dxf/table/ILayer.h>

#include <dxf/Base.h>

using namespace dxf;

/** LAYER表(负责layer的读写). */
class DxfLayer : public DxfTable, public ILayer
{
public:
	static DxfLayer* defaultLayer;            //默认图层

	/**
	 * 创建图层(static).
	 * @param[in] name 图层名称(默认为"0")
	 * @param[in] col 图层颜色(默认为7)
	 * @param[in] ltype 图层的线型(默认为Continuous)
	 * @param[in] f 图层的标志(默认为0)
	 * @return DxfLayer指针
	 */
	static DxfLayer* createLayer(const string& name="0",int col=7,const string& ltype="Continuous",int f=0);

	/**
	 * 初始化图层(static).
	 * @note 创建默认的0层
	 */
	static void initial() { getDefaultLayer(); }

public:

	/** 析构函数. */
	virtual ~DxfLayer(void) {}

	/** 
	 * 判断是否是0层.
	 * @return 如果是0层，则返回true;不是则返回false
	 */
	bool isDefaultZeroLayer() { return this->layerName=="0"; }

	/**
	 * 设置图层名称.
	 * @param[in] name 图层名称
	 */
	virtual void setLayerName(const string& name) { this->layerName=name; }

	/** 
	 * 获取图层名称.
	 * @return 图层名称
	 */
	virtual string getLayerName() { return this->layerName; }

	/**
	 * 设置图层颜色.
	 * @param[in] color 图层颜色
	 * @see Color
	 */
	virtual void setColor(int color) { 	this->color=color; }

	/**
	 * 获取图层颜色.
	 * @return 图层颜色
	 */
	virtual int getColor() { return this->color; }

	/**
	 * 设置图层线型.
	 * @param[in] linetype 线型名称(默认为空值)
	 * @note 如果linetype为空，则将线型设置为Continuous
	 */
	virtual void setLineType(const string& linetype="");

	/**
	 * 获取图层的线型名称.
	 * @return 图层线型名称
	 */
	virtual string getLineType() { return this->linetype; }

	/**
	 * 设置图层标志.
	 * @param[in] flags 图层的标志
	 * @see Flags
	 */
	virtual void setFlags(int flags) { this->flags=flags; }

	/**
	 * 获取图层标志.
	 * @return 图层标志
	 * @see Flags
	 */
	virtual int getFlags() { return this->flags; }

public:

	/**
	 * 判断是否是当前图层.
	 * @return 如果是当前图层，返回true;否则返回false
	 */
	bool isDefaultLayer() { return this == DxfLayer::getDefaultLayer(); }

	/**
	 * 切换当前图层(static).
	 * @param[in] layerName 图层名称
	 */
	static void setDefaultLayer(const string& layerName);//设置当前层

	/**
	 * 获取当前图层(static).
	 * @return DxfLayer指针
	 */
	static DxfLayer* getDefaultLayer();

public:

	/**
	 * 获取图层的名称.
	 * @return 图层名称
	 * @note 这个函数主要用于多态的情况
	 */
	virtual string getName() { return this->getLayerName();  }

	/**
	 * 获取类型标识.
	 * @return  DxfTable::LAYER
	 * @note 用于识别当前对象的类型，避免使用c++的RTTI
	 */
	virtual int typeId() const { return LAYER; }

	/**
	 * 从dxf文件读入layer的内容.
	 * @param[in] inFile DxfReader指针
	 */
	virtual void read(DxfReader* inFile);

	/**
	 * 将layer的内容写入到dxf文件中.
	 * @param[in] outFile DxfWriter
	 */
	virtual void write(DxfWriter* outFile);

	//
	virtual void transform(ITable*& p) { p=this; }
	virtual void transform(ILayer*& p) { p=this; }
	virtual void transform(IReadWrite*& p) { p=this; }

protected:
	/** 
	 * 构造函数.
	 * @param[in] name 图层名称(默认为0)
	 * @param[in] col 图层颜色(默认为7)
	 * @param[in] ltype 图层线型(默认为Continuous)
	 * @param[in] f 图层标志(默认为0)
	 * @see Flags
	 */
	DxfLayer(const string& name="0",int col=7,const string& ltype="Continuous",int f=0);
private:
	string layerName;                         //图层名称，组码-2

	int color;                                //图层颜色，组码-62

	string linetype;                          //图层线型，组码-6

	int flags;                                //图层标志，组码-70

};
