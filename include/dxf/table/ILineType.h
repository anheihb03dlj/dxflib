#pragma once

#include <string>
using std::string;

namespace dxf {

	class ITable;

	class ILineType
	{
		public:
			virtual ~ILineType() {}
			virtual void setLineTypeName(const string& ltname)=0;
			virtual string getLineTypeName()=0;
			virtual void setDescription(const string& desc)=0;
			virtual string getDescription()=0;

			//
			virtual void transform(ITable*& p)=0;
	};
}
