#pragma once

#include <string>
using std::string;

namespace dxf {

	class ITable;

	class ILayer
	{
		public:
			virtual ~ILayer() {}
			virtual void setLayerName(const string& name)=0;
			virtual string getLayerName()=0;
			virtual void setColor(int color)=0;
			virtual int getColor()=0;
			virtual void setLineType(const string& linetype="")=0;
			virtual string getLineType()=0;
			virtual void setFlags(int flags)=0;
			virtual int getFlags()=0;

			//
			virtual void transform(ITable*& p)=0;
	};
}
