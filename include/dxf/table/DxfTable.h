#pragma once

#include <dxf/table/ITable.h>
#include <dxf/IReadWrite.h>

#include <string>
#include <map>
#include <utility>
using std::map;
using std::pair;
using std::make_pair;
using std::string;

/** 表类型名称. */
typedef map<int,pair<string,string> > TableTypeName;

#include <dxf/DxfReader.h>
#include <dxf/DxfWriter.h>

using namespace dxf;

/** 所有table的基类，抽象类. */
class DxfTable : public ITable, public IReadWrite
{
public:
	static bool first;


	/** 表类型名(包含各表所对应的名称，以及arx类名). */
	static TableTypeName tableTypeName;

	/** 析构函数. */
	virtual ~DxfTable(void);

public:
	/** 获取表的名称(纯虚函数). */
	virtual string getName() =0;

	/** 获取表类型标识(纯虚函数). */
	virtual int typeId() const =0;

	/** 
	 * 从dxf文件读入表的内容(纯虚函数).
	 * @param[in] inFile DxfReader指针
	 * @see DxfReader
	 */
	virtual void read(DxfReader* inFile);

	/**
	 * 将表的内容写入到dxf文件中(纯虚函数).
	 * @param[in] outFile DxfWriter指针
	 * @see DxfWriter
	 */
	virtual void write(DxfWriter* outFile);

protected:
	/** 构造函数(保护). */
	DxfTable(void);
};
