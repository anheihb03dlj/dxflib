#pragma once

namespace dxf {

	class ITable;

	class IDxfTable
	{
		public:
			virtual ~IDxfTable() {}
			virtual void addTable( ITable* tbl)=0;
			virtual void removeTable(ITable* tbl)=0;
	};

}
