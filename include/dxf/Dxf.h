#pragma once

#include <dxf/IDxf.h>

#include <dxf/DxfReader.h>
#include <dxf/DxfWriter.h>

#include <dxf/entity/DxfEntity.h>
#include <dxf/table/DxfLayer.h>

#include <map>
using std::map;

using namespace dxf;

/** dxf文件读写、管理类. */
class  Dxf : public IDxf
{
public:

	/** 构造函数. */
	Dxf(void);

	/** 清除内存占用. */
	void clear();

	/** 析构函数. */
	virtual ~Dxf(void);

public:
	/**
	 * 根据file产生DxfReader指针.
	 * @param[in] file 文件名
	 * @return 操作成功则产生DxfReader指针，产生异常则退出程序
	 * @note 应该保证file是存在的
	 * @see DxfReader
	 */
	virtual DxfReader* in(const string& file);

	/**
	 * 根据file产生产生DxfWriter指针.
	 * @param[in] file 文件名
	 * @return 操作成功则产生DxfWriter指针，产生异常则退出程序
	 * @note 不论文件是否存在，都会生成一份新的dxf文件
	 * @see DxfWriter
	 */
	virtual DxfWriter* out(const string& file);

public:
	/**
	 * 通过DxfWriter指针从dxf文件中读入数据到内存中.
	 * @param[in] inFile DxfReader指针
	 * @note 应该保证inFile指针不为空
	 * @see DxfReader
	 */
	virtual void read(DxfReader* inFile);

	/** 
	 * 通过DxfWriter指针将数据从内存中写入到dxf文件中.
	 * @param[in] outFile DxfWriter指针
	 * @note 应该保证outFile指针不为空
	 * @see DxfWriter
	 */
	virtual void write(DxfWriter* outFile);

public:
	/**
	 * 加载线型.
	 * @param[in] ltname 需要加载的线型名称
	 * <ul>
	 * 	  <li>ACAD_ISO02W100
	 *    <li>ACAD_ISO03W100
	 *    <li>ACAD_ISO04W100
	 *    <li>ACAD_ISO05W100
	 *    <li>BORDER
	 *    <li>BORDER2
	 *    <li>BORDERX2
	 *    <li>CENTER
	 *    <li>CENTER2
	 *    <li>CENTERX2
	 *    <li>DASHDOT
	 *    <li>DASHDOT2
	 *    <li>DASHDOTX2
	 *    <li>DASHED
	 *    <li>DASHED2
	 *    <li>DASHEDX2
	 *    <li>DIVIDE
	 *    <li>DIVIDE2
	 *    <li>DIVIDEX2
	 *    <li>DOT
	 *    <li>DOT2
	 *    <li>DOTX2
	 * </ul>
	 * @return DxfLineType指针
	 * @see DxfLineType
	 */
	virtual ILineType* loadLineType(const string& ltname); 

	/**
	 * 设置当前图层.
	 * @param[in] layerName 图层名
	 */
	virtual void setCurrentLayer(const string& layerName);  

	/**
	 * 获取当前图层.
	 * @return 当前图层指针
	 * @see DxfLayer
	 */
	virtual ILayer* getCurrentLayer();

	/**
	 * 根据图层名获取图层指针.
	 * @param[in] layerName 图层名
	 * @return DxfLayer指针
	 */
	virtual ILayer* getLayerByName(const string& layerName);

	// * 遍历所有位于图层上的实体，并返回到map容器中.
	// * @param[in] layerName 图层名
	// * @param[out] entitiesOnLayer 存储实体指针的map容器 
	// * @see DxfEntity
	// */
	//void getEntitiesOnLayer(const string& layerName,map<int,DxfEntity*>& entitiesOnLayer);

	// * 返回所有的图层.
	// * @param[out] layers 图层容器
	// */
	//void getAllLayers(map<string,DxfLayer*>& layers);*/


	/**
	 * 创建遍历在图层上的所有实体的iterator.
	 * @param[in] layerName 需要遍历的图层名
	 * @return IteratorOfEntitiesOnLayer指针
	 * @note 必须保证使用完毕之后要删除该迭代指针
	 *
	 * @section create_itr_entities Example
	 * @code
 	 * Dxf dxf;
	 * //do something.....
	 * IteratorOfEntitiesOnLayer* ptrEntity=dxf.createIterator("图层1");
	 * for(;!ptrEntity->isDone();ptrEntity->next()) {
	 *		//do something
	 * }
	 * @endcode
	 */
	virtual EntityOnLayerIterator* createEntityOnLayerIterator(const string& layerName);

	/** 
	 * 创建遍历所有图层的iterator.
	 * @return IteratorOfAllLayers指针
	 * @note 必须保证使用完毕之后要删除该迭代指针
 	 *
	 * @section create_itr_layers Example
	 * @code
	 * Dxf dxf;
	 * //do something.....
	 * IteratorOfAllLayers* ptrLayer=dxf.createIterator();
	 * for(;!ptrLayer->isDone();ptrLayer->next()) {
	 *		//do something
	 * }
	 * @endcode
	 */
	virtual AllLayersIterator* createAllLayersIterator();


public:
	/**
	 * 添加实体.
	 * @param[in] ent 待添加的实体指针
	 * @note 如果ent==NULL，则什么也不做;如果实体已经存在，则ent不会添加到已有的实体集合中
	 * @see DxfEntity
	 */
	virtual void addEntity(IEntity* ent);

	/**
	 * 删除实体.
	 * @param[in] ent 待删除的实体指针
	 * @note 如果ent==NULL,则什么也不做
	 * @see DxfEntity
	 */
	virtual void removeEntity(IEntity* ent);

	/**
	 * 添加表.
	 * @param[in] tbl 待添加的表
	 * @note 如果tbl==NULL,则什么也不做;如果该表存在，则tbl不会添加到已有talbe集合中
	 * @see DxfTable
	 */
	virtual void addTable( ITable* tbl);

	/**
	 * 删除表.
	 * @param[in] tbl 待删除的表
	 * @note 如果tbl==NULL ,则什么也不做
	 * @see DxfTalbe
	 */
	virtual void removeTable(ITable* tbl);

	/**
	 * 添加块定义.
	 * @param[in] blk 待添加的块定义
	 * @note 如果blk=NULL，则什么也不做;如果块定义重复，则blk不会添加到已有的块定义集合中
	 * @see DxfBlock
	 */
	virtual void addBlock( IBlock* blk);

	/**
	 * 删除块定义.
	 * @param[in] blk 待删除的块定义
	 * @note 如果blk=NULL，则什么也不做
	 * @see DxfBlock
	 */
	virtual void removeBlock(IBlock* blk);

public://添加实体
	
	/**
	 * 添加直线(line).
	 * @param[in] line 待添加的直线
	 * @note 如果line==NULL,则什么也不做;如果该直线已经存在，则line不会添加到已有的实体集合中
	 * @see addEntity
	 */
	virtual void addLine( ILine* line);

	/**
	 * 创建一条直线(line).
	 * @return DxfLine指针
	 * @see DxfLine
	 */
	virtual ILine* createLine();

	/**
	 * 创建一条直线(line).
	 * @param[in] startPt 直线起点坐标 
	 * @param[in] endPt 直线终点坐标
	 * @return DxfLine指针
	 * @see DxfLine DxfPoint
	 */
	virtual ILine* createLine(const DxfPoint& startPt,const DxfPoint& endPt);

	/**
	 * 创建一条直线(line).
	 * @param[in] p1x 直线起点坐标-X
	 * @param[in] p1y 直线起点坐标-Y
	 * @param[in] p1z 直线起点坐标-Z
	 * @param[in] p2x 直线终点坐标-X
	 * @param[in] p2y 直线终点坐标-Y
	 * @param[in] p2z 直线终点坐标-Z
	 * @return DxfLine指针
	 * @see DxfLine
	 */
	virtual ILine* createLine(const double p1x,const double p1y,const double p1z,const double p2x,const double p2y,const double p2z);


	/**
	 * 添加圆弧(arc).
	 * @param[in] arc 待添加的圆弧
	 * @note 如果arc==NULL,则什么也不做;如果该圆弧已存在，则arc不会添加到已有的实体集合中
	 * @see DxfArc
	 */
	virtual void addArc( IArc* arc);

	/**
	 * 创建圆弧(arc).
	 * @return DxfArc指针
	 * @see DxfArc
	 */
	virtual IArc* createArc();

	/**
	 * 创建圆弧(arc).
	 * @param[in] xCenter 圆弧的圆心坐标-X
	 * @param[in] yCenter 圆弧的圆心坐标-Y
	 * @param[in] zCenter 圆弧的圆心坐标-Z
	 * @param[in] radius 圆弧的半径
	 * @param[in] startAngle 圆弧的起始角度(单位为度,范围0~360)
	 * @param[in] endAngle 圆弧的终点角度(单位为度,范围0~360)
	 * @return DxfArc指针
	 * @see DxfArc
	 */
	virtual IArc* createArc(double xCenter,double yCenter,double zCenter,double radius,double startAngle,double endAngle);

	/**
	 * 创建圆弧(arc).
	 * @param[in] center 圆弧的圆心坐标
	 * @param[in] radius 圆弧的半径
	 * @param[in] startAngle 圆弧的起始角度(单位为度,范围0~360)
	 * @param[in] endAngle 圆弧的终点角度(单位为度,范围0~360)
	 * @return DxfArc指针
	 * @see DxfArc DxfPoint
	 */ 
	virtual IArc* createArc(const DxfPoint& center,double radius,double startAngle,double endAngle);

	/**
	 * 添加圆(circle).
	 * @param[in] circle 待添加的圆
	 * @note 如果circle==NULL，则什么也不做;如果该圆已经存在，则circle不会添加到已有的实体集合中
	 * @see DxfCircle
	 */
	virtual void addCircle( ICircle* circle);

	/**
	 * 创建圆(circle).
	 * @return DxfCircle指针
	 * @see DxfCircle
	 */
	virtual ICircle* createCircle();

	/**
	 * 创建圆(circle).
	 * @param[in] center 圆心坐标
	 * @param[in] radius 圆半径
	 * @return DxfCircle指针
	 * @see DxfCicle DxfPoint
	 */
	virtual ICircle* createCircle(const DxfPoint& center,double radius);

	/**
	 * 创建圆(circle).
	 * @param[in] x 圆心坐标-X
	 * @param[in] y 圆心坐标-Y
	 * @param[in] z 圆心坐标-Z
	 * @param[in] radius 圆半径
	 * @return DxfCircle
	 * @see DxfCircle
	 */
	virtual ICircle* createCircle(double x,double y,double z,double radius);

	/**
	 * 添加椭圆(DxfEllipse).
	 * @param[in] ellipse 待添加的椭圆
	 * @note 如果ellipse==NULL,则什么也不做;如果该椭圆已经存在，则ellipse不会添加到已有的实体集合中
	 * @see DxfEllipse
	 */
	virtual void addEllipse( IEllipse* ellipse);

	/**
	 * 创建椭圆(ellipse).
	 * @return DxfEllipse指针
	 * @note 注意ellipse对参数的要求比较严格（目前没有什么好的解决办法）
	 * @see DxfEllipse
	 */
	virtual IEllipse* createEllipse();

	/**
	 * 创建椭圆(ellipse).
	 * @param[in] center 椭圆的中心点坐标
	 * @param[in] endPoint 椭圆的长轴端点(它的坐标相对于center的差值,是一个相对值)
	 * @param[in] ratio 椭圆短轴与长轴的比例(默认值为0.6090456974522166)
	 * @param[in] startAngle 椭圆起始角度(默认值为0)
	 * @param[in] endAngle 椭圆终点角度(默认值为2*PI)
	 * @return DxfEllipse指针
	 * @see DxfEllipse DxfPoint
	 */
	virtual IEllipse* createEllipse(const DxfPoint& center,const DxfPoint& endPoint,double ratio=0.6090456974522166,double startAngle=0,double endAngle=2*PI);

	/**
	 * 插入块(insert).
	 * @param[in] insert 待插入的块引用
	 * @note 如果insert==NULL,则什么也不做;如果该块引用已经存在，则insert不会添加到已有的实体集合中
	 * @see DxfInsert
	 */
	virtual void addInsert( IInsert* insert);

	/**
	 * 创建一个块引用(insert).
	 * @return DxfInsert指针
	 * @see DxfInsert
	 */
	virtual IInsert* createInsert();

	/**
	 * 创建一个块引用(insert).
	 * @param[in] blockName 块名
	 * @param[in] insertPoint 插入点坐标
	 * @param[in] xscale x轴缩放比例(默认值为1)
	 * @param[in] yscale y轴缩放比例(默认值为1)
	 * @param[in] zscale z轴缩放比例(默认值为1)
	 * @param[in] rotationAngle 旋转角度(单位为度，默认值为0)
	 * @return DxfInsert指针
	 * @see DxfInsert DxfPoint
	 */
	virtual IInsert* createInsert(const string& blockName,const DxfPoint& insertPoint,double xscale=1,double yscale=1,double zscale=1,double rotationAngle=0);

	/**
	 * 添加单行文本(text).
	 * @param[in] text 待添加的单行文本
	 * @note 如果text==NULL,则什么也不做;如果该单行文本已存在，则text不会添加到已有的实体集合中
	 * @see DxfText
	 */
	virtual void addText( IText* text);

	/**
	 * 创建单行文本(text).
	 * @return DxfText指针
	 * @see DxfText
	 */
	virtual IText* createText();

	/**
	 * 创建单行文本(text).
	 * @param[in] insertPoint 文本插入点坐标
	 * @param[in] alignPoint 文本对齐坐标(一般不常用)
	 * @param[in] height 文字高度
	 * @param[in] xScaleFactor X比例因子
	 * @param[in] hJustification 水平对齐方式
	 * <ul>
	 * 	  <li>DxfText::Left
	 * 	  <li>DxfText::Center
	 * 	  <li>DxfText::Right
	 * 	  <li>DxfText::Aligned
	 * 	  <li>DxfText::HMiddle
	 * 	  <li>DxfText::Fit
	 * </ul>
	 * @param[in] vJustification 垂直对齐方式
	 * <ul>
	 *    <li>DxfText::BaseLine
	 *    <li>DxfText::Bottom
	 *    <li>DxfText::VMiddle
	 *    <li>DxfText::Top
	 * </ul>
	 * @param[in] styleName 文本样式名
	 * @param[in] text 文本内容
	 * @param[in] rotationAngle 旋转角度(单位为度)
	 * @return DxfText指针
	 * @see DxfText DxfPoint DxfText::HJustification DxfText::VJustification
	 */
	virtual IText* createText(const DxfPoint& insertPoint,const DxfPoint& alignPoint,double height,double xScaleFactor,int hJustification,int vJustification,const string& styleName,const string& text,double rotationAngle);


	/**
	 * 创建单行文本(text).
	 * @param[in] ix 文本插入点坐标-X
	 * @param[in] iy 文本插入点坐标-Y
	 * @param[in] iz 文本插入点坐标-Z
	 * @param[in] ax 文本对齐坐标-X(一般不常用)
	 * @param[in] ay 文本对齐坐标-Y(一般不常用)
	 * @param[in] az 文本对齐坐标-Z(一般不常用)
	 * @param[in] height 文字高度
	 * @param[in] xScaleFactor X比例因子
	 * @param[in] hJustification 水平对齐方式
	 * <ul>
	 * 	  <li>DxfText::Left
	 * 	  <li>DxfText::Center
	 * 	  <li>DxfText::Right
	 * 	  <li>DxfText::Aligned
	 * 	  <li>DxfText::HMiddle
	 * 	  <li>DxfText::Fit
	 * </ul>
	 * @param[in] vJustification 垂直对齐方式
	 * <ul>
	 *    <li>DxfText::BaseLine
	 *    <li>DxfText::Bottom
	 *    <li>DxfText::VMiddle
	 *    <li>DxfText::Top
	 * </ul>
	 * @param[in] styleName 文本样式名
	 * @param[in] text 文本内容
	 * @param[in] rotationAngle 旋转角度(单位为度)
	 * @return DxfText指针
	 * @see DxfText DxfPoint DxfText::HJustification DxfText::VJustification
	 */ 
	virtual IText* createText(double ix,double iy,double iz,double ax,double ay,double az,double height,double xScaleFactor, int hJustification,int vJustification,const string& styleName,const string& text,double rotationAngle);


	/**
	 * 添加多行文本(mtext).
	 * @param mtext 待添加的多行文本
	 * @note 如果mtext==NULL,则什么也不做;如果该多行文本已经存在，则mtext不会添加到已有的实体集合中
	 * @see DxfMText
	 */
	virtual void addMText( IMText* mtext);

	/**
	 * 创建多行文本(mtext).
	 * @return DxfMText指针
	 * @see DxfMText
	 */
	virtual IMText* createMText();

	/**
	 * 创建多行文本(mtext).
	 * @param[in] insertPoint 多行文本插入点坐标
	 * @param[in] height 文本高度
	 * @param[in] width 参考矩形宽度(一般不常用)
	 * @param[in] attachPoint 对齐方式
	 * <ul>
	 * <li>DxfMText::TopLeft
	 * <li>DxfMText::CenterTop
	 * <li>DxfMText::RightTop
	 * <li>DxfMText::LeftCenter
	 * <li>DxfMText::Center
	 * <li>DxfMText::RightCenter
	 * <li>DxfMText::LeftBottom
	 * <li>DxfMText::CenterBottom
	 * <li>DxfMText::RightBottom
	 * </ul>
	 * @param[in] drawDirection 绘制方向
	 * <ul>
	 * <li>DxfMText::LeftRight
	 * <li>DxfMText::TopBottom
	 * <li>DxfMText::Fit
	 * </ul>
	 * @param[in] text 多行文本内容(字符串内容可以有转义字符，目前只能处理 '\\n' ,'\\t')
	 * @param[in] styleName 文本样式名
	 * @param[in] rotationAngle 旋转角度(单位为度)
	 * @return DxfMText指针
	 * @see DxfMText DxfPoint DxfMText::AttachPoint DxfMText::DrawDirection
	 */
	virtual IMText* createMText(const DxfPoint& insertPoint,double height,double width,int attachPoint,int drawDirection,const string& text,const string& styleName,double rotationAngle);


	/**
	 * 创建多行文本(mtext).
	 * @param[in] ix 多行文本插入点坐标-X
	 * @param[in] iy 多行文本插入点坐标-Y
	 * @param[in] iz 多行文本插入点坐标-Z
	 * @param[in] height 文本高度
	 * @param[in] width 参考矩形宽度(一般不常用)
	 * @param[in] attachPoint 对齐方式
	 * <ul>
	 * <li>DxfMText::TopLeft
	 * <li>DxfMText::CenterTop
	 * <li>DxfMText::RightTop
	 * <li>DxfMText::LeftCenter
	 * <li>DxfMText::Center
	 * <li>DxfMText::RightCenter
	 * <li>DxfMText::LeftBottom
	 * <li>DxfMText::CenterBottom
	 * <li>DxfMText::RightBottom
	 * </ul>
	 * @param[in] drawDirection 绘制方向
	 * <ul>
	 * <li>DxfMText::LeftRight
	 * <li>DxfMText::TopBottom
	 * <li>DxfMText::Fit
	 * </ul>
	 * @param[in] text 多行文本内容(字符串内容可以有转义字符，目前只能处理 '\\n' ,'\\t')
	 * @param[in] styleName 文本样式名
	 * @param[in] rotationAngle 旋转角度(单位为度)
	 * @return DxfMText指针
	 * @see DxfMText DxfPoint  DxfMText::AttachPoint DxfMText::DrawDirection
	 */
	virtual IMText* createMText(double ix,double iy,double iz,double height,double width,int attachPoint,int drawDirection,const string& text,const string& styleName,double rotationAngle);


	/**
	 * 添加节点(vertex).
	 * @param[in] vertex 待添加的节点
	 * @note 如果vertex==NULL,则什么也不做;如果vertex已经存在，则vertex不会添加到已有的实体集合中
	 * @see DxfVertex
	 */
	virtual void addVertex( IVertex* vertex);

	/**
	 * 创建节点(vertex).
	 * @return DxfVertex指针
	 * @see DxfVertex
	 */
	virtual IVertex* createVertex();

	/**
	 * 创建节点(vertex).
	 * @param[in] x 节点的坐标-X
	 * @param[in] y 节点的坐标-Y
	 * @param[in] z 节点的坐标-Z(默认为0.0)
	 * @param[in] startWidth 起点宽度(默认为0.0)
	 * @param[in] endWidth 终点宽度(默认为0.0)
	 * @return DxfVertex指针
	 * @see DxfVertex
	 */
	virtual IVertex* createVertex(double x,double y,double z=0.0,double startWidth=0.0,double endWidth=0.0);

	/**
	 * 创建节点(vertex).
	 * @param[in] point 节点坐标
	 * @param[in] startWidth 起点宽度(默认为0.0)
	 * @param[in] endWidth 终点宽度(默认为0.0)
	 * @return DxfVertex指针
	 * @see DxfVertex DxfPoint
	 */
	virtual IVertex* createVertex(const DxfPoint& point,double startWidth=0.0,double endWidth=0.0);


	/**
	 * 添加多段线(lwPolyLine--2维多段线).
	 * @param[in] pline 待添加的多段线
	 * @note 如果pline==NULL,则什么也不做;如果该多段线已经存在，则pline不会添加到已有的实体集合中
	 * @see DxfLWPolyLine
	 */
	virtual void addPolyLine( ILWPolyLine* pline);

	/**
	 * 创建多段线(lwPolyLine--2维多段线).
	 * @return DxfLWPolyLine指针
	 * @see DxfLWPolyLine
	 */
	virtual ILWPolyLine* createPolyLine();

public://添加表（层表、线型表等）

	/**
	 * 添加图层(layer).
	 * @param[in] layer 待添加的图层
	 * @note 如果layer==NULL,则什么也不做;如果该图层已经存在，则layer不会添加到已有的图层集合中
	 * @see DxfLayer
	 */
	virtual void addLayer( ILayer* layer);

	/**
	 * 创建图层(layer).
	 * @note 如果不提供任何参数，则图层名--"图层n",颜色--7，线型--"Continuous"
	 * @return DxfLayer
	 * @see DxfLayer
	 */
	virtual ILayer* createLayer();

	/**
	 * 创建图层(layer).
	 * @param[in] name 图层名
	 * @param[in] color 图层颜色(默认为7)
	 * <ul>
	 *    <li>BLACK
	 *    <li>GREEN
	 *    <li>RED
	 *    <li>BROWN
	 *    <li>YELLOW
	 *    <li>CYAN
	 *    <li>MAGENTA
	 *    <li>GRAY
	 *    <li>BLUE
	 *    <li>WHITE
	 *    <li>BYBLOCK
	 *    <li>BYLAYER
	 * </ul>
	 * @param[in] ltype 线型名称
	 * @param[in] flag 图层标志(默认为0)
	 * @par 图层标志:
	 * <ul>
	 *    <li>FROZEN
	 *    <li>FROZEN_NEW_VIEWPORTS
	 *    <li>LOCKED
	 * </ul>
	 * @return DxfLayer
	 * @see DxfLayer DxfLayer::Flags Color
	 */
	virtual ILayer* createLayer(const string& name,int color=7,const string& ltype="Continuous",int flag=0);

	//暂时不予考虑
	//void addLineType( DxfLineType* ltype);
	//DxfLineType* createLineType();
	//DxfLineType* createLineType(const string& ltypeName,const string& description,int elementsCount=0,double totalLength=0.0);

public:
	/**
	 * 创建块定义(block)--注意区分insert与block.
	 * @return Dxfblock指针
	 * @see DxfBlock
	 */
	virtual IBlock* createBlock();

	/**
	 * 创建块定义(block).
	 * @param[in] name 块名
	 * @param[in] flag 块标志
	 * <ul>
	 *    <li>Anonymous
	 *    <li>Non_Constant_Attributes
	 *    <li>Xref
	 *    <li>Xref_Overlay
	 *    <li>External
	 *    <li>Resolved
	 *    <li>Referenced
	 * </ul>
	 * @param[in] xx 块基点坐标-X
	 * @param[in] yy 块基点坐标-Y
	 * @param[in] zz 块基点坐标-Z
	 * @return DxfBlock
	 * @see DxfBlock DxfBlock::BlockType
	 */
	virtual IBlock* createBlock(const string& name,int flag,double xx,double yy,double zz);

private:
	bool hasCleared;                               //判断是否已经调用了clear()

	void initial();                                //初始化

private:
	void writeHeader(DxfWriter* outFile);          //向dxf文件中写入HEADER SECTION

	void writeObjects(DxfWriter* outFile);         //向dxf文件中写入OBJECTS SECTION

private:
	string readBeginSection(DxfReader* inFile);    //从dxf文件中读入section开头，判断section类型

	bool readEndSection(DxfReader* inFile);        //判断是否section结束了

	void processNothing(DxfReader* inFile);        //一直循环，知道section结束
};


#include <dxf/IIterator.h>
//迭代类

/** 遍历在图层上的所有的实体. */
class IteratorOfEntitiesOnLayer : public IIterator<IEntity*>
{
public:
	IteratorOfEntitiesOnLayer(map<int,DxfEntity*>& entities,const string& layerName) : _entities(entities),_layerName(layerName) { first(); }
	virtual ~IteratorOfEntitiesOnLayer() {}

public:
	virtual void first();
	virtual void next();
	virtual bool isDone() const { if(currentItr==this->_entities.end()) return true; else return false; }
	virtual IEntity* currentItem() { return this->currentItr->second; }
	virtual int count() const { return _entities.size(); }

private:
	typedef map<int,DxfEntity*>::iterator Iterator;
	map<int,DxfEntity*> &_entities;
	Iterator currentItr;
	string _layerName;
};


/** 遍历所有的图层. */
class IteratorOfAllLayers : public IIterator<ILayer*>
{
public:
	IteratorOfAllLayers(map<string,DxfLayer*>& layers) : _layers(layers) { first(); } 
	virtual ~IteratorOfAllLayers() {}

public:
	virtual void first() { this->currentItr=this->_layers.begin(); }
	virtual void next() { if(currentItr!=this->_layers.end()) currentItr++; }
	virtual bool isDone() const { if(currentItr!=this->_layers.end()) return false; else return true; }
	virtual ILayer* currentItem() { return currentItr->second; }
	virtual int count() const { return _layers.size(); }

private:
	typedef map<string,DxfLayer*>::iterator Iterator;
	map<string,DxfLayer*> &_layers;
	Iterator currentItr;
};
