#pragma once
#include <dxf/dxfdll.h>

#include <string>
#include <fstream>
#include <vector>
using std::vector;
using std::ifstream;
using std::string;

namespace dxf {
	/** 读取dxf组码(groupCode)及组值(value). */
	class DxfReader
	{
		public:
			/** 构造函数. */
			DxfReader(void);

			/** 
			 * 构造函数.
			 * @param fileName 文件名
			 */
			DxfReader(const char* fileName);

			/** 析构函数. */
			~DxfReader(void);

		public:
			/** 
			 * 打开文件.
			 * @param fileName 文件名
			 */
			void open(const char* fileName);

			/**
			 * 判断文件是否存在.
			 * @param fileName 文件名
			 * @return 存在true，不存在false
			 */
			bool isFileExist(const char* fileName) const;

			/** 
			 * 打开文件是否失败.
			 * @return 成功true,失败false
			 */
			bool isOpenFailed() const;

			/**
			 * 判断是否到了文件结尾.
			 * @return 到达文件尾true，否则false
			 */
			bool isEndOfFile() const;

			/** 关闭文件. */
			void close();

		public:
			/** 
			 * 读取组码.
			 * @param group_code 待读入的组码
			 */
			void readGroupCode(int &group_code);

			/**
			 * 读取int.
			 * @param value int型组值
			 */
			void readInt(int &value);

			/**
			 * 读取double.
			 * @param value double型组值
			 */
			void readReal(double &value);

			/**
			 * 读取十六进制数.
			 * @param value hex型组值
			 */
			void readHex(int &value);

			/**
			 * 读取字符串.
			 * @param value string型组值
			 */
			void readString(string& value);

		public:
			/** 
			 * 获取当前文件指针所在行.
			 * @return 当前行号
			 */
			int getCurrentLine() const { return this->currentLine; }

			/** 
			 * 获取当前行的起始位置.
			 * @return 当前行的其实位置
			 */
			long getLinePos(int lineNum);

			/**
			 * 文件指针回滚.
			 * @param lineNum 行号
			 */
			void goBack(int lineNum);

		private:
			// input文件流
			ifstream inFile;

			//存储每一行的起始位置. 
			vector<long> startPos;	

			//当前行
			int currentLine;

			//存储当前行位置
			void storePos() {
				this->currentLine++;
				startPos.push_back((long)inFile.tellg()); 
			}
	};
}

