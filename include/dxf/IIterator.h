#pragma once

#include <string>
using std::string;

#include <dxf/table/ILayer.h>
#include <dxf/entity/IEntity.h>

namespace dxf {

	template<typename T>
		class IIterator
		{
			public:
				virtual ~IIterator() {}
				virtual void first()=0;
				virtual void next()=0;
				virtual bool isDone() const=0;
				virtual T currentItem()=0;
				virtual int count() const=0;
		};

	template class IIterator<ILayer*>;
	template class IIterator<IEntity*>;
	template class IIterator<string>;

	typedef IIterator<ILayer*> AllLayersIterator;
	typedef IIterator<IEntity*> EntityOnLayerIterator;
	typedef IIterator<string> XDataIterator;

}
