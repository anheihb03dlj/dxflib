#pragma once

#include <string>
using std::string;

namespace dxf {

	class IBlock;

	class IDxfBlock
	{
		public:
			virtual ~IDxfBlock() {}
			virtual void addBlock( IBlock* blk)=0;
			virtual void removeBlock(IBlock* blk)=0;
			virtual IBlock* createBlock()=0;
			virtual IBlock* createBlock(const string& name,int flag,double xx,double yy,double zz)=0;
	};

}
