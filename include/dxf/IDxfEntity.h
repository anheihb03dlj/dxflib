#pragma once

#include <dxf/Base.h>

namespace dxf {

	//typedef Vec3f DxfPoint;

	//实体接口类
	class IEntity;
	class ILine;
	class IArc;
	class ICircle;
	class IEllipse;
	class IText;
	class IMText;
	class ILWPolyLine;
	class IInsert;
	class IVertex;


	class IDxfEntity
	{
		public:
			virtual ~IDxfEntity() {}
			virtual void addEntity( IEntity* ent)=0;
			virtual void removeEntity(IEntity* ent)=0;

			//
			virtual void addLine(ILine* line)=0;
			virtual ILine* createLine()=0;
			virtual ILine* createLine(const DxfPoint& startPt,const DxfPoint& endPt)=0;
			virtual ILine* createLine(const double p1x,const double p1y,const double p1z,const double p2x,const double p2y,const double p2z)=0;

			//
			virtual void addArc(IArc* arc)=0;
			virtual IArc* createArc()=0;
			virtual IArc* createArc(double xCenter,double yCenter,double zCenter,double radius,double startAngle,double endAngle)=0;
			virtual IArc* createArc(const DxfPoint& center,double radius,double startAngle,double endAngle)=0;

			//
			virtual void addCircle(ICircle* cirlce)=0;
			virtual ICircle* createCircle()=0;
			virtual ICircle* createCircle(const DxfPoint& center,double radius)=0;
			virtual ICircle* createCircle(double x,double y,double z,double radius)=0;

			//
			virtual void addEllipse(IEllipse* ellipse)=0;
			virtual IEllipse* createEllipse()=0;
			virtual IEllipse* createEllipse(const DxfPoint& center,const DxfPoint& endPoint,double ratio=0.6090456974522166,double startAngle=0,double endAngle=2*PI)=0;

			//
			virtual void addInsert(IInsert* insert)=0;
			virtual IInsert* createInsert()=0;
			virtual IInsert* createInsert(const string& blockName,const DxfPoint& insertPoint,double xscale=1,double yscale=1,double zscale=1,double rotationAngle=0)=0;

			//
			virtual void addText(IText* text)=0;
			virtual IText* createText()=0;
			virtual IText* createText(const DxfPoint& insertPoint,const DxfPoint& alignPoint,double height,double xScaleFactor,int hJustification,int vJustification,const string& styleName,const string& text,double rotationAngle)=0;
			virtual IText* createText(double ix,double iy,double iz,double ax,double ay,double az,double height,double xScaleFactor,	int hJustification,int vJustification,const string& styleName,const string& text,double rotationAngle)=0;

			//
			virtual void addMText(IMText* mtext)=0;
			virtual IMText* createMText()=0;
			virtual IMText* createMText(const DxfPoint& insertPoint,double height,double width,int attachPoint,int drawDirection,const string& text,const string& styleName,double rotationAngle)=0;
			virtual IMText* createMText(double ix,double iy,double iz,double height,double width,int attachPoint,int drawDirection,const string& text,const string& styleName,double rotationAngle)=0;

			//
			virtual void addVertex(IVertex* vertex)=0;
			virtual IVertex* createVertex()=0;
			virtual IVertex* createVertex(double x,double y,double z=0.0,double startWidth=0.0,double endWidth=0.0)=0;
			virtual IVertex* createVertex(const DxfPoint& point,double startWidth=0.0,double endWidth=0.0)=0;

			//
			virtual void addPolyLine(ILWPolyLine* pline)=0;
			virtual ILWPolyLine* createPolyLine()=0;
	};
}

