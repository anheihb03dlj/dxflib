#pragma once

#include <string>
#include <map>
using std::map;
using std::string;

/** Block类(负责block的读写). */

#include <dxf/block/IBlock.h>
#include <dxf/IReadWrite.h>

using namespace dxf;

class DxfBlock : public IBlock, public IReadWrite
{
public:

	static bool first;                          //保证2个静态函数createblock不重复创建3个默认块

	/**
	 * 初始化(static).
	 * @note 创建3个默认的不包含任何实体的块定义
	 */
	static void initial();

	/**
	 * 创建块定义(static).
	 * @return DxfBlock指针
	 */
	static DxfBlock* createBlock();

	/**
	 * 创建块定义(static).
	 * @param[in] name 块名
	 * @param[in] flag 块标志
	 * @param[in] xx 块定义基点坐标-X
	 * @param[in] yy 块定义基点坐标-Y
	 * @param[in] zz 块定义基点坐标-Z
	 * @return DxfBlock指针
	 */
	static DxfBlock* createBlock(const string& name,int flag,double xx,double yy,double zz);

public:

	/**
	 * 从dxf文件中读入block的内容.
	 * @param[in] inFile DxfReader指针
	 */
	virtual void read(DxfReader* inFile);

	/** 
	 * 将block的内容写入到dxf文件中.
	 * @param[in] outFile DxfWriter指针
	 */
	virtual void write(DxfWriter* outFile);

public:

	/**
	 * 获取块定义中实体的个数.
	 * @return 块定义中实体的个数
	 */
	virtual int countEntities();

	/**
	 * 判断是否是默认的块定义.
	 * @return 是默认块之一，则返回true;不是则返回false
	 */
	bool isDefaultBlock();

	/**
	 * 向块定义中添加实体.
	 * @param[in] entity 待添加的实体
	 */
	virtual void add(IEntity* entity);

	/**
	 * 删除实体.
	 * @param[in] entityNum 实体的编号(类似与句柄的功能)
	 */
	virtual void remove(int entityNum);

public:
	/** 析构函数. */
	virtual ~DxfBlock(void);

	/**
	 * 设置块名.
	 * @param[in] name 块名
	 */
	virtual void setBlockName(const string& name="");

	/** 
	 * 获取块名.
	 * @return 块名
	 */
	virtual string getBlockName() { return this->blockName; }

	/**
	 * 设置块标志.
	 * @param[in] flag 块标志
	 */
	virtual void setFlag(int flag) { this->flags=flag; }

	/**
	 * 获取块标志.
	 * @return 块标志
	 */
	virtual int getFlag() { return this->flags; }

	/**
	 * 设置块定义的基点.
	 * @param[in] x 块定义基点坐标-X
	 * @param[in] y 块定义基点坐标-Y
	 * @param[in] z 块定义基点坐标-Z
	 */
	virtual void setBasePoint(double x,double y,double z=0.0) {	this->x=x; this->y=y; this->z=z; }


	//
	virtual void transform(IReadWrite*& p) { p=this; }

protected:
	/** 构造函数(保护). */
	DxfBlock(void) { this->x=0;	this->y=0;	this->z=0;	this->flags=0; }

	/** 
	 * 构造函数(保护).
	 * @param[in] name 块名
	 * @param[in] flag 块标志
	 * @param[in] xx 块定义基点坐标-X
	 * @param[in] yy 块定义基点坐标-Y
	 * @param[in] zz 块定义基点坐标-Z
	 */
	DxfBlock(const string& name,int flag,double xx,double yy,double zz) : blockName(name),flags(flag),x(xx),y(yy),z(zz) {}

private:
	string blockName;                           //块名，组码-2
	int flags;                                  //块标志，组码-70
	double x;                                   //基点坐标-x，组码-10
	double y;                                   //基点坐标-y，组码-20
	double z;                                   //基点坐标-z，组码-30
	
	map<int,IEntity*> entities;
	
};
