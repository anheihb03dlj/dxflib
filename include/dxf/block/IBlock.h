#pragma once

#include <string>
using std::string;

namespace dxf {

	class IEntity;
	class IReadWrite;

	class IBlock
	{
		public:
			virtual ~IBlock() {}
			virtual int countEntities()=0;
			virtual void add(IEntity* entity)=0;
			virtual void remove(int entityNum)=0;
			virtual void setBlockName(const string& name="")=0;
			virtual string getBlockName()=0;
			virtual void setFlag(int flag)=0;
			virtual int getFlag()=0;
			virtual void setBasePoint(double x,double y,double z=0.0)=0;

			//
			virtual void transform(IReadWrite*& p)=0;
	};
}
