#pragma once

#include <string>
using std::string;

namespace dxf {

	class ILineType;

	class IDxfLineType
	{
		public:
			virtual ~IDxfLineType() {}
			virtual ILineType* loadLineType(const string& ltname)=0;
	};
}
