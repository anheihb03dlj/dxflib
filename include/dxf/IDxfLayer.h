#pragma once

#include <string>
using std::string;

namespace dxf {

	class ILayer;

	class IDxfLayer
	{
		public:
			virtual ~IDxfLayer() {}
			virtual void setCurrentLayer(const string& layerName)=0;
			virtual ILayer* getCurrentLayer()=0;
			virtual ILayer* getLayerByName(const string& layerName)=0;

			virtual ILayer* createLayer(const string& name,int color=7,const string& ltype="Continuous",int flag=0)=0;
			virtual ILayer* createLayer()=0;

			virtual void addLayer( ILayer* layer)=0;
	};
}
