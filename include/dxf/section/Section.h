#pragma once

#include <dxf/Base.h>
#include <dxf/DxfReader.h>
#include <dxf/DxfWriter.h>
#include <string>
using std::string;

//class HeaderSection;
//class ClassesSection;
class TablesSection;
class BlocksSection;
class EntitiesSection;
//class ObjectsSection;

using namespace dxf;

/** 所有secion的基类，这是一个抽象类. */
class Section
{
public:
	/**
	 * 创建子类section的实例(采用了简单工厂模式).
	 * @param[in] sectionName 子类section的名称
	 * <ul>
	 *    <li>TABLES
	 *    <li>BLOCKS
	 *    <li>ENTITIES
	 * </ul>
	 * @note 该函数并不返回任何子类section的实例指针
	 */
	static void createSection(const string& sectionName);

	/** 析构函数. */
	virtual ~Section(void);

	/** 清空子类section所控制的容器(纯虚函数). */
	virtual void clear()=0;

public:
	/** 获得section的名称(纯虚函数). */
	virtual string getSectionName() const =0;

	/**
	 * 从dxf文件中读取section内容到内存中(纯虚函数). 
	 * @param[in] inFile DxfReader指针
	 * @see DxfReader
	 */
	virtual void read(DxfReader* inFile)=0;

	/** 
	 * 向dxf文件中写入section的内容(纯虚函数).
	 * @param[in] outFile DxfWriter指针
	 * @see DxfWriter
	 */
	virtual void write(DxfWriter* outFile)=0;

public:
	/**
	 * 开始写入section.
	 * @param[in] outFile DxfWriter指针
	 * @see DxfWriter
	 */
	void beginSection(DxfWriter* outFile);

	/**
	 * 结束section的写入.
	 * @param[in] outFile DxfWriter指针
	 * @see DxfWriter
	 */
	void endSection(DxfWriter* outFile);

protected:
	/** 构造函数(保护). */
	Section();

public:
	//暂不予考虑
	//static HeaderSection* headerSection;

	//暂不予考虑
	//static ClassesSection* classesSection;

	/** 
	 * TablesSection指针(static).
	 * @note 整个系统有且仅有一个该类型指针
	 * @see TablesSection
	 */
	static TablesSection* tablesSection;

	/**
	 * BlocksSection指针(static).
	 * @note 整个系统有且仅有一个该类型指针
	 * @see BlocksSection
	 */
	static BlocksSection* blocksSection;

	/**
	 * EntitiesSection指针(static).
	 * @note 整个系统有且仅有一个该类型指针
	 * @see EntitiesSection
	 */
	static EntitiesSection* entitiesSection;

	//暂不予考虑
	//static ObjectsSection* objectsSection;
};
