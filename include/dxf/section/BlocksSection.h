#pragma once
#include <dxf/section/Section.h>
#include <dxf/block/DxfBlock.h>

#include <map>
#include <string>
using std::string;
using std::map;

using namespace dxf;

/** BLOCKS节(负责所有的block). */
class BlocksSection : public Section
{
public:
	/** 构造函数. */
	BlocksSection(void);

	/** 析构函数. */
	virtual ~BlocksSection(void);

	/** 清空BlocksSection所控制的所有block. */
	virtual void clear();

public:

	/**
	 * 从dxf文件读入BLOCKS节的内容.
	 * @param[in] inFile DxfReader指针
	 * @see DxfReader
	 */
	void read(DxfReader* inFile);

	/**
	 * 将BLOCKS节的内容写入dxf文件.
	 * @param[in] outFile DxfWriter
	 * @see DxfWriter
	 */
	void write(DxfWriter* outFile);

	/**
	 * 获取section的名称(系统内部使用).
	 * @return 永远都是"BLOCKS"
	 */
	string getSectionName() const { return "BLOCKS"; }

public:

	/** 
	 * 添加块定义.
	 * @param[in] block 待添加的块定义
	 * @note 如果该块定义已存在，则block不会添加到已有的块定义集合中
	 * @see DxfBlock
	 */
	void addBlock( DxfBlock* block);

	/**
	 * 删除块定义.
	 * @param[in] blockName 块名
	 * @note 如果该块存在，则在删除块定义的同时，也会删除相应的块引用(insert)
	 */
	void removeBlock(const string& blockName);

	/**
	 * 获得块定义指针.
	 * @param[in] blockName 块名
	 * @note 如果块不存在，则返回NULL
	 */ 
	DxfBlock* getBlock(const string& blockName);

	/**
	 * 返回已有块定义的个数.
	 * @return 块定义个数
	 */
	int countBlocks() { return blocks.size(); }

public:
	/** 包含所有block的容器(static). */
	static map<string,DxfBlock*> blocks;

private:
	void beginBlock(DxfWriter* outFile,string blockName);                //开始写入block

	void endBlock(DxfWriter* outFile,string blockName);                  //结束block

	void readBlock(DxfReader* inFile);                                   //从dxf文件读入block
};
