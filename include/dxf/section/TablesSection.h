#pragma once
#include <dxf/section/Section.h>

#include <string>
#include <map>
#include <list>
using std::list;
using std::map;
using std::string;

using namespace dxf;

class DxfTable;
class DxfLayer;
class DxfLineType;
//后续添加其他的表
//如ucs、style、dimstyle、view、vport等


/** TABLES节(负责所有的读写信息) */
class TablesSection : public Section
{
public:
	/** 构造函数. */
	TablesSection();

	/** 析构函数. */
	virtual ~TablesSection();

	/** 清空TablesSection所控制的容器. */
	virtual void clear();

public:

	/** 
	 * 从dxf中读入TablesSection的内容.
	 * @param[in] inFile DxfReader指针
	 * @see DxfReader
	 */
	void read(DxfReader* inFile);

	/**
	 * 向dxf文件写入TablesSection的内容.
	 * @param[in] outFile DxfWriter指针
	 * @see DxfWriter
	 */
	void write(DxfWriter* outFile);

public:

	/**
	 * 添加表.
	 * @param[in] table DxfTable指针
	 * @note 如果table==NULL ,则什么也不做;如果该talbe已经存在，则table不会添加到已有的table集合中
	 * @see DxfTalbe
	 */
	void addTable(DxfTable* table);

	/**
	 * 删除图层.
	 * @param[in] layerName 待删除的图层名称
	 * @note 如果该图层不存在，则什么也不会做;如果是当前的图层，则不会删除图层
	 */
	void removeLayer(const string& layerName);

	/**
	 * 根据图层名称获取图层的指针.
	 * @param[in] layerName 图层名称
	 * @note 如果该图层不存在，则返回NULL
	 */
	DxfLayer* getLayer(const string& layerName);

	/**
	 * 根据线型名称获取线型的指针.
	 * @param[in] ltypeName 线型名称
	 * @note 如果线型不存在，则返回NULL
	 */
	DxfLineType* getLineType(const string& ltypeName);

	/**
	 * 获取section的名称(系统内部使用).
	 * @return 永远都是"TABLES"
	 */
	string getSectionName() const { return "TABLES"; }

public:

	/** 所有图层的集合. */
	static map<string,DxfLayer*> layers;

	/** 所有线型的集合. */
	static map<string,DxfLineType*> ltypes;

	static list<string> appNames;

	//还有其他的表，如vport、view、style等

private:
	void beginTable(DxfWriter* outFile,const string& tableName);                    //向dxf文件写入talbe的开始部分(所有table通用的部分)

	void endTable(DxfWriter* outFile);                                              //表结束

	string readBeginTable(DxfReader* inFile);                                       //从dxf文件中读入table的开始部分，判断表的类型

	void readBeginTableEntry(DxfReader* inFile,const string& tableName);            //读入子表项开始部分，判断表项类型

	void readTableEntry(DxfReader* inFile/*,const string& tableName*/);             //读入子表项

	void processNothing(DxfReader* inFile);                                         //什么也不做，直到表结束

private:
	void writeVPort(DxfWriter* outFile);                                             //向dxf文件写入vport表

	void writeStyle(DxfWriter* outFile);                                             //向dxf文件写入style表

	void writeView(DxfWriter* outFile);                                              //向dxf文件写入view表

	void writeUcs(DxfWriter* outFile);                                               //向dxf文件写入ucs表

	void writeAppid(DxfWriter* outFile);                                             //向dxf文件写入appid表

	void writeDimStyle(DxfWriter* outFile);                                          //向dxf文件写入dimstyle表

	void writeBlockRecord(DxfWriter* outFile);                                       //向dxf文件写入block_record表

	void beginBlockRecord(DxfWriter* outFile);                                       //开始写块表记录(由于函数比较繁杂，所以拆分成较小的函数)

	void writeDefaultRecord(DxfWriter* outFile);                                     //写入3个默认的块表记录(model_space,paper_space,paper_space0)
	
	void endBlockRecord(DxfWriter* outFile);                                         //块表记录结束
};
