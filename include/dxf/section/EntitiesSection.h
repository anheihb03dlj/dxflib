#pragma once
#include <dxf/section/Section.h>

#include <string>
#include <map>
using std::map;
using std::string;

using namespace dxf;

class DxfEntity;

/** ENTITIES节(负责entity的读写、管理). */
class EntitiesSection:public Section
{
public:
	/** 构造函数. */
	EntitiesSection(void);

	/** 析构函数. */
	virtual ~EntitiesSection(void);

	/** 清空包含所有entity的容器. */
	virtual void clear();

public:

	/** 
	 * 从dxf读入ENTITIES节的内容.
	 * @param[in] inFile DxfReader指针
	 * @see DxfReader
	 */
	void read(DxfReader* inFile);

	/**
	 * 将ENTITIES节的内容写入到dxf文件中.
	 * @param[in] outFile DxfWriter
	 * @see DxfWriter
	 */
	void write(DxfWriter* outFile);

	/**
	 * 获取section的名称(系统内部使用).
	 * @return 永远都是"ENTITIES"
	 */
	string getSectionName() const  { return "ENTITIES"; }

public:
	/**
	 * 添加实体.
	 * @param[in] ent 待添加的实体
	 * @note 如果ent==NULL ,则什么也不做;如果该实体已经存在，则ent不会添加到已有的实体结合中
	 * @see DxfEntity
	 */
	void addEntity( DxfEntity* ent);

	/**
	 * 删除实体.
	 * @param[in] entityNum 实体编号(作用类似于句柄，可以通过该num获得实体的指针)
	 * @note 如果entityNum不是一个有效值，则什么也不做
	 */
	void removeEntity(int entityNum);

	/**
	 * 通过entityNum获得实体的指针.
	 * @param[in] entityNum 实体编号(也可以称之为句柄)
	 * @return DxfEntity指针
	 * @note  如果entityNum不是一个有效值，则返回NULL
	 */
	DxfEntity* getEntityByNum(int entityNum);

public:
	/** 包含所有实体的容器(map). */
	static map<int,DxfEntity*> entities;

private:
	void readEntity(DxfReader* inFile);             //从dxf文件中读入实体

};
