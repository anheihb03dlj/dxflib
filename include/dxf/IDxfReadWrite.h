#pragma once

//��д�ӿ���
#include <dxf/IReadWrite.h>

#include <string>
using std::string;

namespace dxf {

	//��д��
	class DxfReader;
	class DxfWriter;

	class IDxfReadWrite : public IReadWrite
	{
		public:
			virtual ~IDxfReadWrite() {}

			virtual DxfReader* in(const string& file)=0;
			virtual DxfWriter* out(const string& file)=0;
			virtual void read(DxfReader* inFile)=0;
			virtual void write(DxfWriter* outFile)=0;
	};
}
