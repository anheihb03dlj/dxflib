#pragma once

#include <dxf/IIterator.h>

namespace dxf {

	class IDxfIterator
	{
		public:
			virtual ~IDxfIterator() {}
			virtual EntityOnLayerIterator* createEntityOnLayerIterator(const string& layerName)=0;
			virtual AllLayersIterator* createAllLayersIterator()=0;
	};
}
