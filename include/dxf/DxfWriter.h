#pragma once
#include <dxf/dxfdll.h>

#include <fstream>
#include <vector>
#include <string>
using std::string;
using std::vector;
using std::fstream;

namespace dxf {

	/** 写dxf组码(groupCode)及组值(value). */
	class DxfWriter
	{
		public:

			//内部使用
			static bool first;

			/** 构造函数. */
			DxfWriter(void);

			/** 
			 * 构造函数.
			 * @param fileName 文件名
			 */
			DxfWriter(const char* fileName);

			/** 析构函数. */
			~DxfWriter(void);

		public:
			/** 
			 * 打开文件.
			 * @param fileName 文件名
			 */
			void open(const char* fileName);

			/** 
			 * 判断文件是否存在.
			 * @param fileName 文件名
			 * @return 文件存在true,不存在false
			 */
			bool isFileExist(const char* fileName) const;

			/**
			 * 判断文件打开是否失败.
			 * @return 打开成功true，失败false
			 */
			bool isOpenFailed() const;

			/** 关闭文件. */
			void close() ;

		public:
			/**
			 * 生成一个unique句柄.
			 * @return unique句柄
			 */
			int handle();

			/**
			 * 查找句柄.
			 * @param _handle 句柄
			 * @return 存在true,不存在false
			 */
			bool findHandle(int _handle);

			/**
			 * 注册句柄.
			 * @param _handle 句柄
			 */
			void registerHandle(int _handle);

			/** 重置句柄为0. */
			void resetHandle();

		public:
			/**
			 * 向dxf写入组码及double组值.
			 * @param group_code 组码
			 * @param value double型组值
			 */
			void writeReal(int group_code, double value) ;

			/**
			 * 向dxf写入组码及int组值.
			 * @param group_code 组码
			 * @param value int型组值
			 */
			void writeInt(int group_code, int value) ;

			/**
			 * 向dxf写入组码及hex组值.
			 * @param group_code 组码
			 * @param value hex组值
			 */
			void writeHex(int group_code, int value) ;

			/**
			 * 向dxf写入组码及string组值.
			 * @param group_code 组码
			 * @param value string组值
			 */
			void writeString(int group_code, const string& value) ;

		private:
			//句柄(dxf2000文件需要)
			int m_handle;

			//output文件流
			fstream outFile;

			//已使用的句柄或者固定句柄
			vector<int> hasRegisterHandle;

	};

}
