#ifndef BASE_H
#define BASE_H

#include <dxf/dxfdll.h>

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <algorithm>
#include <functional>
#include <cctype>
#include <sstream>

#include <string>
using std::string;

namespace dxf {

#define PI 3.1415926535897932384626433832795

	/** color枚举常量. */
	DXF_DLL_API enum Color {
		BLACK = 250,	/**< 黑色. */
			  GREEN = 3,  	/**< 绿色. */
			  RED = 1,    	/**< 红色. */
			  BROWN = 15, 	/**< 棕色. */
			  YELLOW = 2, 	/**< 黄色. */
			  CYAN = 4,   	/**< 蓝绿色. */
			  MAGENTA = 6,	/**< 洋红色. */
			  GRAY = 8,   	/**< 灰色. */
			  BLUE = 5,   	/**< 蓝色. */
			  WHITE = 7,  	/**< 白色. */
			  BYLAYER = 256,  /**< 随层. */
			  BYBLOCK = 0     /**< 随块. */
	};


	/** BlockType枚举. */
	DXF_DLL_API enum BlockType {
		Anonymous=1,                            /**< 匿名块. */
			Non_Constant_Attributes=2,              /**< 非固定属性块. */
			Xref=4,                                 /**< 外部引用. */
			Xref_Overlay=8,                         /**< 外部引用包. */
			External=16,                            /**< 外部从属块. */
			Resolved=32,                            /**< 还原的外部引用. */
			Referenced=64                           /**< 该块被引用. */
	};


	/** TableType枚举(表示表的类型). */
	DXF_DLL_API enum TableType {
		LAYER=1,        /**< 图层表. */
			LTYPE=2,        /**< 线型表. */
			STYLE=3,        /**< 文字样式表. */
			DIMSTYLE=4,     /**< 标注样式表. */
			APPID=5,        /**< 注册应用程序表记录. */
			UCS=6,          /**< 用户坐标系表. */
			VIEW=7,         /**< 视图表. */
			VPORT=8,        /**< 视口表. */
			BLOCK_RECORD=9  /**< 块记录表. */
	};


	/** Flags枚举. */
	DXF_DLL_API enum Flags {
		FROZEN               = 0x1,     /**< 冻结. */
							 FROZEN_NEW_VIEWPORTS = 0x2,     /**< 冻结新的视口. */
							 LOCKED               = 0x4      /**< 锁定. */  
	};


	/** EntityType枚举. */
	DXF_DLL_API enum EntityType {
		LINE=1,               /**< 直线. */
			ARC=2,                /**< 圆弧. */
			CIRCLE=3,             /**< 圆. */
			ELLIPSE=4,            /**< 椭圆. */
			INSERT=5,             /**< 块引用. */
			TEXT=6,               /**< 单行文本. */
			MTEXT=7,              /**< 多行文本. */
			LWPOLYLINE=8,         /**< 多段线. */
			VERTEX=9              /**< 节点. */

				//还有更多
	};



	/** 多段线标志. */
	DXF_DLL_API enum LWPolyLineFlag {
		NORMAL=0,          /**< 不闭合. */
			CLOSED=1           /**< 闭合. */
	};


	struct DXF_DLL_API MTextType {
		/** 对齐方式. */
		enum AttachPoint {
			TopLeft=1,                    /**< 左上. */
			CenterTop=2,                  /**< 中上. */
			RightTop=3,                   /**< 右上. */
			LeftCenter=4,                 /**< 左中. */
			Center=5,                     /**< 中心. */
			RightCenter=6,                /**< 右中. */
			LeftBottom=7,                 /**< 左下. */
			CenterBottom=8,               /**< 中下. */
			RightBottom=9                 /**< 右下. */
		};

		/** 绘制方向. */
		enum DrawDirection {
			LeftRight=1,                   /**< 从左至右. */
			TopBottom=3,                   /**< 从上至下. */
			Fit=5                          /**< 自动调整. */
		};

	};





	struct DXF_DLL_API TextType {
		/** 水平对齐方式. */
		enum HJustification {
			Left=0,                /**< 左对齐. */
			Center=1,              /**< 中心对齐. */
			Right=2,               /**< 右对齐. */
			Aligned=3,             /**< 对齐. */
			HMiddle=4,             /**< 中间对齐. */
			Fit=5                  /**< 调整. */
		};


		/** 垂直对齐方式. */
		enum VJustification {
			BaseLine=0,            /**< 基准线对齐. */
			Bottom=1,              /**< 下对齐. */
			VMiddle=2,             /**< 中间对齐. */
			Top=3                  /**< 上对齐. */
		};
	};

	/** string扩展类. */
	class StringExt
	{
		public:
			/** 
			 * 字符串大写转换，不改变原有字符串.
			 * @param[in] s 需要转换大写的字符串(c-style或者string均可)
			 * @return 大写的字符串
			 */
			static string to_upper_copy(const string& s);

			/**
			 * 字符串小写转换，不改变原有字符串.
			 * @param[in] s 需要转换小写的字符串(c-style或者string均可)
			 * @return 小写的字符串
			 */
			static string to_lower_copy(const string& s);

			/**
			 * 在字符串两端去掉指定内容的字符.
			 * @param[in] s 待操作的字符串
			 * @param[in] drop 指定删除的字符串
			 * @return 去除了指定内容的字符串
			 */
			static string& trim(string& s,const string& drop=" \t");

			/**
			 * 在字符串左端去掉指定内容的字符.
			 * @param[in] ss 待操作的字符串
			 * @param[in] drop 指定删除的字符串
			 * @return 去除了指定内容的字符串
			 */
			static string& trim_left(string& ss,const string& drop=" \t");

			/**
			 * 在字符串右端去掉指定内容的字符.
			 * @param[in] ss 待操作的字符串
			 * @param[in] drop 指定删除的字符串
			 * @return 去除了指定内容的字符串
			 */
			static string& trim_right(string& ss,const string& drop=" \t");

			/**
			 * 替换字符串中的子串.
			 * @param[in] ss 待操作的字符串
			 * @param[in] substrSrc 源子串
			 * @param[in] substrDec 替换子串
			 * @return 替换了子串后的字符串
			 */
			static string& replace_all(string& ss,const string& substrSrc,const string& substrDec);

			/**
			 * 将字符串转化成整型.
			 * @param[in] s 待转换的字符串
			 * @return 整型
			 */
			static int toInt(const string& s);

			/**
			 * 将字符串转化成浮点型.
			 * @param[in] s 待转换的字符串
			 * @return 浮点数
			 */
			static double toReal(const string& s);
	};


	inline string StringExt::to_upper_copy(const string& s)
	{
		string ss=s;
		std::transform(ss.begin(),ss.end(),ss.begin(),toupper);
		return ss;
	}

	inline string StringExt::to_lower_copy(const string& s)
	{
		string ss=s;
		std::transform(ss.begin(),ss.end(),ss.begin(),tolower);
		return ss;
	}

	inline string& StringExt::trim(string& s, const string& drop)//去掉左右两端空白字符(包括空格、制表符),也可以自己添加其他的符号
	{
		// trim right
		s.erase(s.find_last_not_of(drop)+1);
		// trim left
		return s.erase(0,s.find_first_not_of(drop));
	}

	inline string&  StringExt::trim_left(string& ss,const string& drop)//删除左边的空白
	{   
		/*string::iterator p=std::find_if(ss.begin(),ss.end(),std::not1(std::ptr_fun(isspace)));   
		  ss.erase(ss.begin(),p);   
		  return ss;*/   

		return ss.erase(0,ss.find_first_not_of(drop));
	}

	inline  string&  StringExt::trim_right(string &ss,const string& drop)//删除右边的空白
	{   
		/*string::reverse_iterator  p=std::find_if(ss.rbegin(),ss.rend(),std::not1(std::ptr_fun(isspace)));   
		  ss.erase(p.base(),ss.end());   
		  return ss; */  

		return ss.erase(ss.find_last_not_of(drop)+1);
	}

	inline string& StringExt::replace_all(string& ss,const string& substrSrc,const string& substrDec)//替换字符串
	{
		/* ss.replace(ss.find(substrSrc), substrSrc.size(), substrDec);*/

		int lastpos = 0, thispos;

		while ((thispos = ss.find(substrSrc, lastpos)) != string::npos)
		{
			ss.replace(thispos, substrSrc.length(), substrDec);
			lastpos = thispos + 1;
		}
		return ss;
	}

	inline int StringExt::toInt(const string& s)
	{
		int ii;
		std::stringstream ss(s);
		ss>>ii;
		ss.clear();

		return ii;
	}

	inline double StringExt::toReal(const string& s)
	{
		double dd;
		std::stringstream ss(s);
		ss>>dd;
		ss.clear();
		return dd;
	}

	/** 3维向量类，用于点坐标. */
	class DXF_DLL_API Vec3f
	{ 
		public:
			/** X坐标. */
			double x;
			/** Y坐标. */
			double y;
			/** Z坐标. */
			double z;

			/** 默认构造函数. */
			Vec3f(void) {};

			/**
			 * 构造函数.
			 * @param[in] X x坐标
			 * @param[in] Y y坐标
			 * @param[in] Z z坐标
			 */
			Vec3f(const double X, const double Y, const double Z) 
			{ x=X; y=Y; z=Z; };

			/**
			 * 构造函数.
			 * @param[in] xyz 浮点型的3维坐标数组
			 */
			Vec3f(const double *xyz)
			{ x = xyz[0]; y = xyz[1]; z = xyz[2]; }

			/**
			 * 拷贝构造函数.
			 * @param[in] v 源坐标
			 */
			Vec3f (const Vec3f& v) 
			{ x=v.x; y=v.y; z=v.z; };


			Vec3f cross(const Vec3f &v) const
			{ return Vec3f(y*v.z-z*v.y, z*v.x-x*v.z, x*v.y-y*v.x); }

			double dot(const Vec3f &v) const
			{ return x*v.x+y*v.y+z*v.z; }

			/**
			 * 判断向量(坐标)是否相等
			 * @param[in] v 向量(坐标)
			 * @return 相等true，或者不等false
			 */
			bool equals(const Vec3f &v)
			{ return (x == v.x && y == v.y && z == v.z); }
			bool equals(const Vec3f &v, double tol)
			{ return (fabs(x-v.x) <= tol && fabs(y-v.y) <= tol && fabs(z-v.z) <= tol); }

			operator double *() { return &x; } 
			const double *getValue() const { return &x; }
			void getValue(double &_x, double &_y, double &_z) const
			{ _x = x; _y = y; _z = z;}
			double length() const                    
			{ return (double) sqrt(x*x+y*y+z*z); }
			double sqrLength(void) const
			{ return x*x+y*y+z*z; }
			void negate(void)
			{ x = -x; y = -y; z = -z; }
			void setValue(const double *v) 
			{ x = v[0]; y = v[1]; z = v[2]; } 
			void setValue(const double X, const double Y, const double Z)
			{ x=X; y=Y; z=Z; }

			double operator [] (const int i) const
			{ return( (i==0)?x:((i==1)?y:z) ); };
			double & operator [] (const int i)             
			{ return( (i==0)?x:((i==1)?y:z) ); };

			Vec3f &operator *= (const double s)
			{ x*=s; y*=s; z*=s; return *this; }
			Vec3f &operator /= (const double s)
			{ x/=s; y/=s; z/=s; return *this; }
			Vec3f &operator += (const Vec3f &v) 
			{ x+=v.x; y+=v.y; z+=v.z; return *this; }
			Vec3f &operator -= (const Vec3f &v)
			{ x-=v.x; y-=v.y; z-=v.z; return *this;}
			Vec3f operator -() const
			{ return Vec3f(-x, -y, -z); }
			friend Vec3f operator *(const Vec3f &v, double s)
			{ return Vec3f(v.x*s, v.y*s, v.z*s); }
			friend Vec3f operator *(double s, const Vec3f &v)
			{ return Vec3f(v.x*s, v.y*s, v.z*s); }
			friend Vec3f operator / (const Vec3f &v, double s)
			{ return Vec3f(v.x/s, v.y/s, v.z/s); }
			friend Vec3f operator + (const Vec3f &v1, const Vec3f &v2)
			{ return Vec3f(v1.x+v2.x, v1.y+v2.y, v1.z+v2.z); } 
			friend Vec3f operator - (const Vec3f &v1, const Vec3f &v2)
			{ return Vec3f(v1.x-v2.x, v1.y-v2.y, v1.z-v2.z); }

			friend bool operator ==(const Vec3f &v1, const Vec3f &v2)
			{ return (v1.x == v2.x && v1.y == v2.y && v1.z == v2.z); }
			friend bool operator !=(const Vec3f &v1, const Vec3f &v2)
			{ return (v1.x != v2.x || v1.y != v2.y || v1.z != v2.z); }

			Vec3f& operator = (const Vec3f &v)        // extra
			{ x=v.x; y=v.y; z=v.z; return *this; } 

			void multMatrix(double *matrix)       // extra
			{
				double tx, ty, tz;
				tx = ty = tz = 0.0f;
				tx = matrix[0]*x+matrix[1]*y+matrix[2]*z;
				ty = matrix[4]*x+matrix[5]*y+matrix[6]*z;
				tz = matrix[8]*x+matrix[9]*y+matrix[10]*z;
				x = tx, y = ty, z = tz;
			} 


			Vec3f multComponents(const Vec3f &v) const
			{	return Vec3f(x*v.x, y*v.y, z*v.z); }

			/* double angle(const Vec3f &v2);
			   void normalize();*/

	}; // class Vec3f	
}

typedef dxf::Vec3f DxfPoint;

#endif
