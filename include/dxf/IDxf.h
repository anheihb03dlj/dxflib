#pragma once

#include <dxf/IDxfBlock.h>
#include <dxf/IDxfEntity.h>
#include <dxf/IDxfIterator.h>
#include <dxf/IDxfLayer.h>
#include <dxf/IDxfLineType.h>
#include <dxf/IDxfReadWrite.h>

#include <dxf/block/IBlock.h>

#include <dxf/entity/IEntity.h>
#include <dxf/entity/IXData.h>

#include <dxf/table/ILayer.h>
#include <dxf/table/ILineType.h>
#include <dxf/table/ITable.h>


namespace dxf {

	class IDxf: public IDxfBlock,
	public IDxfEntity,
	public IDxfLayer,
	public IDxfLineType,
	public IDxfIterator,
	public IDxfReadWrite
	{
		public:
			virtual ~IDxf() {}
	};


	extern DXF_DLL_API IDxf* createDxf();


	//IDxf接口包含以下功能：
	//    1、与block相关的功能（创建block、添加block等等）--对应接口IDxfBlock
	//    2、与entity相关的功能（创建entity、添加entity等等）--对应接口IDxfEntity
	//    3、与Layer相关的功能（创建layer、添加layer等等）--对应接口IDxfLayer
	//    4、与linetype相关的功能（加载线型）--对应接口IDxfLineType
	//    5、与iterator相关的功能（迭代所有的layer、迭代在某个layer上的所有entity）--对应接口IDxfIterator
	//    6、与ReadWrite相关的功能（读写dxf功能）--对应接口IDxfReadWrite


	//另外IDxf.h文件也包含了entity、block、layer、linetype的属性功能接口
	//#include <dxf/entity/IEntity.h>        --entity属性功能接口(提供设置entity的基本属性的功能，如颜色、图层、线型等)
	//#include <dxf/entity/IXData.h>         --xdata属性功能接口(提供了xdata的读写功能以及迭代xdata的功能)

	//#include <dxf/table/ILayer.h>          --layer的属性功能接口(提供了设置layer基本属性的功能，如名称、线型、颜色等)
	//#include <dxf/table/ILineType.h>       --linetype的属性功能接口(提供了设置linetype的基本功能属性的功能，该接口一般不需要使用)
	//#include <dxf/table/ITable.h>          --table的属性功能接口（该接口也不需要使用，基本上只作为内部使用，但是出于某些原因，不得已暴露出来）

	//#include <dxf/block/IBlock.h>          --block的属性功能接口(提供了设置block属性的功能，如block名称、添加实体、删除实体、设置插入点等)


	//当用户需要其他的功能时，包含相应的接口头文件即可(接口文件都是以“I”打头的)
	//如需要和line相关的功能，则包含#include <dxf/entity/ILine.h>即可
	//
}
