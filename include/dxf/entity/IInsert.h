#pragma once

#include <string>
using std::string;

#include <dxf/Base.h>

namespace dxf {
	//typedef Vec3f DxfPoint;

	class IEntity;

	class IInsert
	{
		public:
			virtual ~IInsert() {}
			virtual void setBlockName(const string& s)=0;
			virtual string getBlockName()=0;
			virtual void setInsertPoint(const DxfPoint& pt)=0;
			virtual void setInsertPoint(double x,double y,double z=0.0)=0;
			virtual DxfPoint getInsertPoint()=0;
			virtual void getInsertPoint(double& x,double& y,double& z)=0;
			virtual void setScale(double xs=1,double ys=1,double zs=1)=0;
			virtual void setRotationAngle(double ra)=0;
			virtual double getRotationAngle()=0;

			//
			virtual void transform(IEntity*& p)=0;
	};
}
