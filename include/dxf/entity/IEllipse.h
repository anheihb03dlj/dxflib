#pragma once

#include <dxf/Base.h>

namespace dxf {

	//typedef Vec3f DxfPoint;

	class IEntity;

	class IEllipse
	{
		public:
			virtual ~IEllipse() {}
			virtual void setCenter(const DxfPoint& pt)=0;
			virtual  void setCenter(double x,double y,double z=0.0)=0;
			virtual  DxfPoint getCenter()=0;
			virtual  void getCenter(double& x,double& y,double& z)=0;
			virtual  void setEndPoint(const DxfPoint& pt)=0;
			virtual void setEndPoint(double x,double y,double z=0.0)=0;
			virtual DxfPoint getEndPoint()=0;
			virtual void getEndPoint(double& x,double& y,double& z)=0;
			virtual void setRatio(double ratio)=0;
			virtual double getRatio()=0;
			virtual void setStartAngle(double sa)=0;
			virtual double getStartAngle()=0;
			virtual void setEndAngle(double ea)=0;
			virtual double getEndAngle()=0;

			//
			virtual void transform(IEntity*& p)=0;
	};
}
