#pragma once
#include <dxf/entity/DxfEntity.h>
#include <dxf/entity/ICircle.h>

using namespace dxf;

/** 圆(circle). */
class DxfCircle : public DxfEntity, public ICircle
{
public:
	/**
	 * 创建圆(static).
	 * @param[in] x 圆心坐标-X
	 * @param[in] y 圆心坐标-Y
	 * @param[in] z 圆心坐标-Z(默认为0)
	 * @param[in] r 圆半径(默认为1)
	 * @return DxfCircle指针
	 */
	static DxfCircle* createCircle(double x,double y,double z=0,double r=1);

	/**
	 * 创建圆(static).
	 * @param[in] pt 圆心坐标(默认为(0,0,0))
	 * @param[in] r 圆半径(默认为1)
	 * @return DxfCircle指针
	 * @see DxfPoint
	 */
	static DxfCircle* createCircle(const DxfPoint& pt=DxfPoint(0,0,0),double r=1);

public:

	/** 析构函数. */
	~DxfCircle(void) {
#ifdef _DEBUG
		std::cout<<"调用~DxfCircle(void)"<<std::endl;
#endif
	}

	/**
	 * 设置圆心坐标.
	 * @param[in] cen 圆心坐标
	 */
	virtual void setCenter(const DxfPoint& cen) { this->center=cen; }

	/**
	 * 设置圆心坐标.
	 * @param[in] x 圆心坐标-X
	 * @param[in] y 圆心坐标-Y
	 * @param[in] z 圆心坐标-Z(默认为0.0)
	 */
	virtual void setCenter(double x,double y,double z=0.0) { this->center=DxfPoint(x,y,z); }

	/**
	 * 获取圆心坐标.
	 * @return 圆心坐标
	 * @see DxfPoint
	 */
	virtual DxfPoint getCenter() { return this->center; }

	/**
	 * 获取圆心坐标.
	 * @param[out] x 圆心坐标-X
	 * @param[out] y 圆心坐标-Y
	 * @param[out] z 圆心坐标-Z
	 */
	virtual void getCenter(double& x,double& y,double& z) {	x=this->center.x;	y=this->center.y;	z=this->center.z; }

	/**
	 * 设置圆半径.
	 * @param[in] r 圆半径
	 */
	virtual void setRadius(double r) { this->radius=r; }

	/**
	 * 获取圆半径.
	 * @return 圆半径
	 */
	virtual double getRadius() { return this->radius; }

public:

	/**
	 * 获取circle类型.
	 * @return DxfEntity::CIRCLE
	 * @see DxfEntity::EntityType
	 */
	virtual int typeId() const { return CIRCLE; }

	/** 
	 * 从dxf文件中读入circle的内容.
	 * @param[in] inFile DxfReader指针
	 */
	virtual void read(DxfReader* inFile);

	/**
	 * 将circle的内容写入到dxf文件中
	 * @param[in] outFile DxfWriter指针
	 */
	virtual void write(DxfWriter* outFile);


	//
	virtual void transform(IEntity*& p) { p=this; }
	virtual void transform(ICircle*& p) { p=this; }
	virtual void transform(IReadWrite*& p) { p=this; }

protected:

	/**
	 * 构造函数(保护).
	 * @param[in] pt 圆心坐标(默认为(0,0,0))
	 * @param[in] r 圆半径(默认为1)
	 */
	DxfCircle(const DxfPoint& pt=DxfPoint(0,0,0),double r=1) : center(pt),radius(r) {}

	/**
	 * 构造函数(保护).
	 * @param[in] x 圆心坐标-X
	 * @param[in] y 圆心坐标-Y
	 * @param[in] z 圆心坐标-Z(默认为0)
	 * @param[in] r 圆半径(默认为1)
	 */
	DxfCircle(double x,double y,double z=0,double r=1) : radius(r),center(DxfPoint(x,y,z)) {}

private:
	DxfPoint center;             //圆心坐标，组码-10,20,30
	double radius;               //圆半径，组码-40
};
