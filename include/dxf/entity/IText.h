#pragma once

#include <string>
using std::string;

#include <dxf/Base.h>

namespace dxf {

	//typedef Vec3f DxfPoint;

	class IEntity;

	class IText
	{
		public:
			virtual ~IText() {}
			virtual void setInsertPoint(const DxfPoint& pt)=0;
			virtual void setInsertPoint(double x,double y,double z=0.0)=0;
			virtual DxfPoint getInsertPoint()=0;
			virtual void getInsertPoint(double& x,double& y,double& z)=0;
			virtual void setAlignPoint(const DxfPoint& pt)=0;
			virtual void setAlignPoint(double x,double y,double z=0.0)=0;
			virtual DxfPoint getAlignPoint()=0;
			virtual void getAlignPoint(double& x,double& y,double& z)=0;
			virtual void setTextHeight(double h)=0;
			virtual double getTextHeight()=0;
			virtual void setXScaleFactor(double xsfactor)=0;
			virtual double getXScaleFactor()=0;
			virtual void setTextJustification(int hj=TextType::Left,int vj=TextType::BaseLine)=0;
			virtual void setStyleName(const string& styleName)=0;
			virtual string getStyleName()=0;
			virtual void setText(const string& text)=0;
			virtual string getText()=0;
			virtual void setRotationAngle(double angle)=0;
			virtual double getRotationAngle()=0;

			//
			virtual void transform(IEntity*& p)=0;
	};
}
