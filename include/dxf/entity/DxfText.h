#pragma once

#include <dxf/entity/DxfEntity.h>
#include <dxf/entity/IText.h>


/** 单行文本(text). */
class DxfText : public DxfEntity, public IText
{
public:

	/**
	 * 创建单行文本(static).
	 * @return DxfText指针
	 */
	static DxfText* createText();

	/**
	 * 创建单行文本(static).
	 * @param[in] insertPoint 文本插入点坐标
	 * @param[in] alignPoint 文本对齐坐标(一般不常用)
	 * @param[in] height 文字高度
	 * @param[in] xScaleFactor X比例因子
	 * @param[in] hJustification 水平对齐方式
	 * <ul>
	 * 	  <li>Left
	 * 	  <li>Center
	 * 	  <li>Right
	 * 	  <li>Aligned
	 * 	  <li>HMiddle
	 * 	  <li>Fit
	 * </ul>
	 * @param[in] vJustification 垂直对齐方式
	 * <ul>
	 *    <li>BaseLine
	 *    <li>Bottom
	 *    <li>VMiddle
	 *    <li>Top
	 * </ul>
	 * @param[in] styleName 文本样式名
	 * @param[in] text 文本内容
	 * @param[in] rotationAngle 旋转角度(单位为度)
	 * @return DxfText指针
	 * @see DxfPoint HJustification VJustification
	 */
	static DxfText* createText(const DxfPoint& insertPoint,const DxfPoint& alignPoint,
		double height,double xScaleFactor,int hJustification,int vJustification,
		const string& styleName,const string& text,double rotationAngle);


	/**
	 * 创建单行文本(static).
	 * @param[in] ix 文本插入点坐标-X
	 * @param[in] iy 文本插入点坐标-Y
	 * @param[in] iz 文本插入点坐标-Z
	 * @param[in] ax 文本对齐坐标-X(一般不常用)
	 * @param[in] ay 文本对齐坐标-Y(一般不常用)
	 * @param[in] az 文本对齐坐标-Z(一般不常用)
	 * @param[in] height 文字高度
	 * @param[in] xScaleFactor X比例因子
	 * @param[in] hJustification 水平对齐方式
	 * <ul>
	 * 	  <li>Left
	 * 	  <li>Center
	 * 	  <li>Right
	 * 	  <li>Aligned
	 * 	  <li>HMiddle
	 * 	  <li>Fit
	 * </ul>
	 * @param[in] vJustification 垂直对齐方式
	 * <ul>
	 *    <li>BaseLine
	 *    <li>Bottom
	 *    <li>VMiddle
	 *    <li>Top
	 * </ul>
	 * @param[in] styleName 文本样式名
	 * @param[in] text 文本内容
	 * @param[in] rotationAngle 旋转角度(单位为度)
	 * @return DxfText指针
	 * @see DxfPoint HJustification VJustification
	 */ 
	static DxfText* createText(double ix,double iy,double iz,double ax,double ay,double az,
		double height,double xScaleFactor,	int hJustification,int vJustification,
		const string& styleName,const string& text,double rotationAngle);

public:

	/**
	 * 从dxf文件中读入text的内容.
	 * @param[in] inFile DxfReader指针
	 */
	virtual void read(DxfReader* inFile);

	/**
	 * 将text的内容写入到dxf文件中.
	 * @param[in] outFile DxfWriter指针
	 */
	virtual void write(DxfWriter* outFile);

	/**
	 * 获取text的类型.
	 * @return DxfEntity::TEXT
	 * @see DxfEntity::EntityType
	 */
	virtual int typeId() const { return TEXT; }

public:
	/** 析构函数. */
	~DxfText(void) {
#ifdef _DEBUG
		std::cout<<"调用~DxfText(void)"<<std::endl;
#endif
	}

public:

	/**
	 * 设置文本的插入点坐标.
	 * @param[in] pt 文本插入点坐标
	 */
	virtual void setInsertPoint(const DxfPoint& pt) { this->insertPoint=pt; }

	/**
	 * 设置文本的插入点坐标.
	 * @param[in] x 文本插入点坐标-X
	 * @param[in] y 文本插入点坐标-Y
	 * @param[in] z 文本插入点坐标-Z(默认为0.0)
	 */
	virtual void setInsertPoint(double x,double y,double z=0.0) { this->insertPoint=DxfPoint(x,y,z); }

	/**
	 * 获取文本的插入点坐标.
	 * @return 文本的插入点坐标
	 * @see DxfPoint
	 */
	virtual DxfPoint getInsertPoint() { return this->insertPoint; }

	/**
	 * 获取文本的插入点坐标.
	 * @param[out] x 文本的插入点坐标-X
	 * @param[out] y 文本的插入点坐标-Y
	 * @param[out] z 文本的插入点坐标-Z
	 */
	virtual void getInsertPoint(double& x,double& y,double& z) { x=this->insertPoint.x;	y=this->insertPoint.y;	z=this->insertPoint.z; }

	/**
	 * 设置文本的对齐点坐标.
	 * @param[in] pt 文本对齐点坐标
	 */
	virtual void setAlignPoint(const DxfPoint& pt) { this->alignPoint=pt; }

	/**
	 * 设置文本的对齐点坐标.
	 * @param[in] x 文本对齐点坐标-X
	 * @param[in] y 文本对齐点坐标-Y
	 * @param[in] z 文本对齐点坐标-Z(默认为0.0)
	 */
	virtual void setAlignPoint(double x,double y,double z=0.0) { this->alignPoint=DxfPoint(x,y,z); }

	/**
	 * 获取文本的对齐点坐标.
	 * @return 文本的对齐点坐标
	 * @see DxfPoint
	 */
	virtual DxfPoint getAlignPoint() { return this->alignPoint; }

	/**
	 * 获取文本的对齐点坐标.
	 * @param[out] x 文本的对齐点坐标-X
	 * @param[out] y 文本的对齐点坐标-Y
	 * @param[out] z 文本的对齐点坐标-Z
	 */
	virtual void getAlignPoint(double& x,double& y,double& z) { x=this->alignPoint.x;	y=this->alignPoint.y;	z=this->alignPoint.z; }


	/**
	 * 设置文本高度.
	 * @param[in] h 文本高度
	 */
	virtual void setTextHeight(double h) { this->height=h; }

	/**
	 * 获取文本高度.
	 * @return 文本高度
	 */
	virtual double getTextHeight() { return this->height; }

	/**
	 * 设置x比例因子.
	 * @param[in] xsfactor x比例因子
	 */
	virtual void setXScaleFactor(double xsfactor) { this->xScaleFactor=xsfactor; }

	/**
	 * 获取x比例因子.
	 * @return x比例因子
	 */
	virtual double getXScaleFactor() { return this->xScaleFactor; }

	/**
	 * 设置文本的对齐方式.
	 * @param[in] hj 文本水平对齐方式(默认为Left)
	 * @param[in] vj 文本垂直对齐方式(默认为BaseLine)
	 * @see HJustification VJustification
	 */
	virtual void setTextJustification(int hj=TextType::Left,int vj=TextType::BaseLine) { this->hJustification=hj;	this->vJustification=vj; }

	/**
	 * 设置文本样式.
	 * @param[in] styleName 文本样式名
	 */
	virtual void setStyleName(const string& styleName) { this->style=styleName; }

	/**
	 * 获取文本样式.
	 * @return 文本样式名
	 */
	virtual string getStyleName() { return this->style; }

	/**
	 * 设置文本内容.
	 * @param[in] text 文本内容
	 */
	virtual void setText(const string& text) { this->text=text; }

	/**
	 * 获取文本内容.
	 * @return 文本内容
	 */
	virtual string getText() { return this->text; }

	/**
	 * 设置旋转角度.
	 * @param[in] angle 旋转角度(单位为度)
	 */
	virtual void setRotationAngle(double angle) { this->rotationAngle=angle; }

	/**
	 * 获取旋转角度.
	 * @return 旋转角度(单位为度)
	 */
	virtual double getRotationAngle() { return this->rotationAngle; }

	//
	virtual void transform(IEntity*& p) { p=this; }
	virtual void transform(IText*& p) { p=this; }
	virtual void transform(IReadWrite*& p) { p=this; }

protected:

	/** 构造函数(保护). */
	DxfText() : rotationAngle(0.0), xScaleFactor(1.0), style("STANDARD"), hJustification(TextType::Left), vJustification(TextType::BaseLine), height(50.0), alignPoint(DxfPoint(0,0,0)) {}
	/**
	 * 构造函数(保护).
	 * @param[in] insertPt 文本插入点坐标
	 * @param[in] alignPt 文本对齐坐标(一般不常用)
	 * @param[in] h 文字高度
	 * @param[in] xsFactor X比例因子
	 * @param[in] hj 水平对齐方式
	 * <ul>
	 * 	  <li>Left
	 * 	  <li>Center
	 * 	  <li>Right
	 * 	  <li>Aligned
	 * 	  <li>HMiddle
	 * 	  <li>Fit
	 * </ul>
	 * @param[in] vj 垂直对齐方式
	 * <ul>
	 *    <li>BaseLine
	 *    <li>Bottom
	 *    <li>VMiddle
	 *    <li>Top
	 * </ul>
	 * @param[in] styleName 文本样式名
	 * @param[in] t 文本内容
	 * @param[in] angle 旋转角度(单位为度)
	 * @see DxfPoint HJustification VJustification
	 */
	DxfText(const DxfPoint& insertPt,const DxfPoint& alignPt,
		double h,double xsFactor,int hj,int vj,	const string& styleName,const string& t,double angle)
		:insertPoint(insertPt),alignPoint(alignPt),height(h),xScaleFactor(xsFactor),hJustification(hj),vJustification(vj),rotationAngle(angle),style(styleName),text(t) {}

	/**
	 * 构造函数(保护).
	 * @param[in] ix 文本插入点坐标-X
	 * @param[in] iy 文本插入点坐标-Y
	 * @param[in] iz 文本插入点坐标-Z
	 * @param[in] ax 文本对齐坐标-X(一般不常用)
	 * @param[in] ay 文本对齐坐标-Y(一般不常用)
	 * @param[in] az 文本对齐坐标-Z(一般不常用)
	 * @param[in] h 文字高度
	 * @param[in] xsFactor X比例因子
	 * @param[in] hj 水平对齐方式
	 * <ul>
	 * 	  <li>Left
	 * 	  <li>Center
	 * 	  <li>Right
	 * 	  <li>Aligned
	 * 	  <li>HMiddle
	 * 	  <li>Fit
	 * </ul>
	 * @param[in] vj 垂直对齐方式
	 * <ul>
	 *    <li>BaseLine
	 *    <li>Bottom
	 *    <li>VMiddle
	 *    <li>Top
	 * </ul>
	 * @param[in] styleName 文本样式名
	 * @param[in] t 文本内容
	 * @param[in] angle 旋转角度(单位为度)
	 * @see DxfPoint HJustification VJustification
	 */ 
	DxfText(double ix,double iy,double iz, double ax,double ay,double az,double h,double xsFactor,int hj,int vj,
		const string& styleName,const string& t,double angle)
		:insertPoint(DxfPoint(ix,iy,iz)),alignPoint(DxfPoint(ax,ay,az)),height(h),xScaleFactor(xsFactor),hJustification(hj),vJustification(vj),rotationAngle(angle),style(styleName),text(t) {}

private:

	DxfPoint insertPoint;                         //插入点坐标，组码-10,20,30

	DxfPoint alignPoint;                          //对齐点,组码-11,21,31
	
	double height;                                //高度，组码-40

	double rotationAngle;                         //旋转角度,组码-50

	double xScaleFactor;                          //x比例因子，组码-41

	int hJustification;                           //水平对齐方式,组码-72

	int vJustification;                           //垂直对齐方式,组码-73

	string style;                                 //文本样式，组码-7

	string text;                                  //文本内容，组码-1

	//int textGeneration;                         //文本生成标记，组码-71(不过一般不常用)

};
