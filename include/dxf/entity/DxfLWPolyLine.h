#pragma once
#include <dxf/entity/DxfEntity.h>
#include <dxf/entity/ILWPolyLine.h>

#include <vector>
using std::vector;

using namespace dxf;

/** 多段线(LWPolyLine). */
class DxfLWPolyLine : public DxfEntity, public ILWPolyLine
{
public:

	/**
	 * 创建多段线(static).
	 * @return DxfLWPolyLine指针
	 */
	static DxfLWPolyLine* createLWPolyLine();

public:
	
	/**
	 * 从dxf文件中读入LWPolyLine的内容.
	 * @param[in] inFile DxfReader指针
	 */
	virtual void read(DxfReader* inFile);

	/**
	 * 将LWPolyLine的内容写入到dxf文件中.
	 * @param[in] outFile DxfWriter指针
	 */
	virtual void write(DxfWriter* outFile);

	/**
	 * 获取LWPolyLine的类型.
	 * @return DxfEntity::LWPOLYLINE
	 * @see DxfEntity::EntityType
	 */
	virtual  int typeId() const  { return LWPOLYLINE; }

public:

	/** 析构函数. */
	~DxfLWPolyLine();

	/**
	 * 添加节点.
	 * @param[in] vertex 待添加节点
	 * @see DxfVertex
	 */
	virtual void addVertex(IVertex* vertex) { if(vertex) vertices.push_back(vertex); }

	/**
	 * 多段线节点个数.
	 * @return 节点个数
	 */
	virtual int vertexCount() const { return vertices.size(); }
	/**
	 * 设置多段线标志.
	 * @param[in] isClose 是否闭合(默认不闭合)
	 */
	virtual void setFlag(bool isClose=false) { 	if(isClose) { flag=1; }	else { flag=0; } }

	//
	virtual void transform(IEntity*& p) { p=this; }
	virtual void transform(ILWPolyLine*& p) { p=this; }
	virtual void transform(IReadWrite*& p) { p=this; }

protected:

	/** 构造函数(保护). */
	DxfLWPolyLine() : flag(0) {}

private:
	vector<IVertex*> vertices;          //节点集合

	int flag;                             //是否闭合
};
