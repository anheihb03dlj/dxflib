#pragma once

#include <dxf/entity/DxfEntity.h>
#include <dxf/entity/IMText.h>

using namespace dxf;

/**  多行文本(mtext). */
class DxfMText:public DxfEntity, public IMText
{
public:

	/** 
	 * 创建多行文本(static).
	 * @return DxfMText指针
	 */
	static DxfMText* createMText();

	/**
	 * 创建多行文本(static).
	 * @param[in] insertPoint 多行文本插入点坐标
	 * @param[in] height 文本高度
	 * @param[in] width 参考矩形宽度(一般不常用)
	 * @param[in] attachPoint 对齐方式
	 * <ul>
	 *    <li>TopLeft
	 *    <li>CenterTop
	 *    <li>RightTop
	 *    <li>LeftCenter
	 *    <li>Center
	 *    <li>RightCenter
	 *    <li>LeftBottom
	 *    <li>CenterBottom
	 *    <li>RightBottom
	 * </ul>
	 * @param[in] drawDirection 绘制方向
	 * <ul>
	 *    <li>LeftRight
	 *    <li>TopBottom
	 *    <li>Fit
	 * </ul>
	 * @param[in] text 多行文本内容(字符串内容可以有转义字符，目前只能处理 '\\n' ,'\\t')
	 * @param[in] styleName 文本样式名
	 * @param[in] rotationAngle 旋转角度(单位为度)
	 * @return DxfMText指针
	 * @see DxfPoint AttachPoint DrawDirection
	 */
	static DxfMText* createMText(const DxfPoint& insertPoint,double height,double width,int attachPoint,int drawDirection,const string& text,const string& styleName,double rotationAngle);


	/**
	 * 创建多行文本(static).
	 * @param[in] ix 多行文本插入点坐标-X
	 * @param[in] iy 多行文本插入点坐标-Y
	 * @param[in] iz 多行文本插入点坐标-Z
	 * @param[in] height 文本高度
	 * @param[in] width 参考矩形宽度(一般不常用)
	 * @param[in] attachPoint 对齐方式
	 * <ul>
	 *    <li>TopLeft
	 *    <li>CenterTop
	 *    <li>RightTop
	 *    <li>LeftCenter
	 *    <li>Center
	 *    <li>RightCenter
	 *    <li>LeftBottom
	 *    <li>CenterBottom
	 *    <li>RightBottom
	 * </ul>
	 * @param[in] drawDirection 绘制方向
	 * <ul>
	 *    <li>LeftRight
	 *    <li>TopBottom
	 *    <li>Fit
	 * </ul>
	 * @param[in] text 多行文本内容(字符串内容可以有转义字符，目前只能处理 '\\n' ,'\\t')
	 * @param[in] styleName 文本样式名
	 * @param[in] rotationAngle 旋转角度(单位为弧度)
	 * @return DxfMText指针
	 * @see DxfPoint  AttachPoint DrawDirection
	 */
	static DxfMText* createMText(double ix,double iy,double iz,double height,double width,int attachPoint,int drawDirection,const string& text,const string& styleName,double rotationAngle);

public:

	/** 析构函数. */
	~DxfMText(void) {
#ifdef _DEBUG
		std::cout<<"调用~DxfMText(void)"<<std::endl;
#endif
	}

public:

	/**
	 * 设置多行文本的插入点坐标.
	 * @param[in] pt 插入点坐标
	 * @see DxfPoint
	 */
	virtual void setInsertPoint(const DxfPoint& pt) { this->insertPoint=pt; }

	/**
	 * 设置多行文本的插入点坐标.
	 * @param[in] x 插入点坐标-X
	 * @param[in] y 插入点坐标-Y
	 * @param[in] z 插入点坐标-Z(默认为0.0)
	 */
	virtual void setInsertPoint(double x,double y,double z=0.0) { this->insertPoint=DxfPoint(x,y,z); }

	/**
	 * 获取多行文本的插入点坐标.
	 * @return 插入点坐标
	 * @see DxfPoint
	 */
	virtual DxfPoint getInsertPoint() { return this->insertPoint; }

	/**
	 * 获取多行文本的插入点坐标.
	 * @param[out] x 插入点坐标-X
	 * @param[out] y 插入点坐标-Y
	 * @param[out] z 插入点坐标-Z
	 */
	virtual void getInsertPoint(double& x,double& y,double& z) { x=this->insertPoint.x;	y=this->insertPoint.y;	z=this->insertPoint.z; }

	/**
	 * 设置多行文本的高度.
	 * @param[in] h 文本高度
	 */
	virtual void setTextHeight(double h) { this->height=h; }

	/**
	 * 获取多行文本高度.
	 * @return 文本高度
	 */
	virtual double getTextHeight() { return this->height; }

	/**
	 * 设置文本框宽度.
	 * @param[in] w 文本框宽度
	 * @note 这个一般没有必要设置
	 */
	virtual void setTextWidth(double w) { this->width=w; }

	/**
	 * 获取文本框宽度.
	 * @return 文本框宽度
	 * @note 这个一般可以忽略
	 */
	virtual double getTextWidth() { return this->width; }

	/**
	 * 设置多行文本的对齐方式.
	 * @param[in] attach 文本对齐方式
	 * @see AttachPoint
	 */
	virtual void setAttachPoint(int attach) { this->attachPoint=attach; }

	/**
	 * 设置多行文本的绘制方向.
	 * @param[in] dir 文本绘制方向
	 * @see DrawDirection
	 */
	virtual void setDrawDirection(int dir) { this->drawDirection=dir; }

	/**
	 * 设置文本内容.
	 * @param[in] s  多行文本内容(字符串内容可以有转义字符，目前只能处理 '\\n' ,'\\t')
	 */
	virtual void setText(const string& s) { this->text=s; }

	/**
	 * 获取文本内容.
	 * @return 多行文本内容
	 */
	virtual string getText() { return this->text; }

	/**
	 * 设置文本样式.
	 * @param[in] s 文本样式名
	 */
	virtual void setTextStyle(const string& s) { this->style=s; }

	/**
	 * 获取文本样式.
	 * @return 文本样式名
	 */
	virtual string getTextStyle() { return this->style; }

	/**
	 * 设置文本旋转角度.
	 * @param[in] angle 旋转角度(单位为度)
	 */
	virtual void setRotationAngle(double angle) { this->rotationAngle=angle; }

	/**
	 * 获取文本旋转角度.
	 * @return 文本旋转角度
	 */
	virtual double getRotationAngle() { return this->rotationAngle; }

public:

	/**
	 * 获取mtext类型.
	 * @return DxfEntity::MTEXT
	 * @see DxfEntity::EntityType
	 */
	virtual int typeId() const { return MTEXT; }

	/**
	 * 从dxf文件中读入mtext的内容.
	 * @param[in] inFile DxfReader指针
	 */
	virtual void read(DxfReader* inFile);

	/**
	 * 将mtext的内容写入到dxf文件中.
	 * @param[in] outFile DxfWriter指针
	 */
	virtual void write(DxfWriter* outFile);

	//
	virtual void transform(IEntity*& p) { p=this; }
	virtual void transform(IMText*& p) { p=this; }
	virtual void transform(IReadWrite*& p) { p=this; }

protected:

	/** 构造函数(保护). */
	DxfMText() : style("STANDARD"), attachPoint(MTextType::TopLeft), drawDirection(MTextType::LeftRight), rotationAngle(0.0), height(50.0), width(0.0) {}

	/** 
	 * 构造函数(保护).
	 * @param[in] pt 多行文本插入点坐标
	 * @param[in] h 文本高度
	 * @param[in] w 参考矩形宽度(一般不常用)
	 * @param[in] attachPt 对齐方式
	 * <ul>
	 *    <li>TopLeft
	 *    <li>CenterTop
	 *    <li>RightTop
	 *    <li>LeftCenter
	 *    <li>Center
	 *    <li>RightCenter
	 *    <li>LeftBottom
	 *    <li>CenterBottom
	 *    <li>RightBottom
	 * </ul>
	 * @param[in] drawDir 绘制方向
	 * <ul>
	 *    <li>LeftRight
	 *    <li>TopBottom
	 *    <li>Fit
	 * </ul>
	 * @param[in] t 多行文本内容(字符串内容可以有转义字符，目前只能处理 '\\n' ,'\\t')
	 * @param[in] s 文本样式名
	 * @param[in] angle 旋转角度(单位为度)
	 * @see DxfPoint AttachPoint DrawDirection
	 */
	DxfMText(const DxfPoint& pt,double h,double w,int attachPt,int drawDir,const string& t,const string& s,double angle)
		:insertPoint(pt),height(h),width(w),attachPoint(attachPt),drawDirection(drawDir),text(t),style(s),rotationAngle(angle) {}

	/** 
	 * 构造函数(保护).
	 * @param[in] ix 多行文本插入点坐标-X
	 * @param[in] iy 多行文本插入点坐标-Y
	 * @param[in] iz 多行文本插入点坐标-Z
	 * @param[in] h 文本高度
	 * @param[in] w 参考矩形宽度(一般不常用)
	 * @param[in] attachPt 对齐方式
	 * <ul>
	 *    <li>TopLeft
	 *    <li>CenterTop
	 *    <li>RightTop
	 *    <li>LeftCenter
	 *    <li>Center
	 *    <li>RightCenter
	 *    <li>LeftBottom
	 *    <li>CenterBottom
	 *    <li>RightBottom
	 * </ul>
	 * @param[in] drawDir 绘制方向
	 * <ul>
	 *    <li>LeftRight
	 *    <li>TopBottom
	 *    <li>Fit
	 * </ul>
	 * @param[in] t 多行文本内容(字符串内容可以有转义字符，目前只能处理 '\\n' ,'\\t')
	 * @param[in] s 文本样式名
	 * @param[in] angle 旋转角度(单位为度)
	 * @see DxfPoint AttachPoint DrawDirection
	 */
	DxfMText(double ix,double iy,double iz,double h,double w,int attachPt,int drawDir,const string& t,const string& s,double angle)
		:insertPoint(DxfPoint(ix,iy,iz)),height(h),width(w),attachPoint(attachPt),drawDirection(drawDir),text(t),style(s),rotationAngle(angle) {}

private:
	DxfPoint insertPoint;                          //插入点坐标，组码-10,20,30

	double height;                                 //高度，组码-40

	double width;                                  //文本框宽度，组码-41
	
	int attachPoint;                               //对齐方式，组码-71

	int drawDirection;                             //绘制方向，组码-72

	//int lineSpacingStyle;                        //73

	//double lineSpacingFactor;                    //44

	string text;                                   //文本内容，组码-1

	string style;                                  //文本样式，组码-7

	double rotationAngle;                          //旋转角度，组码-50
};
