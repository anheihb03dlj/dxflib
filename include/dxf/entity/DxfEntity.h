#pragma once

#include <dxf/entity/IEntity.h>

#include <dxf/Base.h>

#include <dxf/IReadWrite.h>

#include <dxf/DxfReader.h>
#include <dxf/DxfWriter.h>

//typedef dxf::Vec3f DxfPoint;    /** DxfPoint类型定义. */

class EntityXData;

namespace dxf {
	class IXData;
}

#include <string>
#include <utility>
#include <map>
using std::map;
using std::string;
using std::pair;
using std::make_pair;


#include <iostream>

/** EntityTypeName类型定义. */
typedef map<int,pair<string,string> > EntityTypeName;

using namespace dxf;


/** 所有实体类的基类(控制实体的图层、颜色、线型等基本属性). */
class DxfEntity : public IEntity, public IReadWrite
{
public:
	static int num;//实体的编号
	static bool first;//仅在第一次执行的时候进行初始化

	/**
	 * 实体类型名实例变量.
	 * @note 包含实体typeId与实体名称、实体arx类名的对应关系
	 */
	static EntityTypeName entityTypeName;

	/**
	 * 根据实体类型名创建实体(static).
	 * @param[in] entityTypeName 实体类型名
	 * @return DxfEntity指针
	 */
	static DxfEntity* createEntityByTypeName(const string& entityTypeName);


public:

	/** 析构函数. */
	virtual ~DxfEntity(void);

	/**
	 * 返回实体类型(纯虚函数).
	 * @return 实体类型
	 */
	virtual int typeId() const=0;

	/**
	 * 从dxf文件中读入实体数据(virtual).
	 * @param[in] inFile DxfReader指针
	 */
	virtual void read(DxfReader* inFile);

	/**
	 * 将实体的数据写入到dxf文件中(virtual).
	 * @param[in] outFile DxfWriter指针
	 */
	virtual void write(DxfWriter* outFile);

	/**
	* 读取扩展数据.
	* @param[in] inFile DxfReader指针
	*/
	void readXData(DxfReader* inFile);

	/**
	* 向dxf文件写入扩展数据.
	* @param[in] outFile DxfWriter指针
	*/
	void writeXData(DxfWriter* outFile);


public:
	/**
	 * 获取实体所在的图层名.
	 * @return 图层名
	 */
	virtual string getLayer();

	/**
	 * 设置实体的图层.
	 * @param[in] s 图层名(默认为空)
	 * @note 如果图层名为空,则将实体的图层设置为当前图层
	 */
	virtual void setLayer(const string& s="");
	
	/**
	 * 获取实体的线型.
	 * @return 实体的线型
	 */
	virtual string getLineType();

	/**
	 * 设置实体的线型.
	 * @param[in] s 实体的线型(默认为空)
	 * @note 如果线型名为空，则设置实体的线型为ByLayer
	 */
	virtual void setLineType(const string& s="");
	
	/**
	 * 获取实体的颜色.
	 * @return 实体的颜色.
	 */
	virtual int getColor();

	/**
	 * 设置实体的颜色.
	 * @param[in] col 实体的颜色
	 */
	virtual void setColor(int col);

	/**
	 * 判断实体是否标记为删除.
	 * @return 如果标记为删除，则返回true,否则返回false
	 */
	virtual bool isDeleted();

	/**
	 * 设置实体是否标记为删除.
	 * @param[in] onOff 删除标记开关(默认为false，不删除)
	 */
	virtual void setDeleted(bool onOff=false);

public:
	/**
	 * 查看和获取实体扩展数据指针.
	 * @param[in] isCreateNewOne 如果该值为true，则当实体没有扩展数据时，新建一个(默认为true)；否则直接返回扩展数据指针
	 * @return EntityXData指针
	 * @see EntityXData
	 * 
	 * @section new_xdata Example
	 * @code
	 * //对扩展数据进行迭代
	 * DxfEntity* entity;(已获得指向entity基类或子类的指针)
	 * EntityXData* pData=entity->xData();//获得扩展数据指针(若扩展数据不存在，默认创建一个新的扩展数据指针)
	 * 
	 * //扩展数据迭代器(对appname进行迭代)
	 * IteratorOfEntityXData entityXDataItr(pData);
	 * for(;!entityXDataItr.isDone();entityXDataItr.next()) {
	 *
	 *    //扩展数据appname
	 *    string appName=entityXDataItr.currentItem();
     *
	 *    //扩展数据项迭代器
	 *    IteratorOfXDataElement xdataElementItr(pData,appName);
	 *
	 *    for(;!xdataElementItr.isDone();xdataElementItr->next()) {
	 *        cout<<xdataElementItr.currentItem()<<" -- ";
	 *    }
	 *    cout<<endl;
	 * }
	 * @endcode
	 * @section get_xdata Example
	 * @code
	 * //查看扩展数据是否存在
	 * DxfEntity* entity;
	 * EntityXData* pData=entity->xData(false);
	 * if(pData==NULL) {
	 *    //扩展数据不存在(仅仅判断是否存在，不做其它的操作)
	 * }
	 * else
	 *    //扩展数据存在(仅仅判断是否存在，不做其它的操作)
	 * @endcode
	 * @note 如果仅仅需要查看扩展数据是否存在，应该调用xData(false),并且不能进行扩展数据的其它操作(如添加、删除扩展数据，对扩展数据进行迭代操作，否则会产生异常问题)
	 */
	virtual IXData* xData(bool isCreateNewOne=true);
	
	//删除扩展数据
	//virtual void deleteXData();


public:
	/**
	 * 获得实体的entityNum.
	 * @return 实体的entityNum
	 * @note 它的作用类似与句柄
	 */
	virtual int getEntityNum() const { return this->entityNum; }

protected:
	/** 构造函数(保护). */
	DxfEntity(void);

private:
	string layer;                         //图层，组码-8

	string linetype;                      //线型，组码-6

	int color;                            //颜色，组啊-62

	bool deletedFlag;                     //删除标记
	
	int entityNum;                        //实体编号

	EntityXData *xdata;                   //扩展数据
};
