#pragma once

namespace dxf {

	class IVertex;

	class IEntity;

	class ILWPolyLine
	{
		public:
			virtual ~ILWPolyLine() {}
			virtual void addVertex(IVertex* vertex)=0;
			virtual int vertexCount() const=0;
			virtual void setFlag(bool isClose=false)=0;

			//
			virtual void transform(IEntity*& p)=0;
	};
}
