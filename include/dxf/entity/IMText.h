#pragma once

#include <string>
using std::string;

#include<dxf/Base.h>

namespace dxf {

	//typedef Vec3f DxfPoint;

	class IEntity;

	class IMText
	{
		public:
			virtual ~IMText() {}
			virtual void setInsertPoint(const DxfPoint& pt)=0;
			virtual void setInsertPoint(double x,double y,double z=0.0)=0;
			virtual DxfPoint getInsertPoint()=0;
			virtual void getInsertPoint(double& x,double& y,double& z)=0;
			virtual void setTextHeight(double h)=0;
			virtual double getTextHeight()=0;
			virtual void setTextWidth(double w)=0;
			virtual double getTextWidth()=0;
			virtual void setAttachPoint(int attach)=0;
			virtual void setDrawDirection(int dir)=0;
			virtual void setText(const string& s)=0;
			virtual string getText()=0;
			virtual void setTextStyle(const string& s)=0;
			virtual string getTextStyle()=0;
			virtual void setRotationAngle(double angle)=0;
			virtual double getRotationAngle()=0;

			//
			virtual void transform(IEntity*& p)=0;
	};
}
