#pragma once

#include <dxf/Base.h>

namespace dxf {

	//typedef Vec3f DxfPoint;

	class IEntity;

	class IArc
	{
		public:
			virtual ~IArc() {}
			virtual void setCenter(double x,double y,double z=0.0)=0;
			virtual void setCenter(const DxfPoint& pt)=0;
			virtual DxfPoint getCenter()=0;
			virtual void getCenter(double& x,double& y,double& z)=0;
			virtual void setRadius(double r)=0;
			virtual double getRadius()=0;
			virtual void setAngle(double sa,double ea)=0;
			virtual void setStartAngle(double sa)=0;
			virtual void setEndAngle(double ea)=0;
			virtual void getAngle(double& sa,double& ea)=0;
			virtual double getStartAngle()=0;
			virtual double getEndAngle()=0;

			//
			virtual void transform(IEntity*& p)=0;
	};
}
