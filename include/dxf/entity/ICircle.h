#pragma once

#include <dxf/Base.h>

namespace dxf {

	//typedef Vec3f DxfPoint;

	class IEntity;

	class ICircle
	{
		public:
			virtual ~ICircle() {}
			virtual void setCenter(const DxfPoint& cen)=0;
			virtual void setCenter(double x,double y,double z=0.0)=0;
			virtual DxfPoint getCenter()=0;
			virtual void getCenter(double& x,double& y,double& z)=0;
			virtual void setRadius(double r)=0;
			virtual double getRadius()=0;

			//
			virtual void transform(IEntity*& p)=0;
	};
}
