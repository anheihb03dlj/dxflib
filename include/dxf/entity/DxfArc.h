#pragma once
#include <dxf/entity/DxfEntity.h>
#include <dxf/entity/IArc.h>

/** 圆弧类(arc). */
class DxfArc : public DxfEntity ,public IArc
{
public:
	/**
	 * 创建圆弧(static).
	 * @param[in] x 圆弧圆心坐标-X
	 * @param[in] y 圆弧圆心坐标-Y
	 * @param[in] z 圆弧圆心坐标-Z
	 * @param[in] r 圆弧半径(默认为1)
	 * @param[in] sa 圆弧起始角度(单位为度,默认为0)
	 * @param[in] ea 圆弧终点角度(单位为度,默认为90.0,即半圆)
	 * @return DxfArc指针
	 */
	static DxfArc* createArc(double x,double y,double z,double r=1,double sa=0.0,double ea=90.0);

	/**
	 * 创建圆弧(static).
	 * @param[in] cen 圆弧圆心坐标(默认为(0,0,0))
	 * @param[in] r 圆弧半径(默认为1)
	 * @param[in] sa 圆弧起始角度(单位为度,默认为0)
	 * @param[in] ea 圆弧终点角度(单位为度,默认为90.0,即半圆)
	 * @return DxfArc指针
	 */ 
	static DxfArc* createArc(const DxfPoint& cen=DxfPoint(0,0,0),double r=1,double sa=0.0,double ea=90.0);

public:
	/**
	 * 从dxf文件中读入arc的内容.
	 * @param[in] inFile DxfReader指针
	 */
	virtual void read(DxfReader* inFile);

	/**
	 * 将arc的内容写入到dxf文件中.
	 * @param[in] outFile DxfWriter指针
	 */
	virtual void write(DxfWriter* outFile);

	/**
	 * 获取arc类型.
	 * @return DxfEntity::ARC
	 * @see DxfEntity::EntityType
	 */
	virtual  int typeId() const { return ARC; }

public:

	/** 析构函数. */
	~DxfArc(void) { 
#ifdef _DEBUG
		std::cout<<"调用~DxfArc(void)"<<std::endl;
#endif
	}

	/**
	 * 设置圆弧的圆心坐标.
	 * @param[in] x 圆心坐标-X
	 * @param[in] y 圆心坐标-Y
	 * @param[in] z 圆心坐标-Z(默认为0.0)
	 */
	virtual void setCenter(double x,double y,double z=0.0) { this->center=DxfPoint(x,y,z); }

	/**
	 * 设置圆弧的圆心坐标.
	 * @param[in] pt 圆心坐标
	 * @see DxfPoint
	 */
	virtual void setCenter(const DxfPoint& pt) { this->center=pt; }

	/**
	 * 获取圆弧圆心坐标.
	 * @return 圆心坐标
	 * @see DxfPoint
	 */
	virtual DxfPoint getCenter() { return this->center; }

	/**
	 * 获取圆心坐标.
	 * @param[out] x 圆心坐标-X
	 * @param[out] y 圆心坐标-Y
	 * @param[out] z 圆心坐标-Z
	 */
	virtual void getCenter(double& x,double& y,double& z) {	x=this->center.x;	y=this->center.y;	z=this->center.z; }

	/**
	 * 设置圆弧半径.
	 * @param[in] r 圆弧半径
	 */
	virtual void setRadius(double r) { this->radius=r; }

	/**
	 * 获取圆弧半径.
	 * @return 圆弧半径
	 */
	virtual double getRadius() { return this->radius; }

	/**
	 * 设置圆弧角度.
	 * @param[in] sa 起始角度(单位为度)
	 * @param[in] ea 终点角度(单位为度)
	 */
	virtual void setAngle(double sa,double ea) { this->startAngle=sa;	this->endAngle=ea; }

	/**
	 * 设置圆弧的起始角度.
	 * @param[in] sa 起始角度(单位为度)
	 */
	virtual void setStartAngle(double sa) { this->startAngle=sa; }

	/**
	 * 设置圆弧的终点角度.
	 * @param[in] ea 终点角度(单位为度)
	 */
	virtual void setEndAngle(double ea) { this->endAngle=ea; }

	/**
	 * 获取圆弧的角度.
	 * @param[out] sa 起始角度(单位为度)
	 * @param[out] ea 终点角度(单位为度)
	 */
	virtual void getAngle(double& sa,double& ea) {	sa=this->startAngle;	ea=this->endAngle; }

	/**
	 * 获取圆弧的起始角度.
	 * @return 起始角度
	 */
	virtual double getStartAngle() { return this->startAngle; }

	/**
	 * 获取圆弧的终点角度.
	 * @return 终点角度
	 */
	virtual double getEndAngle() { return this->endAngle; }

	//
	virtual void transform(IEntity*& p) { p=this; }
	virtual void transform(IArc*& p) { p=this; }
	virtual void transform(IReadWrite*& p) { p=this; }

protected:

	/**
	 * 创建圆弧(static).
	 * @param[in] cen 圆弧圆心坐标(默认为(0,0,0))
	 * @param[in] r 圆弧半径(默认为1)
	 * @param[in] sa 圆弧起始角度(单位为度,默认为0)
	 * @param[in] ea 圆弧终点角度(单位为度,默认为90.0,即半圆)
	 */
	DxfArc(const DxfPoint& cen=DxfPoint(0,0,0),double r=1.0,double sa=0.0,double ea=90.0) : center(cen),radius(r),startAngle(sa),endAngle(ea) {}

private:

	DxfPoint center;                     //圆弧圆心坐标，组码-10,20,30
	double radius;                       //圆弧半径，组码-40
	double startAngle;                   //圆弧起始角度，组码-50
	double endAngle;                     //圆弧终点角度，组码-51
};
