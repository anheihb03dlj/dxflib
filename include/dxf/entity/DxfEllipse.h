#pragma once
#include <dxf/entity/DxfEntity.h>
#include <dxf/entity/IEllipse.h>

using namespace dxf;

/** 椭圆(ellipse). */
class DxfEllipse:public DxfEntity, public IEllipse
{
public:
	/**
	 * 创建椭圆(static).
	 * @return DxfEllipse指针
	 */
	static DxfEllipse* createEllipse();

	/**
	 * 创建椭圆(static).
	 * @param[in] cen 椭圆中心点
	 * @param[in] ept 椭圆长轴端点
	 * @param[in] r 椭圆短轴与长轴之比(默认为0.6090456974522166)
	 * @param[in] sa 起始角度(单位为弧度,默认为0.0)
	 * @param[in] ea 终点角度(单位为弧度,默认为2*PI)
	 * @return DxfEllipse指针
	 * @see DxfPoint
	 */
	static DxfEllipse* createEllipse(const DxfPoint& cen,const DxfPoint& ept,double r=0.6090456974522166,double sa=0,double ea=2*PI);

public:
	/** 析构函数. */
	~DxfEllipse(void) {
#ifdef _DEBUG
		std::cout<<"调用~DxfEllipse(void)"<<std::endl;
#endif
	}

	/**
	 * 设置椭圆中心点坐标.
	 * @param[in] pt 中心点坐标
	 * @see DxfPoint
	 */
	virtual  void setCenter(const DxfPoint& pt) { this->center=pt; }

	/**
	 * 设置中心点坐标.
	 * @param[in] x 中心点坐标-X
	 * @param[in] y 中心点坐标-Y
	 * @param[in] z 中心点坐标-Z(默认为0.0)
	 */
	virtual  void setCenter(double x,double y,double z=0.0) { this->center=DxfPoint(x,y,z); }

	/**
	 * 获取中心点坐标.
	 * @return 中心点坐标
	 * @see DxfPoint
	 */
	virtual  DxfPoint getCenter() { return this->center; }

	/**
	 * 获取中心点坐标.
	 * @param[out] x 中心点坐标-X
	 * @param[out] y 中心点坐标-Y
	 * @param[out] z 中心点坐标-Z
	 */
	virtual  void getCenter(double& x,double& y,double& z) { x=this->center.x;	y=this->center.y;	z=this->center.z; }

	/**
	 * 设置长轴端点坐标.
	 * @param[in] pt 长轴端点坐标
	 * @see DxfPoint
	 */
	virtual  void setEndPoint(const DxfPoint& pt) { this->endPoint=pt; }

	/**
	 * 设置长轴端点坐标.
	 * @param[in] x 长轴端点坐标-X
	 * @param[in] y 长轴端点坐标-Y
	 * @param[in] z 长轴端点坐标-Z(默认为0.0)
	 */
	virtual void setEndPoint(double x,double y,double z=0.0) { this->endPoint=DxfPoint(x,y,z); }

	/**
	 * 获取长轴端点坐标.
	 * @return 长轴端点坐标
	 * @see DxfPoint
	 */
	virtual DxfPoint getEndPoint() { return this->endPoint; }

	/**
	 * 获取长轴端点坐标.
	 * @param[out] x 长轴端点坐标-X
	 * @param[out] y 长轴端点坐标-Y
	 * @param[out] z 长轴端点坐标-Z
	 */
	virtual void getEndPoint(double& x,double& y,double& z) { x=this->endPoint.x;	y=this->endPoint.y;	z=this->endPoint.z; }

	/**
	 * 设置短轴与长轴之比.
	 * @param[in] ratio 短轴与长轴之比
	 */
	virtual void setRatio(double ratio) { this->ratio=ratio; }

	/**
	 * 获取短轴与长轴之比.
	 * @return 短轴与长轴之比
	 */
	virtual double getRatio() { return this->ratio; }

	/**
	 * 设置起始角度.
	 * @param[in] sa 起始角度(单位为弧度)
	 */
	virtual void setStartAngle(double sa) { this->startAngle=sa; }

	/**
	 * 获取起始角度.
	 * @return 起始角度(单位为弧度)
	 */
	virtual double getStartAngle() { return this->startAngle; }

	/**
	 * 设置终点角度.
	 * @param[in] ea 终点角度
	 */
	virtual void setEndAngle(double ea) { this->endAngle=ea; }

	/**
	 * 获取终点角度.
	 * @return 终点角度
	 */
	virtual double getEndAngle() { return this->endAngle; }

public:
	/**
	 * 获取ellipse类型.
	 * @return DxfEntity::ELLIPSE
	 * @see DxfEntity::EntityType
	 */
	virtual  int typeId() const { return ELLIPSE; }
	/**
	 * 从dxf文件中读入ellipse的内容.
	 * @param[in] inFile DxfReader指针
	 */
	virtual void read(DxfReader* inFile);

	/**
	 * 将ellipse的内容写入到dxf文件中
	 * @param[in] outFile DxfWriter指针
	 */
	virtual void write(DxfWriter* outFile);

	//
	virtual void transform(IEntity*& p) { p=this; }
	virtual void transform(IEllipse*& p) { p=this; }
	virtual void transform(IReadWrite*& p) { p=this; }

protected:
	/** 构造函数(保护). */
	DxfEllipse() : ratio(0.6090456974522166), startAngle(0.0), endAngle(2*PI) {}

	/**
	 * 构造函数(保护).
	 * @param[in] cen 椭圆中心点
	 * @param[in] ept 椭圆长轴端点
	 * @param[in] r 椭圆短轴与长轴之比(默认为0.6090456974522166)
	 * @param[in] sa 起始角度(单位为弧度,默认为0.0)
	 * @param[in] ea 终点角度(单位为弧度,默认为2*PI)
	 * @return DxfEllipse指针
	 * @see DxfPoint
	 */
	DxfEllipse(const DxfPoint& cen,const DxfPoint& ept,double r=0.6090456974522166,double sa=0.0,double ea=2*PI)
		:center(cen),endPoint(ept),ratio(r),startAngle(sa),endAngle(ea) {}

private:
	DxfPoint center;                        //椭圆中心点坐标，组码-10,20,30

	DxfPoint endPoint;                      //椭圆长轴端点坐标，组码-11,21,31

	double ratio;                           //短轴与长轴之比，组码-40

	double startAngle;                      //起始角度，组码-41

	double endAngle;                        //终点角度，组码-42
};
