#pragma once

#include <dxf/entity/IXData.h>
#include <dxf/IReadWrite.h>

#include <limits>
#include <string>
#include <map>
#include <list>
using std::map;
using std::list;
using std::numeric_limits;
using std::string;

using namespace dxf;

/** 扩展数据链表. */
typedef list<string> XDataList;


/** 扩展数据类. */
class EntityXData : public IXData, public IReadWrite
{
public:
	/** 构造函数. */
	EntityXData() {}

	/** 析构函数. */
	virtual ~EntityXData();

public:
	virtual void read(DxfReader* inFile);
	virtual void write(DxfWriter* outFile);
	
public:

	virtual XDataIterator* createXDataIterator(); /* { return new IteratorOfEntityXData(this); }*/

	virtual XDataIterator* createXDataItemIterator(const string& appName); /*{ return new IteratorOfXDataElement(this,appName); }*/

	/**
	 * 添加(字符串<-->整数)扩展数据.
	 * @param[in] name 字段名称
	 * @param[in] value 字段值(int)
	 */
	virtual void addData(const string& name,int value);

	/**
	 * 添加(字符串<-->浮点数)扩展数据.
	 * @param[in] name 字段名称
	 * @param[in] value 字段值(double)
	 */
	virtual void addData(const string& name,double value);

	/**
	 * 添加(字符串<-->字符串)扩展数据.
	 * @param[in] name 字段名称
	 * @param[in] value 字段值(string)
	 */
	virtual void addData(const string& name,const string& value);

	/**
	 * 添加(字符串<-->坐标)扩展数据.
	 * @param[in] name 字段名称
	 * @param[in] x 字段值(X坐标)
	 * @param[in] y 字段值(Y坐标)
	 * @param[in] z 字段之(Z坐标)
	 */
	virtual void addData(const string& name,double x,double y,double z=0.0);


	/**
	 * 获取对应字段的值(string).
	 * @param[in] name 字段名称
	 * @param[out] value 字段值
	 * @note 如果value返回为""，则表示该字段不存在
	 */
	virtual void getData(const string& name,string& value);


	/**
	 * 获取对应字段的值(int).
	 * @param[in] name 字段名称
	 * @param[out] value 字段值
	 * @note 如果value返回int的最大范围值，则表示该字段不存在
	 */
	virtual void getData(const string& name,int& value);

	/**
	 * 获取相应字段的值(double).
	 * @param[in] name 字段名称
	 * @param[out] value 字段值
	 * @note 如果value返回double的最大范围值，则表示该字段不存在
	 */
	virtual void getData(const string& name,double& value);


	/**
	 * 获取相应字段的值(point).
	 * @param[in] name 字段名称
	 * @param[out] x,y,z 字段值
	 * @note 如果x,y,z返回double的最大范围值，则表示该字段不存在
	 */
	virtual void getData(const string& name,double& x,double& y,double& z);


	/** 
	 * 添加整数扩展数据.
	 * @param[in] value 整数值
	 */
	virtual void addInt(int value);

	/**
	 * 添加长整数扩展数据.
	 * @param[in] value 长整数
	 */
	virtual void addLong(long value);

	/**
	 * 添加浮点数扩展数据.
	 * @param[in] value 浮点数
	 */
	virtual void addReal(double value);

	/**
	 * 添加3维空间点.
	 * @param[in] x 点坐标-X
	 * @param[in] y 点坐标-Y
	 * @param[in] z 点坐标-Z
	 */
	virtual void addPoint(double x,double y,double z=0.0);

	/**
	 * 添加字符串.
	 * @param[in] value 字符串
	 */
	virtual void addString(const string& value);


	/**
	 * 判断扩展数据使用的appName是否存在.
	 * @param[in] appName 扩展数据appid名
	 * @return appid名存在，则返回true，否则返回false
	 */
	 virtual bool isAppExist(const string& appName);

	/**
	 * 注册应用程序名.
	 * @param[in] appName 扩展数据应用程序名
	 * @note 在调用了该方法之后，不需要再调用setCurrentApp方法；扩展数据自动的添加当前注册的appname中
	 * @note <b>该方法常常和isAppExist方法配合使用，最好不要单独调用该方法</b>
	 * @see isAppExist
	 */
	 virtual void registerApp(const string& appName);

	 /**
	  * 设置该扩展数据为当前.
	  * @param[in] appName 扩展数据应用程序名
	  * @note 在读取和添加数据的时候，应注意所操作的字段是否在当前的appname中，如果不在appname中，则应该调用该方法
	  * @note 该方法常常和isAppExist、registerApp两个方法一起使用，如果已经调用registerApp，则不必再调用该方法了
	  * @see isAppExist,registerApp
	  */
	 virtual void setCurrentApp(const string& appName);


	 //
	 virtual void transform(IReadWrite*& p) { p=this; }
public:
	//返回内部容器，内部使用
	map<string,XDataList>& getXData() { return this->xdatas; }

private:
	map<string,XDataList> xdatas;
	string currentAppName;
};


/** 遍历实体的扩展数据(对扩展数据的appid名进行遍历). */
class IteratorOfEntityXData : public IIterator<string>
{
public:
	/** 
	 * 构造函数.
	 * @param[in] xdata 扩展数据
	 */
	IteratorOfEntityXData(EntityXData* xdata) : xdatas(xdata->getXData()) { first(); }

	/** 析构函数. */
	virtual ~IteratorOfEntityXData() {}

public:


	/** 定位到最开始的位置. */
	virtual void first() { this->currentItr=this->xdatas.begin(); }

	/** 定位到下一个位置. */
	virtual void next();

	/** 判断是否结束. */
	virtual bool isDone() const;

	/** 返回当前迭代器指向的数据. */
	virtual string currentItem() { return currentItr->first; }
	/** 
	 * 获取扩展数据的app的个数
	 * @return 扩展数据app的个数
	 */
	virtual int count() const { return xdatas.size(); }

private:
	typedef map<string,XDataList> XDataListMap;
	XDataListMap::iterator currentItr;
	XDataListMap& xdatas;
};


/** 扩展数据项迭代器. */
class IteratorOfXDataElement : public IIterator<string>
{
public:
	/** 
	 * 构造函数.
	 * @param[in] xdata 扩展数据
	 * @param[in] appName 扩展数据中的appname
	 */
	IteratorOfXDataElement(EntityXData* xdata,const string& appName) : xdatas((xdata->getXData())[appName]) { first(); }

	/** 析构函数. */
	virtual ~IteratorOfXDataElement() {}

public:
	/** 定位到最开始位置. */
	virtual void first() { currentItr=xdatas.begin(); }

	/** 定位到下一个位置. */
	virtual void next() { if(currentItr!=xdatas.end()) { currentItr++; } }

	/** 判断是否到结束. */
	virtual bool isDone() const  { if(currentItr!=xdatas.end()) { return false; } else { return true; } }

	/** 返回当前迭代器所指向的扩展数据项. */
	virtual string currentItem() { return *currentItr; }

	/** 
	 * 获取appname对应的扩展数据的个数
	 * @return appname对应的扩展数据的个数
	 */
	virtual int count() const { return xdatas.size(); }

private:
	list<string>& xdatas;                   //扩展数据链表
	list<string>::iterator currentItr;     //当前迭代器指针

};
