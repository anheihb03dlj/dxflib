#pragma once

#include <string>
using std::string;

namespace dxf {
	//class EntityXData;
	class IXData;

	//
	class ILine;
	class IArc;
	class ICircle;
	class IEllipse;
	class IText;
	class IMText;
	class ILWPolyLine;
	class IInsert;
	class IVertex;

	//
	class IReadWrite;

	class IEntity
	{
		public:
			virtual ~IEntity() {}
			virtual int typeId() const=0;
			virtual string getLayer()=0;
			virtual void setLayer(const string& s="")=0;
			virtual string getLineType()=0;
			virtual void setLineType(const string& s="")=0;
			virtual int getColor()=0;
			virtual void setColor(int col)=0;
			virtual bool isDeleted()=0;
			virtual void setDeleted(bool onOff=false)=0;

			virtual IXData* xData(bool isCreateNewOne=true)=0;

			virtual int getEntityNum() const=0;

			//
			virtual void transform(ILine*& p) { p=NULL; }
			virtual void transform(IArc*& p) { p=NULL; }
			virtual void transform(ICircle*& p) { p=NULL; }
			virtual void transform(IEllipse*& p) { p=NULL; }
			virtual void transform(IText*& p) { p=NULL; }
			virtual void transform(IMText*& p) { p=NULL; }
			virtual void transform(ILWPolyLine*& p) { p=NULL; }
			virtual void transform(IInsert*& p) { p=NULL; }
			virtual void transform(IVertex*& p) { p=NULL; }

			//
			virtual void transform(IReadWrite*& p)=0;
	};
}
