#pragma once

#include <dxf/IIterator.h>

namespace dxf {

	class IReadWrite;

	class IXData
	{
		public:
			virtual ~IXData() {}

			virtual XDataIterator* createXDataIterator()=0;
			virtual XDataIterator* createXDataItemIterator(const string& appName)=0;

			virtual void addData(const string& name,int value)=0;
			virtual void addData(const string& name,double value)=0;
			virtual void addData(const string& name,const string& value)=0;
			virtual void addData(const string& name,double x,double y,double z=0.0)=0;


			virtual void getData(const string& name,string& value)=0;
			virtual void getData(const string& name,int& value)=0;
			virtual void getData(const string& name,double& value)=0;
			virtual void getData(const string& name,double& x,double& y,double& z)=0;


			virtual void addInt(int value)=0;
			virtual void addLong(long value)=0;
			virtual void addReal(double value)=0;
			virtual void addPoint(double x,double y,double z=0.0)=0;
			virtual void addString(const string& value)=0;


			virtual bool isAppExist(const string& appName)=0;
			virtual void registerApp(const string& appName)=0;
			virtual void setCurrentApp(const string& appName)=0;

			//
			virtual void transform(IReadWrite*& p)=0;
	};
}
