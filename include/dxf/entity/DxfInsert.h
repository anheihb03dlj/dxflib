#pragma once
#include <dxf/entity/DxfEntity.h>
#include <dxf/entity/IInsert.h>

using namespace dxf;

/** 块引用(insert). */
class DxfInsert:public DxfEntity, public IInsert
{
public:

	/**
	 * 创建块引用(static).
	 * @param[in] name 块名
	 * @param[in] pt 插入点坐标
	 * @param[in] xs x轴缩放比例(默认值为1)
	 * @param[in] ys y轴缩放比例(默认值为1)
	 * @param[in] zs z轴缩放比例(默认值为1)
	 * @param[in] ra 旋转角度(单位为弧度，默认值为0)
	 * @return DxfInsert指针
	 * @see DxfPoint
	 */
	static DxfInsert* createInsert(const string& name,const DxfPoint& pt=DxfPoint(0,0,0),double xs=1,double ys=1,double zs=1,double ra=0);

	/**
	 * 创建块引用(static).
	 * @return DxfInsert指针
	 */
	static DxfInsert* createInsert();

public:

	/**
	 * 从dxf文件中读入insert的内容.
	 * @param[in] inFile DxfReader指针
	 */
	virtual void read(DxfReader* inFile);

	/**
	 * 将insert的内容写入到dxf文件中.
	 * @param[in] outFile DxfWriter指针
	 */
	virtual void write(DxfWriter* outFile);

	/**
	 * 获取insert的类型.
	 * @return DxfEntity::INSERT
	 * @see DxfEntity::EntityType
	 */
	virtual int typeId() const { return INSERT; }
public:

	/** 析构函数. */
	~DxfInsert(void) {
#ifdef _DEBUG	
		std::cout<<"调用~DxfInsert(void)"<<std::endl;
#endif
	}

	/**
	 * 设置块名.
	 * @param[in] s 块名
	 */
	virtual void setBlockName(const string& name) { this->blockName=name; }

	/**
	 * 获取块名.
	 * @return 块名
	 */
	virtual string getBlockName() { return this->blockName; }

	/**
	 * 设置插入点坐标.
	 * @param[in] pt 插入点坐标
	 * @see DxfPoint
	 */
	virtual void setInsertPoint(const DxfPoint& pt) { this->insertPoint=pt; }

	/**
	 * 设置插入点坐标.
	 * @param[in] x 插入点坐标-X
	 * @param[in] y 插入点坐标-Y
	 * @param[in] z 插入点坐标-Z(默认为0)
	 */
	virtual void setInsertPoint(double x,double y,double z=0.0) { this->insertPoint=DxfPoint(x,y,z); } 

	/**
	 * 获取插入点坐标.
	 * @return 插入点坐标
	 * @see DxfPoint
	 */
	virtual DxfPoint getInsertPoint() { return this->insertPoint; }

	/**
	 * 获取插入点坐标.
	 * @param[out] x 插入点坐标-X
	 * @param[out] y 插入点坐标-Y
	 * @param[out] z 插入点坐标-Z
	 */
	virtual void getInsertPoint(double& x,double& y,double& z) { x=this->insertPoint.x;  y=this->insertPoint.y;	 z=this->insertPoint.z; }

	/**
	 * 设置插入块的比例.
	 * @param[in] xs x轴比例(默认为1)
	 * @param[in] ys y轴比例(默认为1)
	 * @param[in] zs z轴比例(默认为1)
	 */
	virtual void setScale(double xs=1,double ys=1,double zs=1) { this->xscale=xs;	this->yscale=ys;	this->zscale=zs; }

	/**
	 * 设置旋转角度.
	 * @param[in] ra 旋转角度
	 */
	virtual void setRotationAngle(double ra) { this->rotationAngle=ra; }

	/**
	 * 获取旋转角度.
	 * @return 旋转角度
	 */
	virtual double getRotationAngle() { return this->rotationAngle; }

	virtual void transform(IEntity*& p) { p=this;}
	virtual void transform(IInsert*& p) { p=this; }
	virtual void transform(IReadWrite*& p) { p=this; }

protected:

	/** 构造函数(保护). */
	DxfInsert()	: rotationAngle(0.0), xscale(1.0), yscale(1.0), zscale(1.0) {}

	/** 
	 * 构造函数(保护).
	 * @param[in] name 块名
	 * @param[in] pt 块插入点坐标(默认为(0,0,0))
	 * @param[in] xs x轴比例(默认为1）
	 * @param[in] ys y轴比例(默认为1)
	 * @param[in] zs z轴比例(默认为1)
	 * @param[in] ra 旋转角度(单位为度)
	 */
	DxfInsert(const string& name,const DxfPoint& pt=DxfPoint(0,0,0),double xs=1,double ys=1,double zs=1,double ra=0)
		:blockName(name),insertPoint(pt),xscale(xs),yscale(ys),zscale(zs),rotationAngle(ra) {}

private:
	string blockName;                             //块名，组码-2

	DxfPoint insertPoint;                         //插入点坐标，组码-10,20,30

	double xscale;                                //x轴比例，组码-41

	double yscale;                                //y轴比例，组码-42

	double zscale;                                //z轴比例，组码-43

	double rotationAngle;                         //旋转角度，组码-50
};
