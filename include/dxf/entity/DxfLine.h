#pragma once
#include <dxf/entity/DxfEntity.h>
#include <dxf/entity/ILine.h>

using namespace dxf;

/** 直线类(Line). */
class DxfLine : public DxfEntity, public ILine
{
public:
	/**
	 * 创建直线(static).
	 * @param[in] startPt 直线起点坐标(默认为(0,0,0))
	 * @param[in] endPt 直线终点坐标(默认为(0,0,0))
	 * @return DxfLine指针
	 */
	static DxfLine* createLine(const DxfPoint& startPt=DxfPoint(0,0,0),const DxfPoint& endPt=DxfPoint(0,0,0));

	/**
	 * 创建直线(static).
	 * @param[in] p1x 直线起点坐标-X
	 * @param[in] p1y 直线起点坐标-Y
	 * @param[in] p1z 直线起点坐标-Z
	 * @param[in] p2x 直线终点坐标-X
	 * @param[in] p2y 直线终点坐标-Y
	 * @param[in] p2z 直线终点坐标-Z
	 * @return DxfLine指针
	 */
	static DxfLine* createLine(double p1x,double p1y,double p1z,double p2x,double p2y,double p2z);

public:
	/** 析构函数. */
	virtual ~DxfLine(void) { 
#ifdef _DEBUG	
		std::cout<<"调用~DxfLine(void)"<<std::endl;
#endif
	}

	/** 设置直线的起点坐标.
	 * @param[in] pt 起点坐标
	 */
	virtual void setStartPoint(const DxfPoint& pt) { this->startPoint=pt; }

	/**
	 * 设置直线的起点坐标.
	 * @param[in] x 起点坐标-X
	 * @param[in] y 起点坐标-Y
	 * @param[in] z 起点坐标-Z(默认为0)
	 */
	virtual void setStartPoint(const double x,const double y,const double z=0) { this->startPoint=DxfPoint(x,y,z); }

	/**
	 * 设置直线的终点坐标.
	 * @param[in] pt 终点坐标
	 */
	virtual void setEndPoint(const DxfPoint& pt) { this->startPoint=pt; }

	/**
	 * 设置直线的终点坐标.
	 * @param[in] x 终点坐标-X
	 * @param[in] y 终点坐标-Y
	 * @param[in] z 终点坐标-Z(默认为0)
	 */
	virtual void setEndPoint(const double x,const double y,const double z=0) { this->endPoint=DxfPoint(x,y,z); }

	/**
	 * 获取直线起点坐标.
	 * @return 起点坐标
	 */
	virtual DxfPoint getStartPoint() { return startPoint; }

	/**
	 * 获取直线起点坐标.
	 * @param[out] x 直线起点坐标-X
	 * @param[out] y 直线起点坐标-Y
	 * @param[out] z 直线起点坐标-Z
	 */
	virtual void getStartPoint(double& x,double& y,double& z) { x=this->startPoint.x;	y=this->startPoint.y;	z=this->startPoint.z; }

	/**
	 * 获取直线终点坐标.
	 * @return 终点坐标
	 */
	virtual DxfPoint getEndPoint() { return endPoint; }

	/**
	 * 获取直线终点坐标.
	 * @param[out] x 直线终点坐标-X
	 * @param[out] y 直线终点坐标-Y
	 * @param[out] z 直线终点坐标-Z
	 */
	virtual void getEndPoint(double& x,double& y,double& z) { x=this->endPoint.x;	y=this->endPoint.y;	z=this->endPoint.z; }

public:
	/** 
	 * 获取直线类型.
	 * @return DxfEntity::LINE.
	 * @see DxfEntity::EntityType
	 */
	virtual int typeId() const { return LINE; }
	/**
	 * 从dxf文件中读入line的数据.
	 * @param[in] inFile DxfReader指针
	 */
	virtual void read(DxfReader* inFile);

	/**
	 * 将line的数据写入到dxf文件中.
	 * @param[in] outFile DxfWriter指针
	 */
	virtual void write(DxfWriter* outFile);

	//
	virtual void transform(IEntity*& p) { p=this; }
	virtual void transform(ILine*& p) { p=this; }
	virtual void transform(IReadWrite*& p) { p=this; }

protected:

	/**
	 * 构造函数(保护).
	 * @param[in] startPt 直线起点坐标(默认为(0,0,0))
	 * @param[in] endPt 直线终点坐标(默认为(0,0,0))
	 */
	DxfLine(const DxfPoint& startPt=DxfPoint(0,0,0),const DxfPoint& endPt=DxfPoint(0,0,0))
		:startPoint(startPt), endPoint(endPt) {}

	/**
	 * 构造函数(保护).
	 * @param[in] p1x 直线起点坐标-X
	 * @param[in] p1y 直线起点坐标-Y
	 * @param[in] p1z 直线起点坐标-Z
	 * @param[in] p2x 直线终点坐标-X
	 * @param[in] p2y 直线终点坐标-Y
	 * @param[in] p2z 直线终点坐标-Z
	 */
	DxfLine(double p1x,double p1y,double p1z,double p2x,double p2y,double p2z)
		:startPoint(p1x,p1y,p1z),endPoint(p2x,p2y,p2z) {}

private:
	DxfPoint startPoint;              //直线起点坐标，组码-10,20,30
	DxfPoint endPoint;                //直线终点坐标，组码-11,21,31
};
