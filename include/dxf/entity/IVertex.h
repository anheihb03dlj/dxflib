#pragma once

#include <dxf/Base.h>

namespace dxf {

	//typedef Vec3f DxfPoint;

	class IEntity;

	class IVertex
	{
		public:
			virtual ~IVertex() {}
			virtual void setPoint(const DxfPoint& pt)=0;
			virtual void setPoint(double x,double y,double z=0.0)=0;
			virtual DxfPoint getPoint()=0;
			virtual void getPoint(double& x,double& y,double& z)=0;
			virtual void setStartWidth(double sw)=0;
			virtual void setEndWidth(double ew)=0;
			virtual double getStartWidth()=0;
			virtual double getEndWidth()=0;
			virtual void setSEWidth(double sw,double ew)=0;
			virtual void getSEWidth(double& sw,double& ew)=0;

			//
			virtual void transform(IEntity*& p)=0;
	};
}
