#pragma once

#include <dxf/entity/DxfEntity.h>
#include <dxf/entity/IVertex.h>

using namespace dxf;

/** 节点(vertex). */
class DxfVertex : public DxfEntity ,public IVertex
{
public:

	/**
	 * 创建节点(static).
	 * @return DxfVertex指针
	 */
	static DxfVertex* createVertex();

	/**
	 * 创建节点(static).
	 * @param[in] x 节点的坐标-X
	 * @param[in] y 节点的坐标-Y
	 * @param[in] z 节点的坐标-Z(默认为0.0)
	 * @param[in] sw 起点宽度(默认为0.0)
	 * @param[in] ew 终点宽度(默认为0.0)
	 * @return DxfVertex指针
	 */
	static DxfVertex* createVertex(double x,double y,double z=0.0,double sw=0.0,double ew=0.0);

	/**
	 * 创建节点(static).
	 * @param[in] pt 节点坐标
	 * @param[in] sw 起点宽度(默认为0.0)
	 * @param[in] ew 终点宽度(默认为0.0)
	 * @return DxfVertex指针
	 * @see DxfPoint
	 */
	static DxfVertex* createVertex(const DxfPoint& pt,double sw=0.0,double ew=0.0);

public:

	/**
	 * 设置节点坐标.
	 * @param[in] pt 节点坐标
	 * @see DxfPoint
	 */
	virtual void setPoint(const DxfPoint& pt) { this->point=pt; }

	/**
	 * 设置节点坐标.
	 * @param[in] x 节点坐标-X
	 * @param[in] y 节点坐标-Y
	 * @param[in] z 节点坐标-Z(默认为0.0)
	 */
	virtual void setPoint(double x,double y,double z=0.0) { this->point=DxfPoint(x,y,z); }

	/**
	 * 获取节点坐标.
	 * @return 节点坐标
	 */
	virtual DxfPoint getPoint() { return this->point; }

	/**
	 * 获取节点坐标.
	 * @param[out] x 节点坐标-X
	 * @param[out] y 节点坐标-Y
	 * @param[out] z 节点坐标-Z
	 */
	virtual void getPoint(double& x,double& y,double& z) { x=this->point.x;	y=this->point.y; z=this->point.z; }

	/**
	 * 设置节点起点宽度.
	 * @param[in] sw 起点宽度
	 */
	virtual void setStartWidth(double sw) { this->startWidth=sw; }

	/**
	 * 设置节点终点宽度.
	 * @param[in] ew 终点宽度
	 */ 
	virtual void setEndWidth(double ew) { this->endWidth=ew; }

	/**
	 * 获取节点起点宽度.
	 * @return 起点宽度
	 */
	virtual double getStartWidth() { return this->startWidth; }

	/**
	 * 获取节点终点宽度.
	 * @return 终点宽度
	 */
	virtual double getEndWidth() { return this->endWidth; }

	/**
	 * 设置节点起点及终点宽度.
	 * @param[in] sw 起点宽度
	 * @param[in] ew 终点宽度
	 */
	virtual void setSEWidth(double sw,double ew) { this->startWidth=sw;	this->endWidth=ew; }

	/**
	 * 获取节点起点及终点宽度.
	 * @param[out] sw 起点宽度
	 * @param[out] ew 终点宽度
	 */
	virtual void getSEWidth(double& sw,double& ew) { sw=this->startWidth;	ew=this->endWidth; }

	/** 析构函数. */
	~DxfVertex(void) {
#ifdef _DEBUG	
		std::cout<< "调用~DxfVertex(void)"<<std::endl;
#endif
	}

public:
	/**
	 * 获取vertex的类型.
	 * @return DxfEntity::VERTEX
	 * @see DxfEntity::EntityType
	 */
	virtual int typeId() const { return VERTEX; }

	/**
	 * 从dxf文件中读入vertex的内容.
	 * @param[in] inFile DxfReader指针
	 */
	virtual void read(DxfReader* inFile);

	/**
	 * 将vertex的内容写入dxf文件中.
	 * @param[in] outFile DxfWriter指针
	 */
	virtual void write(DxfWriter* outFile);

	virtual void transform(IEntity*& p) { p=this; }
	virtual void transform(IVertex*& p) { p=this; }
	virtual void transform(IReadWrite*& p) { p=this; }

protected:

	/** 构造函数(保护). */
	DxfVertex(void) : startWidth(0.0), endWidth(0.0) {}

	/**
	 * 构造函数(保护).
	 * @param[in] pt 节点坐标
	 * @param[in] sw 起点宽度(默认为0.0)
	 * @param[in] ew 终点宽度(默认为0.0)
	 * @see DxfPoint
	 */
	DxfVertex(const DxfPoint& pt,double sw=0.0,double ew=0.0) : point(pt),startWidth(sw),endWidth(ew) {}

private:
	DxfPoint point;                           //节点坐标，组码-10,20,30

	double startWidth;                        //起点宽度，组码-40

	double endWidth;                          //终点宽度，组码-41

	//double bulge;                           //凸度,组码-42

	//int flag;                               //顶点标志,组码-70

	//double tangentDir;                      //曲线拟合切线方向,组码-50(暂时不考虑曲线拟合)
};
