#pragma once

#include <dxf/Base.h>

namespace dxf {

	//typedef Vec3f DxfPoint;

	class IEntity;

	class ILine
	{
		public:
			virtual ~ILine() {}
			virtual void setStartPoint(const DxfPoint& pt)=0;
			virtual void setStartPoint(const double x,const double y,const double z=0)=0;
			virtual void setEndPoint(const DxfPoint& pt)=0;
			virtual void setEndPoint(const double x,const double y,const double z=0)=0;
			virtual DxfPoint getStartPoint()=0;
			virtual void getStartPoint(double& x,double& y,double& z)=0;
			virtual DxfPoint getEndPoint()=0;
			virtual void getEndPoint(double& x,double& y,double& z)=0;

			//
			virtual void transform(IEntity*& p)=0;
	};
}
