#pragma once

namespace dxf {

	class DxfReader;
	class DxfWriter;

	class IReadWrite
	{
		public:
			virtual ~IReadWrite() {}
			virtual void read(DxfReader* inFile)=0;
			virtual void write(DxfWriter* outFile)=0;
	};

}
