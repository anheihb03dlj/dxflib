#include <stdlib.h>
#include <time.h>

#include <dxf/IDxf.h>

#ifdef _DEBUG
#pragma comment(lib,"dxfd.lib")
#else
#pragma comment(lib,"dxf.lib")
#endif

#include <iostream>
#include <string>
using std::string;
using std::cout;
using std::endl;

#include <dxf/entity/ILine.h>
#include <dxf/entity/ICircle.h>
#include <dxf/entity/IArc.h>
#include <dxf/entity/IEllipse.h>
#include <dxf/entity/IText.h>
#include <dxf/entity/IMText.h>
#include <dxf/entity/ILWPolyLine.h>
#include <dxf/entity/IVertex.h>
#include <dxf/entity/IInsert.h>

using namespace dxf;

//检测内存泄漏的代码
//inline void EnableMemLeakCheck()
//{
//   _CrtSetDbgFlag(_CrtSetDbgFlag(_CRTDBG_REPORT_FLAG) | _CRTDBG_LEAK_CHECK_DF);
//}
//
//#ifdef _DEBUG
//#define new   new(_NORMAL_BLOCK, __FILE__, __LINE__)
//#endif

//	EnableMemLeakCheck();//开启内存泄漏检查
//_CrtSetBreakAlloc(919);//定位第xx次内存分配

void testWrite();
void testRead();

int main()
{
	testWrite();
	std::cout<<"\n*********************"<<std::endl;
	std::cout<<"再次调用test()"<<std::endl;
	std::cout<<"\n*********************"<<std::endl;
	testWrite();
	return 0;
}



void testWrite()
{
	IDxf* dxf=createDxf();
	//创建新的文件，并准备写入
	DxfWriter* outFile=dxf->out("test.dxf");


	/*创建图层测试，如果没有提供图层名称参数，则按照 “图层n” 的命名方式创建图层*/

	//创建"图层1"
	ILayer* layer=dxf->createLayer();
	dxf->addLayer(layer);

	//创建"图层2"
	layer=dxf->createLayer();
	dxf->addLayer(layer);

	//创建"图层3"
	layer=dxf->createLayer();
	dxf->addLayer(layer);

	//创建"图层4"
	layer=dxf->createLayer();
	dxf->addLayer(layer);

	//创建"图层5"
	layer=dxf->createLayer();
	dxf->addLayer(layer);


	//创建"通风网络图层"
	layer=dxf->createLayer();
	layer->setLayerName("通风网络图层");
	layer->setColor(BLUE);
	layer->setLineType("Continuous");
	dxf->addLayer(layer);

	//设置当前图层
	dxf->setCurrentLayer("通风网络图层");


	IEntity* ent=NULL;//这个作为下面的接口之间的转换操作的中间变量

	//创建直线line
	ILine* line=dxf->createLine();
	//要设置实体的一般属性（即entity的属性：颜色、图层、线型等），必须使用IEntity接口，因此需要将接口进行转换，使用transform方法进行该转换
	//line->transform(ent);
	//ent->setLayer("图层1");
	//ent->setColor(WHITE);
	line->setStartPoint(10,20);
	line->setEndPoint(30,50);

	//使用addEntity方法添加entity，必须转换成entity指针
	line->transform(ent);
	dxf->addEntity(ent);//需要IEntity*
	//或者直接用方法addLine添加直线
	//dxf->addLine(line);


	//****************************************
	//给直线line添加xdata,使用IXData接口
	line->transform(ent);
	IXData* pXData=ent->xData();//通过IEntity接口获取IXData接口指针

	//下面的xdata方法属于IXData接口
	if(!pXData->isAppExist("测试扩展数据"))
		pXData->registerApp("测试扩展数据");
	//pXData->setCurrentApp("测试扩展数据");//可以不用调用setCurrentApp方法，因为registerApp内部已经调用了该方法

	pXData->addData("风速",4.60);
	pXData->addData("风压",100);//百帕
	pXData->addData("摩擦阻力系数",0.123344);
	pXData->addData("温度",23.5);
	pXData->addData("坐标",-700,-800);



	if(!pXData->isAppExist("井下通风参数"))
		pXData->registerApp("井下通风参数");
	//pXData->setCurrentApp("井下通风参数");

	pXData->addString("风量");
	pXData->addReal(20.3);
	pXData->addString("压力");
	pXData->addReal(14567.4);
	pXData->addString("温度");
	pXData->addReal(23.6);
	pXData->addString("坐标");
	pXData->addPoint(-700,-800);

	if(!pXData->isAppExist("附加数据"))
		pXData->registerApp("附加数据");
	//pXData->setCurrentApp("附加数据");
	pXData->addInt(1000);
	pXData->addLong(885375873);
	pXData->addReal(555.6);
	pXData->addString("附注");
	pXData->addPoint(13,23);
	//addInt(),addLong(),addString(),addReal(),addPoint()
	//****************************************

	//创建直线line1
	ILine* line1=dxf->createLine();
	line1->transform(ent);
	ent->setLayer("图层1");
	//line->setColor(WHITE);
	line1->setStartPoint(100,200);
	line1->setEndPoint(300,500);
	//dxf->addEntity(line1); error?
	dxf->addLine(line1);

	////创建块block，包含直线line
	IBlock* block=dxf->createBlock();
	double x,y,z;
	line->getStartPoint(x,y,z);
	block->setBasePoint(x,y,z);
	block->setBlockName("块1");
	dxf->addBlock(block);
	line->transform(ent);
	block->add(ent);

	//创建块block1,包含直线line1
	IBlock* block1=dxf->createBlock();
	line1->getStartPoint(x,y,z);
	block1->setBasePoint(x,y,z);
	block1->setBlockName("块2");
	dxf->addBlock(block1);
	line1->transform(ent);
	block1->add(ent);

	//设置线型，并加载到图形中
	line->transform(ent);
	ILineType* lt=dxf->loadLineType("ACAD_ISO02W100");
	if(lt!=NULL) {
		std::string sname=lt->getLineTypeName();
		ent->setLineType(sname);
	}

	//创建circle
	ICircle* circle=dxf->createCircle();
	circle->setCenter(1100,2000);
	circle->setRadius(50);
	dxf->addCircle(circle);
	/*circle->transform(ent);
	dxf->addEntity(ent);*/

	////作为速度测试，读写速度有点慢
	//// srand ((unsigned int)time(NULL) );
	//// int randNumX,randNumY;

	////for(int i=0;i<1000;i++) {
	////	line=dxf.createLine();
	////	randNumX= rand() % 10000 + 1;
	////	randNumY= rand() % 10000 + 1;

	////	line->setStartPoint(randNumX,randNumY);
	////	randNumX= rand() % 10000 + 1;
	////	randNumY= rand() % 10000 + 1;
	////	line->setEndPoint(randNumX,randNumY);

	////	dxf.addEntity(line);
	////}

	//创建arc
	IArc* arc=dxf->createArc();
	arc->setCenter(1400,1500);
	arc->setRadius(60);
	arc->setAngle(0,180);
	dxf->addArc(arc);
	/*arc->transform(ent);
	dxf->addEntity(ent);*/

	//ellipse测试(注意ellipse数据的有效性，否则cad不能识别)
	IEllipse* ellipse=dxf->createEllipse();
	ellipse->setCenter(953.90934356397793,502.98094701784669);
	ellipse->setEndPoint(-369.28569842378062,266.11749905621468);
	//ellipse->setStartAngle(0);
	//ellipse->setEndAngle(2*PI);
	dxf->addEllipse(ellipse);
	/*ellipse->transform(ent);
	dxf->addEntity(ent);*/

	ellipse->transform(ent);
	ent->setDeleted(true);//删除实体的另外的一种方法，将实体标记为deleted，则在输出dxf文件时，将不会写入到dxf文件中



	//创建单行文本
	IText* text=dxf->createText();
	text->transform(ent);
	ent->setColor(YELLOW);//设置entity属性
	text->setInsertPoint(500,600);
	//text->setAlignPoint(600,700);
	//text->setTextHeight(50);
	text->setRotationAngle(60);
	text->setText("anheihb03dlj");
	//text->setXScaleFactor(20);
	text->setTextJustification(TextType::HMiddle,TextType::VMiddle);
	dxf->addText(text);

	//创建多行文本
	IMText* mtext=dxf->createMText();
	mtext->setInsertPoint(700,500);
	mtext->setTextHeight(100);
	string stext="anbdjjffffffffffffffffffffffffffffff\ndkkkkkkkkkkk\
				 anbdjjffffffffffffffffffffffffffffff\ndkkkkkkkkkkk\
				 nbdjjffffffffffffffffffffffffffffff\ndkkkkkkkkkkk\
				 anbdjjffffffffffffffffffffffffffffff\ndkkkkkkkkkkk \
				 anbdjjffffffffffffffffffffffffffffff\ndkkkkkkkkkkk\
				 anbdjjffffffffffffffffffffffffffffff\ndkkkkkkkkkkk\
				 anbdjjffffffffffffffffffffffffffffff\ndkkkkkkkkkkk\
				 anbdjjffffffffffffffffffffffffffffff\ndkkkkkkkkkkk\
				 anbdjjffffffffffffffffffffffffffffff\ndkkkkkkkkkkk\
				 anbdjjffffffffffffffffffffffffffffff\ndkkkkkkkkkkk\
				 anbdjjffffffffffffffffffffffffffffff\ndkkkkkkkkkkk\
				 anbdjjffffffffffffffffffffffffffffff\ndkkkkkkkkkkk\
				 anbdjjffffffffffffffffffffffffffffff\ndkkkkkkkkkkk\
				 anbdjjffffffffffffffffffffffffffffff\ndkkkkkkkkkkk\
				 anbdjjffffffffffffffffffffffffffffff\ndkkkkkkkkkkk\
				 anbdjjffffffffffffffffffffffffffffff\ndkkkkkkkkkkk\
				 kkkkk\ndkkkkkkkkkkkkkk\nfjfjf\t";/*"ffffffffffff";*/

	mtext->setText(stext);//
	dxf->addMText(mtext);


	typedef ILWPolyLine PLine;
	typedef IVertex Vertex;


	//创建polyline（2维多段线，平面）
	PLine* pline=dxf->createPolyLine();

	srand ((unsigned int)time(NULL) );
	int randNumX,randNumY;
	double sw,ew;
	Vertex* vertex;

	for(int i=0;i<10;i++) {//生成10条任意长度、任意宽度的线段组成的多段线
		randNumX= rand() % 1000 + 1;
		randNumY= rand() % 1000 + 1;
		vertex=dxf->createVertex(randNumX,randNumY);
		sw=rand() % 50;
		ew=rand() % 50;
		vertex->setSEWidth(sw,ew);
		pline->addVertex(vertex);
	}

	dxf->addPolyLine(pline);


	//创建插入块(insert)
	IInsert* insert=dxf->createInsert();
	insert->setBlockName("块1");
	insert->setInsertPoint(2000,2000);
	insert->transform(ent);
	ent->setColor(YELLOW);
	insert->setScale(20);

	dxf->addInsert(insert);

	//最终写入到dxf文件中
	dxf->write(outFile);

	delete dxf;
}



void testRead()
{
	IDxf* dxf=createDxf();
	DxfReader* inFile=dxf->in("test.dxf");
	dxf->read(inFile);

	//详情请查看IIterator接口，该接口定义了4个接口类，
	//分别提供了遍历所有图层、在某个图层上的所有实体、
	//实体扩展数据、实体扩展数据项的功能


	AllLayersIterator* ptrLayer=dxf->createAllLayersIterator(); 
	for(;!ptrLayer->isDone();ptrLayer->next()) {
		//do something
		cout<<ptrLayer->currentItem()->getLayerName()<<endl;
	}

	//delete ptrLayer;//清除ptrLayer
	cout<<endl;

	EntityOnLayerIterator* pEntityItr=dxf->createEntityOnLayerIterator("通风网络图层");//遍历 “通风网络图层” 上的所有实体

	IEntity* pEntity;
	IXData* entityXData;
	string appName;
	string data;

	for(;!pEntityItr->isDone();pEntityItr->next()) {
		pEntity=pEntityItr->currentItem();
		cout<<"实体编号:"<<pEntity->getEntityNum()
			<<" --- 图层:"<<pEntity->getLayer()
			<<", 颜色:"<<pEntity->getColor()
			<<", 线型:"<<pEntity->getLineType()<<endl;


		//简单的测试，显示实体的扩展数据

		entityXData=pEntity->xData();

		//声明扩展数据迭代器(迭代appname)
		XDataIterator* entityXDataItr=entityXData->createXDataIterator();//详情请查看IXData接口，它提供了对xdata的读写操作以及遍历的功能
		XDataIterator* xdataElementItr=NULL;

		for(;!entityXDataItr->isDone();entityXDataItr->next()) {

			//扩展数据的appname(每个appname对应一个扩展数据链表)
			appName=entityXDataItr->currentItem();


			cout<<"\n扩展数据"
				<<"  APPID--"<<appName<<endl;

			//声明扩展数据里的元素的迭代器
			xdataElementItr=entityXData->createXDataItemIterator(appName);

			for(;!xdataElementItr->isDone();xdataElementItr->next()) {
				data=xdataElementItr->currentItem();
				cout<<data<<"  ";
			}

			cout<<"\n----------------------\n";
			cout<<"小测试，试验EntityXData类的getData函数是否有效\n";
			if(appName=="测试扩展数据" && xdataElementItr->count()!=0 ) {
				entityXData->setCurrentApp(appName);
				double x;
				entityXData->getData("温度",x);//浮点数类型
				cout<<"温度:"<<x<<endl;

				double v;
				entityXData->getData("风速",v);
				cout<<"风速:"<<v<<endl;

				string s;
				entityXData->getData("附注",s);//不存在
				cout<<"附注:"<<s<<endl;

				int i;
				entityXData->getData("风压",i);//int类型
				cout<<"风压:"<<i<<endl;

				double cx,cy,cz;
				entityXData->getData("坐标",cx,cy,cz);//坐标类型
				cout<<"坐标:"<<cx<<","<<cy<<","<<cz<<endl;
				cout<<endl;
			}
			cout<<"----------------------\n";

			cout<<endl;
		}	
		cout<<endl;

	}
	delete dxf;
}